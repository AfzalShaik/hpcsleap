var app = angular.module('myApp', ['ngRoute','datatables','ngSanitize','angularUtils.directives.dirPagination']);

app.config(function($routeProvider, $locationProvider) {
  $routeProvider

    .when('/', {
      templateUrl : '/dashboard.html',
      controller  : 'lineController'
    })
    .when('/ssoLogin', {
      templateUrl : '/dashboard.html',
      controller  : 'lineController'
    })
    .when('/transactions', {
      templateUrl : '/flow-of-transactions.html',
      controller  : 'transactionController'
    })
    .when('/settlement', {
      templateUrl : '/settlement-statements.html',
      controller  : 'SettlementController'
    })
    .when('/settlementlogs', {
      templateUrl : '/settlement-statements-logs.html',
      controller  : 'SettlementController'
    })
    .when('/reports', {
      templateUrl : '/reports.html',
      controller  : 'reportsController'
    })
    .when('/totaltransactions/:appId', {
      templateUrl : '/total-transactions.html',
      controller  : 'totalTransactionsController'
    })
    .when('/totaltransactions/:appId/:agencyTrackingId', {
      templateUrl : '/total-transactions.html',
      controller  : 'totalTransactionsController'
    })
    .when('/transactiondetails/:transactionId', {
      templateUrl : '/transaction-details.html',
      controller  : 'transactionDetailsController'
    })
    .when('/users', {
      templateUrl : '/user.html',
      controller  : 'usersController'
    })
    .when('/roles', {
      templateUrl : '/role.html',
      controller  : 'rolesController'
    })
    .when('/appConfig', {
      templateUrl : '/application-config.html',
      controller  : 'AppConfigController'
    })
    .when('/applicationDetails/:tcsAppId', {
      templateUrl : '/application-config.html',
      controller  : 'AppConfigController'
    })
    .when('/postTransactions', {
      templateUrl : '/post-transactions.html',
      controller  : 'postTransactionsController'
    })
    .when('/re-postTransactions', {
      templateUrl : '/re-post-transactions.html',
      controller  : 'rePostTransactionsController'
    })

    .when('/settlement-reportStatus', {
      templateUrl : '/settlement-report-status.html',
      controller  : 'settlementReportStatusController'
    })
    .when('/scheduler-status', {
      templateUrl : '/scheduler-status.html',
      controller  : 'schedulerStatusController'
    })
    .when('/settlement-report', {
      templateUrl : '/settlement-report.html',
      controller  : 'settlementRepController'
    })
    .otherwise({redirectTo: '/'});

  // use the HTML5 History API
  //$locationProvider.html5Mode({
  //  enabled: true,
  //  requireBase: false
  //});
});


app.controller('loginController', function($scope, $http, $rootScope, $window, $location,$sanitize) {
  var sanitizeFunction = $sanitize;
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
    sanitizeFunction = $sanitize;
  });
  $scope.user = {};
  $scope.showLoginError = false;
  $scope.loginError = '';

  $scope.ssoSubmit = function(){

    $http({
      method: 'POST',
      url: '/ssoLoginSubmit',
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data: $scope.user
    }).success(function (response) {
      if(response) {
        if(checkUrlFormat(response) && response.length > 0) {
          $window.location.href = $sanitize(response);
        }
      }
    } );
  };

  $scope.loginSubmit = function(ssoCheck){
    $scope.showLoginError = false;
    $rootScope.superAdminLogin = false;
    $rootScope.adminLogin = false;
    $rootScope.dashboardUserLogin = false;
    $rootScope.applicationUserLogin = false;
    $scope.loginError = '';
    if(ssoCheck != 'SSO') {
      if (checkMailFormat($scope.user.email)) {
        $http({
          method: 'POST',
          url: 'api/HpcsUsers/login',
          headers: {"Content-Type": "application/json", "Accept": "application/json"},
          data: $scope.user
        }).success(function (response) {
          $window.localStorage.setItem('accessToken', response.id);
          $window.localStorage.setItem('accessTokenExpireTime', response.expireTime);
          $rootScope.userLogin = true;
          $scope.user = {};
          if (response.userInfo) {
            if(response.userInfo.status == 'Active') {

              $rootScope.userName = response.userInfo.name;
              $window.localStorage.setItem('userName', response.userInfo.name);
              $window.localStorage.setItem('userInfo', JSON.stringify(response.userInfo));
              $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
              $window.localStorage.setItem('afterLogin', 'true');

              var superAdmin = 0;
              var admin = 0;
              var dashboard = 0;
              var application = 0;

              for (var i = 0; i < $rootScope.userInfo.role.length; i++) {

                if ($rootScope.userInfo.role[i] == 'Super Admin') {
                  superAdmin++;
                  break;
                } else if ($rootScope.userInfo.role[i] == 'Application Admin') {
                  admin++;
                } else if ($rootScope.userInfo.role[i] == 'Dashboard User') {
                  dashboard++;
                } else if ($rootScope.userInfo.role[i] == 'Application User') {
                  application++;
                }
              }

              if (superAdmin > 0) {
                $rootScope.superAdminLogin = true;
              } else if (admin > 0) {
                $rootScope.adminLogin = true;
              } else if (dashboard > 0) {
                $rootScope.dashboardUserLogin = true;
              } else if (application > 0) {
                $rootScope.applicationUserLogin = true;
              }
              if($rootScope.superAdminLogin ==false) {
                $rootScope.accessibleAppList = getAccessibleApps($rootScope.userInfo, $scope, $rootScope, $window);
                if ($rootScope.accessibleAppList != null) {
                  var selectedApp = {"id" : $rootScope.accessibleAppList[0].id, "name" : $rootScope.accessibleAppList[0].name };
                  if(selectedApp != null){
                    $rootScope.filterOption = selectedApp;
                    $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
                    $rootScope.userInfo.tcsAppId = selectedApp.id;
                    $rootScope.userInfo.applicationName = selectedApp.name;
                    $window.localStorage.setItem('filterOption',JSON.stringify(selectedApp));
                    $window.localStorage.setItem('userInfo',JSON.stringify($rootScope.userInfo));
                  }
                }
              }
              $rootScope.getDashboardLineData();

              //$location.url('/');
              //$window.location.reload();
            }else{
              //if(response.userInfo.status == 'Inactive')
              $window.localStorage.removeItem('accessToken');
              $window.localStorage.removeItem('accessTokenExpireTime');
              $rootScope.userLogin = false;
              $scope.showLoginError = true;
              $scope.loginError = 'Your account is Inactive. Please contact admin';
            }
          }
        }).error(function (response) {
          $scope.showLoginError = true;
          $scope.loginError = 'Invalid Email or Password';
        });
      } else {
        $scope.showLoginError = true;
        $scope.loginError = 'Please enter a valid Email';
      }
    }
  };
  //***********forgot pwd email verification*************
  $scope.openForgotPopup = function(){
    $scope.forgotEmail = '';
    $scope.showForgotError = false;
    $scope.forgotError = '';
    $scope.showLoginError = false;
    $scope.loginError = '';
  };
  $scope.openForgotPopup();

  $scope.forgotPassword = function(){
    $scope.showForgotError = false;
    $scope.forgotError = '';
    if(checkMailFormat($scope.forgotEmail)) {
      $http({
        method: 'GET',
        url: 'api/HpcsUsers/forgotPassword?email='+$scope.forgotEmail,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
        if(response.status) {
          if (response.status == 'User Not Found') {
            $scope.showForgotError = true;
            $scope.forgotError = response.status;
          } else {
            $scope.showLoginError = true;
            $scope.loginError = response.status;
            var _tpass = new timePassed(5000);

            // setTimeout(function () {
            //   $rootScope.showLoginError = false;
            //   $scope.loginError = '';
            // }, 5000);

            if( _tpass() ) {
              $rootScope.showLoginError = false;
              $scope.loginError = '';
            }
            $('#myModal').modal('hide');
          }
        }
      }).error(function(err){
        console.log('Error:'+JSON.stringify(err));
      });
    }else{
      $scope.showForgotError = true;
      $scope.forgotError = 'Please enter a valid Email';
      //alert("Email FALSE");
    }

  };

  $scope.reset = function(){
    $scope.user = {};
    $scope.showLoginError = false;
    $scope.loginError = '';
  }


});


app.controller('indexController', function($scope, $rootScope, $window, $location, $http,$sanitize) {
  var sanitizeFunction;
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    sanitizeFunction = $sanitize;
  });

  $scope.removeSessionInfo = function(){
    var sessionId = $window.localStorage.getItem('sessionId');
    $http({
      method: 'DELETE',
      url: 'api/SessionInfos/'+sessionId,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $window.localStorage.removeItem('sessionId');
    });
  };

  $rootScope.validateSession = function(redirectCheck){
    if(redirectCheck == 'redirectToLogin '){
      $('#sessionTimeOut').modal('hide');
      $location.url('/');
    }else {
      $window.localStorage.removeItem("userInfo");
      $window.localStorage.removeItem("accessToken");
      $window.localStorage.removeItem("accessTokenExpireTime");
      $window.localStorage.removeItem("afterLogin");
      $window.localStorage.removeItem("reportsDateRange");
      $window.localStorage.removeItem("reportsTransactions");
      $window.localStorage.removeItem("userName");
      $rootScope.userLogin = false;
      sessionActive = true;
      if($window.localStorage.getItem('sessionId')){
        $scope.removeSessionInfo();
      }
      $('#sessionTimeOut').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
      });
      $location.url('/');
    }
  };
  $rootScope.userLogin = false;

  $rootScope.userName = $window.localStorage.getItem('userName');

  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));

  if($window.localStorage.getItem('sso') == 'true'){
    var userEmail = $window.localStorage.getItem('userEmail');
    var accessToken= $window.localStorage.getItem('accessToken');
    var ssoMessage = $window.localStorage.getItem('ssoMessage');

    if(ssoMessage == 'Enter Email' || ssoMessage == 'User Not Found'){
      $rootScope.userLogin = false;
      $window.localStorage.removeItem('sso');
      $window.localStorage.removeItem('userEmail');
      $window.localStorage.removeItem('accessToken');
      $window.localStorage.removeItem("userInfo");
    }else {
      $http({
        method: 'GET',
        url: 'api/HpcsUsers?filter={"where":{"email":"' + userEmail + '"}}&access_token=' + accessToken,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
        if (response) {
          $window.localStorage.setItem('userInfo', JSON.stringify(response[0]));
          $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
          $window.localStorage.setItem('afterLogin', 'true');
          $window.localStorage.removeItem('sso');
          $scope.checkUserRole();
          $window.location.href = 'https://' + $window.location.host;
        }
      }).error(function (response) {
        $scope.showLoginError = true;
        $scope.loginError = 'User not registered in HPCS. Please contact HPCS administrator for registration.';
      });
    }
  }

  $scope.checkUserRole = function() {
    if ($rootScope.userInfo) {
      $rootScope.userLogin = true;

      var superAdmin = 0;
      var admin = 0;
      var dashboard = 0;
      var application = 0;
      for (var i = 0; i < $rootScope.userInfo.role.length; i++) {
        if ($rootScope.userInfo.role[i] == 'Super Admin') {
          superAdmin++;
          break;
        } else if ($rootScope.userInfo.role[i] == 'Application Admin') {
          admin++;
        } else if ($rootScope.userInfo.role[i] == 'Dashboard User') {
          dashboard++;
        } else if ($rootScope.userInfo.role[i] == 'Application User') {
          application++;
        }
      }

      if (superAdmin > 0) {
        $rootScope.superAdminLogin = true;
      } else if (admin > 0) {
        $rootScope.adminLogin = true;
      } else if (dashboard > 0) {
        $rootScope.dashboardUserLogin = true;
      } else if (application > 0) {
        $rootScope.applicationUserLogin = true;
      }
    }
  };
  $scope.checkUserRole();

  $scope.logout = function(){
    var accessToken = $window.localStorage.getItem('accessToken');
    function removeLocalStorage(){
      $window.localStorage.removeItem("userInfo");
      $window.localStorage.removeItem("accessToken");
      $window.localStorage.removeItem("afterLogin");
      $window.localStorage.removeItem("userName");
      $rootScope.userLogin = false;

      if($window.localStorage.getItem('sessionId')){
        $scope.removeSessionInfo();
      }
      $window.localStorage.setItem('afterLogout','true');
      $location.url('/');
    }
    $http({
      method: 'POST',
      url: 'api/HpcsUsers/logout?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      removeLocalStorage();
    }).error(function(response){
      if(response.error.message){
        //if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
        removeLocalStorage();
        //}
      }
    });
  };

  $rootScope.ExcelExport = function(tableId, sheetName){
    //alert('...............');
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById(tableId); // id of table


    if(tab_text) {
      for (j = 0; j < tab.rows.length; j++) {
        var rowsText = $sanitize(tab.rows[j].innerHTML);
        if(rowsText) {
          tab_text = tab_text + rowsText + "</tr>";
        }else {
          tab_text = tab_text + "</tr>";
        }
      }

      tab_text = tab_text + "</table>";
      tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
      tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
      tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
      tab_text = $sanitize(tab_text);
      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");

      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
      {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, sheetName);
      }
      else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));


      return (sa);
    }
  }
});

var sessionActive = true;
app.controller('SessionController', function($scope, $rootScope, $window, $http, $location) {
  var sessionCheck = 0;

  function timerCount(counter){
    $rootScope.minTimer = parseInt($rootScope.countDown/60);
    $rootScope.secTimer = $rootScope.countDown%60;
    if(counter <= 0){
      sessionCheck = 0;
      $('#sessionTimer').modal('hide');
      $rootScope.validateSession();
    }else {
      var _tpass = new timePassed(1000);
      // setTimeout(function () {
      //   $rootScope.countDown = counter -1;
      //   if(sessionActive == false) {
      //     timerCount(counter - 1)
      //   }else{
      //     $rootScope.countDown = 0;
      //   }
      // }, 1000);
      if( _tpass() ) {
        $rootScope.countDown = counter -1;
        if(sessionActive == false) {
          timerCount(counter - 1)
        }else{
          $rootScope.countDown = 0;
        }
      }
    }
  }

  $rootScope.continueSession = function(){
    var accessToken = $window.localStorage.getItem('accessToken');
    var sessionId = $window.localStorage.getItem('sessionId');
    var currentTime = new Date();
    $http({
      method: 'PUT',
      url: 'api/SessionInfos/'+sessionId+'?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data:{"lastAccessedTime":currentTime}
    }).success(function(data){
      sessionActive = true;
      sessionCheck = 0;
      $('#sessionTimer').modal('hide');
    }).error(function(data){

    });
  };

  setInterval(function () {
    if($window.localStorage.getItem('accessToken') && $window.localStorage.getItem('userInfo')) {
      var accessToken = $window.localStorage.getItem('accessToken');
      var expire =new Date(Number($window.localStorage.getItem('accessTokenExpireTime')));

      expire = expire.getTime();
      var currentTime = new Date().getTime();

      if(expire > currentTime) {
        //alert('Not Expired');
        $rootScope.userLogin = true;
        $rootScope.userName = $window.localStorage.getItem('userName');
        $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));

        if((expire - currentTime) < 2*60*1000){
          $http({
            method: 'POST',
            url: 'api/AccessTokens?access_token='+accessToken,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data:{
              'userId': $rootScope.userInfo.id
            }
          }).success(function(data){
            $window.localStorage.setItem('accessToken',data.id);
            $window.localStorage.setItem('accessTokenExpireTime',data.expireTime);
          }).error(function(data){

          });
        }
      }else{
        $rootScope.validateSession();
      }
      $http({
        method: 'GET',
        url: 'api/SessionInfos?filter={"where":{"accessToken":"'+accessToken+'"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function(data){
        $window.localStorage.setItem('sessionId',data[0].id);
        var lastAccessedTime = new Date(data[0].lastAccessedTime).getTime();
        if((currentTime - lastAccessedTime) >= (data[0].sessionDuration-(5*60*1000))){
          sessionActive = false;
          if(sessionCheck == 0) {
            sessionCheck = 1;
            $rootScope.countDown = 5*60;
            $('#sessionTimer').modal({
              backdrop: 'static',
              keyboard: false,
              show : true
            });
            timerCount($rootScope.countDown);
          }
        }else{
          sessionActive = true;
        }

      }).error(function(data){

      });
    }
  }, 1000);
});

app.controller('SettlementController', function($scope, $rootScope, $window, $http) {
  $scope.message = 'SSController';
  if($rootScope.filterOption == undefined){
    $rootScope.filterOption = JSON.parse($window.localStorage.getItem('filterOption'));
  }
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  var accessToken = $window.localStorage.getItem('accessToken');
  if($rootScope.userInfo && $rootScope.superAdminLogin != true) {
    $http({
      method: 'GET',
      url: 'api/ApplicationConfigurations?filter={"where":{"name":"' + $rootScope.userInfo.applicationName + '"}}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.appSettlement = {
        "machineId": response[0].machineId,
        "password": response[0].password,
        "agencyId": response[0].agencyId
      };
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  }

  $(function() {
    var changeReportDate = function () {
      $scope.reportDate = $("#datepicker").datepicker("getDate");
      $scope.$apply();
    };

    $("#datepicker").datepicker({
      maxDate:0,
      dateFormat: 'mm/dd/yy',
      onSelect: changeReportDate
    });
  });

  $scope.formSubmit = function(){
    agency_id=$("#agency_id").val();
    username= $("#username").val();
    password = $("#password").val();
    date=$("#datepicker").val();
    report_name =$("#reportOptions").val();
    format = $("#format").val();
    $('#submit').prop("disabled", true);
    $.post("/settlementReport",{'agency_id': agency_id,'username': username,'password':password,'date':date,'report_name':report_name,'format':format}, function(data)
    {
      $('#submit').prop("disabled", false);
      var filename = report_name+"_" + date + ".xml";
      saveTextAsFile(filename,data);
      savelogs(report_name,date);
    });
  }

  function saveTextAsFile(fileNameToSaveAs, textToWrite) {
    var ie = navigator.userAgent.match(/MSIE\s([\d.]+)/),
      ie11 = navigator.userAgent.match(/Trident\/7.0/) && navigator.userAgent.match(/rv:11/),
      ieEDGE = navigator.userAgent.match(/Edge/g),
      ieVer=(ie ? ie[1] : (ie11 ? 11 : (ieEDGE ? 12 : -1)));

    if (ie && ieVer<10) {
      console.log("No blobs on IE ver<10");
      return;
    }

    var textFileAsBlob = new Blob([textToWrite], {
      type: 'application/xml'
    });

    if (ieVer>-1) {
      window.navigator.msSaveBlob(textFileAsBlob, fileNameToSaveAs);

    } else {
      var link = document.getElementById("downloadLink");
      var url = 'data:application/xml,' + escape(textToWrite);
      link.setAttribute("href",url );
      link.setAttribute("download", fileNameToSaveAs);
      link.click();
    }
  }

  function savelogs(reportName,reportDate){
    $http({
      method: 'POST',
      url: 'api/SettlementLogs?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data:{
        'userName': $rootScope.userInfo.name,
        'applicationName': $rootScope.userInfo.applicationName,
        'machineId': $scope.appSettlement.machineId,
        'password': $scope.appSettlement.password,
        'agencyId': $scope.appSettlement.agencyId,
        'reportName': reportName,
        'reportDate': reportDate ,
        'createdDate': new Date()
      }
    }).success(function (response) {
      console.log('Success....'+JSON.stringify(response));
    }).error(function(response){
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };


  /*
   $scope.formSubmit = function(){
   $http({
   method: 'POST',
   url: 'api/SettlementLogs?access_token='+accessToken,
   headers: {"Content-Type": "application/json", "Accept": "application/json"},
   data:{
   'userName': $rootScope.userInfo.name,
   'applicationName': $rootScope.userInfo.applicationName,
   'machineId': $scope.appSettlement.machineId,
   'password': $scope.appSettlement.password,
   'agencyId': $scope.appSettlement.agencyId,
   'reportName': $scope.selectedReport,
   'reportDate': $scope.reportDate,
   'createdDate': new Date()
   }
   }).success(function (response) {
   console.log('Success....'+JSON.stringify(response));
   }).error(function(response){
   if(response.error.message){
   if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
   $window.localStorage.removeItem("userInfo");
   $rootScope.validateSession();
   }else if(response.error.message == 'Session Expired'){
   $rootScope.validateSession();
   }
   }
   });
   };*/

  $scope.getSettlementLogs = function(){
    var url;
    if($rootScope.userInfo && $rootScope.adminLogin == true){
      url = 'api/SettlementLogs?filter={"where":{"applicationName":"'+$rootScope.userInfo.applicationName+'"}}&access_token='+accessToken;
    }else{
      url = 'api/SettlementLogs?access_token='+accessToken;
    }

    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function(response){
      $scope.settlementLogsData = response;
    }).error(function(response){
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };
  $scope.getSettlementLogs();

  $scope.exportToExcel = function(action){
    switch(action){
      case 'pdf': $scope.$broadcast('export-pdf', {});
        break;
      case 'excel': $scope.$broadcast('export-excel', {});
        break;
      case 'doc': $scope.$broadcast('export-doc', {});
        break;
      case 'xml': $scope.$broadcast('export-xml', {});
        break;
      default: console.log('no event caught');
    }
  };
});

app.controller('reportsController', function($scope, $http, $window, Excel, $rootScope, $timeout, $location) {
  $scope.message = 'ReportsController';
  $(document).ready(function () {
    $('html,body').scrollTop(0);
  });
  //$rootScope.validateAccessToken();
  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.showReports = false;
  if($rootScope.filterOption == undefined){
    $rootScope.filterOption = JSON.parse($window.localStorage.getItem('filterOption'));
  }
  $scope.getApplications = function(filterApp){
    //$rootScope.validateAccessToken();
    var url;
    if(filterApp){
      url = 'api/ApplicationConfigurations?filter={"where":{"tcsAppId":"'+filterApp+'"}}&access_token='+accessToken;
    }else{
      url = 'api/ApplicationConfigurations?access_token='+accessToken;
    }
    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.applications = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $(function() {
    $( "#startDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

    $( "#endDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });
  });

  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  if($rootScope.userInfo && $rootScope.userInfo.tcsAppId){
    $scope.getApplications($rootScope.userInfo.tcsAppId);
  }else {
    $scope.getApplications();
  }
  //
  //var currentTime = new Date();
  //currentTime = currentTime.setHours(0,0,0,0);
  var dateRange = [];

  $scope.filterByDate = function(){
    $scope.dateSelectionError = false;

    $scope.startDate = $("#startDatePicker").datepicker("getDate");
    $scope.endDate = $("#endDatePicker").datepicker("getDate");

    $scope.selectedApplication = document.getElementById('appFilterOptions').value;
    $scope.agencyTrackignId = document.getElementById('agencyTrackingId').value;

    if ($scope.startDate) {
      if ($scope.endDate) {
        if ($scope.startDate > $scope.endDate) {
          var tempDate;
          tempDate = $scope.endDate;
          $scope.endDate = $scope.startDate;
          $scope.startDate = tempDate;
        }
        $scope.startDate = $scope.startDate.setHours(0,0,0,0);
        $scope.startDate = new Date($scope.startDate);
        $scope.endDate = $scope.endDate.setHours(23,59,59,59);
        $scope.endDate = new Date($scope.endDate);

        dateRange = [];
        dateRange.push($scope.startDate);
        dateRange.push($scope.endDate);

        var url;

        if($scope.selectedApplication != ''){
          url = 'api/Requests/getTransactionsCount?filterQuery='+$scope.selectedApplication+'&filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
        }else{
          if($rootScope.superAdminLogin) {
            url = 'api/Requests/getTransactionsCount?filterDateQuery=' + JSON.stringify(dateRange)+'&access_token='+accessToken;
          }else if($rootScope.userInfo && $rootScope.userInfo.tcsAppId){
            url = 'api/Requests/getTransactionsCount?filterQuery='+$rootScope.userInfo.tcsAppId+'&filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
          }
        }

        if($scope.agencyTrackignId != ''){
          url += ('&filterAgencyTrackingId=' + $scope.agencyTrackignId);
        }

        $http({
          method: 'GET',
          url: url,
          headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
          $scope.showReports = true;
          $scope.agencyTransactions = response.TransactionCount;
        }).error(function (response) {
          if(response.error.message){
            if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
              $window.localStorage.removeItem("userInfo");
              $rootScope.validateSession();
            }else if(response.error.message == 'Session Expired'){
              $rootScope.validateSession();
            }
          }
        });
      } else {
        $scope.dateSelectionError = true;
        $scope.dateError = 'Please Select End Date';
      }
    } else {
      $scope.dateSelectionError = true;
      $scope.dateError = 'Please Select Start Date';
    }

  };

  /*$scope.exportToExcel=function(tableId){ // ex: '#my-table'
   var exportHref=Excel.tableToExcel(tableId,'Reports');
   //$timeout(function(){location.href=exportHref;},100); // trigger download
   $timeout(function() {
   var link = document.createElement('a');
   link.download = "Reports(Transactions Count).xls";
   link.href = exportHref;
   link.click();
   }, 100);
   };*/

  $scope.exportToExcel = function(action){
    switch(action){
      case 'pdf': $scope.$broadcast('export-pdf', {});
        break;
      case 'excel': $scope.$broadcast('export-excel', {});
        break;
      case 'doc': $scope.$broadcast('export-doc', {});
        break;
      case 'xml': $scope.$broadcast('export-xml', {});
        break;
      default: console.log('no event caught');
    }
  };

  $scope.getStatus = function(status, appId, appName, agencyTrackingId){
    $window.localStorage.setItem('reportsDateRange', JSON.stringify(dateRange));
    $window.localStorage.setItem('reportsTransactions', 'true');
    $window.localStorage.setItem('selectedApplication', appName);
    $window.localStorage.setItem('agencyTrackingId', agencyTrackingId);
    if(status == 'removeStatus'){
      $window.localStorage.removeItem('transactionStatus');
    }else {
      $window.localStorage.setItem('transactionStatus', status);
    }

    if(agencyTrackingId){
      $location.url('/totaltransactions/' + appId + '/' + agencyTrackingId);
    }else {
      $location.url('/totaltransactions/' + appId);
    }
  };

});

app.controller('transactionController', function($scope, $http, $window, $rootScope) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
  });
  //$rootScope.validateAccessToken();
  $scope.message = 'TTController';

  if($rootScope.filterOption == undefined){
    $rootScope.filterOption = JSON.parse($window.localStorage.getItem('filterOption'));
  }

  if($rootScope.superAdminLogin){
    $rootScope.filterOption = {};
  }
  //var currentTime = new Date();
  //currentTime = currentTime.setHours(0,0,0,0);

  var dateRange = [];
  $(function() {
    $( "#startDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

    $( "#endDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });
  });

  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.transactionCount = function(filterApp) {
    var url;
    if(filterApp && filterApp != ''){
      if(dateRange.length > 0){
        url = 'api/Requests/getTransactionsCount?filterQuery='+filterApp+'&filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
      }else{
        url = 'api/Requests/getTransactionsCount?filterQuery='+filterApp+'&access_token='+accessToken;
      }
    }else{
      if(dateRange.length > 0){
        url = 'api/Requests/getTransactionsCount?filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
      }else{
        url = 'api/Requests/getTransactionsCount?access_token='+accessToken;
      }
    }

    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      if(response.TransactionCount == 'No Applications Found'){
        $scope.agencyTransactions = [];
      }else {
        $scope.agencyTransactions = response.TransactionCount;
      }
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

  };

  $scope.getApplications = function(filterApp){
    //$rootScope.validateAccessToken();
    var url;
    if(filterApp){
      url = 'api/ApplicationConfigurations?filter={"where":{"tcsAppId":"'+filterApp+'"}}&access_token='+accessToken;
    }else{
      url = 'api/ApplicationConfigurations?access_token='+accessToken;
    }
    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.applications = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  if($rootScope.userInfo && $rootScope.userInfo.tcsAppId){
    $scope.transactionCount($rootScope.userInfo.tcsAppId);
    $scope.getApplications($rootScope.userInfo.tcsAppId);
  }else {
    if($window.localStorage.getItem('reportsDateRange')){
      dateRange = JSON.parse($window.localStorage.getItem('reportsDateRange'));
      $window.localStorage.removeItem('reportsDateRange');
    }
    $scope.transactionCount();
    $scope.getApplications();
  }

  $scope.getStatus = function(status, appName){
    $window.localStorage.setItem('reportsDateRange', JSON.stringify(dateRange));
    $window.localStorage.setItem('reportsTransactions', 'true');
    $window.localStorage.removeItem('reportsTransactions');
    $window.localStorage.setItem('selectedApplication', appName);
    if(status == 'removeStatus'){
      $window.localStorage.removeItem('transactionStatus');
    }else {
      $window.localStorage.setItem('transactionStatus', status);
    }
  };

  $scope.filterByDate = function(){
    $scope.dateSelectionError = false;
    $scope.startDate = $("#startDatePicker").datepicker("getDate");
    $scope.endDate = $("#endDatePicker").datepicker("getDate");

    if($scope.startDate) {
      if ($scope.endDate) {

        if ($scope.startDate > $scope.endDate) {
          var tempDate;
          tempDate = $scope.endDate;
          $scope.endDate = $scope.startDate;
          $scope.startDate = tempDate;
        }

        //alert($scope.startDate +'........'+$scope.endDate);

        $scope.startDate = $scope.startDate.setHours(0,0,0,0);
        $scope.startDate = new Date($scope.startDate);
        $scope.endDate = $scope.endDate.setHours(23,59,59,59);
        $scope.endDate = new Date($scope.endDate);
        //alert($scope.startDate +'........'+$scope.endDate);

        dateRange = [];
        dateRange.push(new Date($scope.startDate));
        dateRange.push(new Date($scope.endDate));

        var check = document.getElementById('appFilterOptions').value;
        var url;
        if(checkAlphaNumeric(check) && check != ''){
          url = 'api/Requests/getTransactionsCount?filterQuery='+check+'&filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
        }else{
          if($rootScope.userInfo && $rootScope.userInfo.tcsAppId){
            url = 'api/Requests/getTransactionsCount?filterQuery='+$rootScope.userInfo.tcsAppId+'&filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
          }else {
            url = 'api/Requests/getTransactionsCount?filterDateQuery='+JSON.stringify(dateRange)+'&access_token='+accessToken;
          }
        }
        $http({
          method: 'GET',
          url: url,
          headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
          $scope.agencyTransactions = response.TransactionCount;
        }).error(function (response) {
          if(response.error.message){
            if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
              $window.localStorage.removeItem("userInfo");
              $rootScope.validateSession();
            }else if(response.error.message == 'Session Expired'){
              $rootScope.validateSession();
            }
          }
        });
      }else{
        $scope.dateSelectionError = true;
        $scope.dateError = 'Please Select End Date';
      }
    }else{
      $scope.dateSelectionError = true;
      $scope.dateError = 'Please Select Start Date';
    }
  };
});

app.controller('totalTransactionsController', function($scope, $http, $routeParams, $window, $rootScope, $timeout, Excel) {

  $(document).ready(function () {
    $('html,body').scrollTop(0);
  });
  if($rootScope.filterOption == undefined){
    $rootScope.filterOption = JSON.parse($window.localStorage.getItem('filterOption'));
  }

  if(!$rootScope.superAdminLogin){
    $scope.selectedAppName = $rootScope.filterOption;
  }
  //default initial values for pagination
  $scope.transactionsPerPage = 10;
  $scope.pageNumber = 1;

  $scope.orderByField = '- created_date';
  $scope.reverseSort = false;

  $scope.pagination = {
    current: 1
  };

  $scope.pageChanged = function(newPage) {
    $scope.pageNumber = newPage;
    getTransactions();
  };



  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.message = 'totalTransactionsController';
  $scope.showExportExcel = false;
  if($window.localStorage.getItem('reportsTransactions')){
    $scope.showExportExcel = true;
  }

  $(function() {
    $( "#startDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

    $( "#endDatePicker" ).datepicker({
      dateFormat: 'mm/dd/yy',
      maxDate: 0
    });
  });

  var filterArray = [];
  var dateRange = [];
  $scope.filterByDate = function(){
    $scope.dateSelectionError = false;
    $scope.startDate = $("#startDatePicker").datepicker("getDate");
    $scope.endDate = $("#endDatePicker").datepicker("getDate");

    if($scope.startDate) {
      if ($scope.endDate) {

        if ($scope.startDate > $scope.endDate) {
          var tempDate;
          tempDate = $scope.endDate;
          $scope.endDate = $scope.startDate;
          $scope.startDate = tempDate;
        }

        //alert($scope.startDate +'........'+$scope.endDate);

        $scope.startDate = $scope.startDate.setHours(0,0,0,0);
        $scope.startDate = new Date($scope.startDate);
        $scope.endDate = $scope.endDate.setHours(23,59,59,59);
        $scope.endDate = new Date($scope.endDate);
        //alert($scope.startDate +'........'+$scope.endDate);

        dateRange = [];
        dateRange.push($scope.startDate);
        dateRange.push($scope.endDate);
        getTransactions();
      }else{
        $scope.dateSelectionError = true;
        $scope.dateError = 'Please Select End Date';
      }
    }else{
      $scope.dateSelectionError = true;
      $scope.dateError = 'Please Select Start Date';
    }
  };

  if($routeParams.appId) {
    var appName = $routeParams.appId;
    filterArray.push({"tcs_app_id": appName});

    if($routeParams.agencyTrackingId){
      var agencyTrackingId = $routeParams.agencyTrackingId;
      filterArray.push({"agency_tracking_id": {"like":agencyTrackingId}});
    }
    if($window.localStorage.getItem('transactionStatus')){
      var transactionStatus = $window.localStorage.getItem('transactionStatus');
      if(transactionStatus == 'Submitted'){
        filterArray.push({"or":[{"status":"Submitted"},{"status":"Received"}]});
      }else {
        filterArray.push({"status": transactionStatus});
      }
    }

    if($window.localStorage.getItem('selectedApplication')) {
      $scope.selectedAppName = $window.localStorage.getItem('selectedApplication');
    }
    getTransactions();

  }

  function getTransactionsCount(appFilterArray){
    $http({
      method: 'GET',
      url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '}}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.transactionsCount = Object.keys(response).length;
      //console.log('TOTAL TRANSACTIONS COUNT METHOD ' + $scope.transactionsCount);
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

    if($scope.transactionCount == 0){
      $scope.pageNumber = 0;
    }

  }

  function getTransactions(){
    var appFilterArray = [];
    appFilterArray = appFilterArray.concat(filterArray);
    if($window.localStorage.getItem('reportsDateRange')){
      dateRange = JSON.parse($window.localStorage.getItem('reportsDateRange'));
      $window.localStorage.removeItem('reportsDateRange');
    }
    if(dateRange.length > 0){
      appFilterArray.push({"created_date":{"between": dateRange}});
    }
    getTransactionsCount(appFilterArray);
    $scope.pageNumber = $scope.pageNumber-1;
    var skip = $scope.transactionsPerPage * $scope.pageNumber;

    if(skip < 0)
      skip = 0;

    $http({
      method: 'GET',
      //url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '}}&access_token='+accessToken,
      url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '},"order":"created_date desc","limit":'+ $scope.transactionsPerPage + ',"skip":"'+ skip +'"}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response)
    {
      $scope.totalTransactions = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  }

  /*$scope.exportToExcel=function(tableId){ // ex: '#my-table'
   var exportHref=Excel.tableToExcel(tableId,'Total Transactions');
   //$timeout(function(){location.href=exportHref;},100); // trigger download
   $timeout(function() {
   var link = document.createElement('a');
   link.download = "Reports(Total Transactions).xls";
   link.href = exportHref;
   link.click();
   }, 100);
   };*/

  $scope.exportToExcel = function(action){
    switch(action){
      case 'pdf': $scope.$broadcast('export-pdf', {});
        break;
      case 'excel': $scope.$broadcast('export-excel', {});
        break;
      case 'doc': $scope.$broadcast('export-doc', {});
        break;
      case 'xml': $scope.$broadcast('export-xml', {});
        break;
      default: console.log('no event caught');
    }
  }
});

app.controller('transactionDetailsController', function($scope, $routeParams, $http, $rootScope, $window) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
  });

  //$rootScope.validateAccessToken();
  $scope.showExportExcel = false;
  if($window.localStorage.getItem('reportsTransactions')){
    $scope.showExportExcel = true;
  }
  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.message = 'transactionDetailsController';
  $scope.requestArray = [];
  $scope.requestSubArray = [];
  $scope.responseArray = [];
  $scope.responseSubArray = [];
  $http({
    method: 'GET',
    url: 'api/Requests/getTransactionsDetails?transactionId='+$routeParams.transactionId+'&access_token='+accessToken,
    headers:{"Content-Type": "application/json", "Accept": "application/json"}
  }).success(function(response){
    $scope.transactionData = response.transactionData;
    if($scope.transactionData.request) {
      var requestData = JSON.parse($scope.transactionData.request);
      for (var property in requestData) {
        if (!requestData.hasOwnProperty(property)) {
          continue;
        }
        if ((typeof requestData[property] == 'object') && requestData[property] != null) {
          var tempSubArray = [];
          for (var reqObj in requestData[property]) {
            if (!requestData[property].hasOwnProperty(reqObj)) {
              continue;
            }
            if(reqObj == 'account_number' || reqObj == 'routing_transit_number') {
              var maskedStr = new Array(requestData[property][reqObj].length - 4 + 1).join('x')
                + requestData[property][reqObj].slice(-4);
              tempSubArray.push({"key": reqObj, "value": maskedStr});
            }
            else {
              tempSubArray.push({"key": reqObj, "value": requestData[property][reqObj]});
            }
          }
          $scope.requestSubArray.push({"key": property, "value": tempSubArray});
        } else {
          $scope.requestArray.push({"key": property, "value": requestData[property]});
        }
      }
    }

    if($scope.transactionData.response){
      var responseData = JSON.parse($scope.transactionData.response);
      for (var property in responseData) {
        if ( ! responseData.hasOwnProperty(property)) {
          continue;
        }
        if((typeof responseData[property] == 'object') && responseData[property] != null){
          var tempSubArray = [];
          for (var reqObj in responseData[property]) {
            if (!responseData[property].hasOwnProperty(reqObj)) {
              continue;
            }
            tempSubArray.push({"key": reqObj, "value": responseData[property][reqObj]});
          }
          $scope.responseSubArray.push({"key": property, "value": tempSubArray});
        }else {
          $scope.responseArray.push({"key":property,"value":responseData[property]});
        }
      }
    }

  }).error(function(response){
    if(response.error.message){
      if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
        $window.localStorage.removeItem("userInfo");
        $rootScope.validateSession();
      }else if(response.error.message == 'Session Expired'){
        $rootScope.validateSession();
      }
    }
  });
});

//*************drop Down*****************

app.controller('myCtrl', function($scope) {
  $scope.agency = [
    {agency : "agency1"},
    {agency : "agency2"},
    {agency : "agency3"}
  ];

  $(function() {
    $( "#datepicker" ).datepicker();
  });


});

//*******************User create Code**********

app.controller('usersController', function($scope,$http,$rootScope, $window) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
  });

  //$rootScope.validateAccessToken();

  var accessToken = $window.localStorage.getItem('accessToken');
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  var url;
  if($rootScope.userInfo && $rootScope.adminLogin == true){
    url = 'api/HpcsUsers?filter={"where":{"and":[{"applicationName":"'+$rootScope.userInfo.applicationName+'"},{"role":{"neq":"Super Admin"}}]}}&access_token='+accessToken;
  }else{
    url = 'api/HpcsUsers?filter={"where":{"role":{"neq":"Super Admin"}}}&access_token='+accessToken;
  }

  $scope.getUsers = function() {
    //$rootScope.validateAccessToken();
    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $rootScope.usersData = response;

    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };
  $scope.getUsers();

  $scope.reset = function() {
    $scope.userCreationError = false;
    $scope.createUserError = '';
    $scope.user = angular.copy($scope.master);
    $scope.getRolesAndApps();
  };

  $scope.user = {};
  $scope.userCreationError = false;
  $scope.createUserError = '';
  //************role dropdown*******************
  $scope.getRolesAndApps = function() {
    //$rootScope.validateAccessToken();
    $http({
      method: 'GET',
      url: 'api/HpcsRoles?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.role = response;

      if($scope.role.length > 0){
        for(var i=0; i < $scope.role.length; i++){
          if($scope.role[i].name == 'Super Admin'){
            $scope.role.splice(i,1);
          }
        }
      }
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
    $http({
      method: 'GET',
      url: 'api/ApplicationConfigurations?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.applications = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

    $http({
      method: 'GET',
      url: 'api/HpcsApis',
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.apiMethods = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };
  $scope.showPwdField = true;

  var hudUserCheck;
  $scope.hudPwd = '';

  $scope.getHudPassword = function(value){
    if(value == 'edit'){
      hudUserCheck = document.getElementById('hudCheckEdit').checked;
    }else {
      hudUserCheck = document.getElementById('hudCheck').checked;
    }
    $scope.hudPwd = '';
    if(hudUserCheck == true){
      $scope.showPwdField = false;
      $scope.hudPwd = generateHudPwd();
    }else{
      $scope.showPwdField = true;
    }
  };

  var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
  var pwdCheck = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;

  $scope.createUser = function() {
    hudUserCheck = document.getElementById('hudCheck').checked;
    $scope.userCreationError = false;
    $scope.createUserError = '';
    if($scope.user.name.match(alphabetsWithSpaces) && $scope.user.name.length < 20){
      //alert("Name TRUE");
      if(checkOnlyAlphabets($scope.user.lastName) && $scope.user.lastName.length < 15){
        if (checkMailFormat($scope.user.email)) {
          if($scope.user.role) {
            if($scope.user.applicationName){
              if($scope.user.status) {
                var pwdMatch;
                if(hudUserCheck == true){
                  pwdMatch = true;
                  $scope.user.password = $scope.hudPwd;
                  $scope.user.userType = 'HudUser';
                }else{

                  pwdMatch = $scope.user.password.match(pwdCheck);
                  $scope.user.userType = 'NonHudUser';
                }

                if (pwdMatch) {
                  $http({
                    method: 'POST',
                    url: 'api/HpcsUsers?access_token=' + accessToken,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.user

                  }).success(function (response) {
                    $rootScope.usersData.push(response);
                    $scope.user = {};
                    $('#myModal').modal('hide');
                  }).error(function (response) {
                    if (response.error.details.messages.email) {
                      $scope.createUserError = response.error.details.messages.email[0];
                      $scope.createUserError = 'Email already Exist';

                      $scope.userCreationError = true;
                    }
                    if (response.error.message) {
                      if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
                        $window.localStorage.removeItem("userInfo");
                        $rootScope.validateSession();
                      } else if (response.error.message == 'Session Expired') {
                        $rootScope.validateSession();
                      }
                    }

                  });
                } else {
                  $scope.createUserError = 'Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters';
                  $scope.userCreationError = true;
                }
              }else {
                $scope.createUserError = 'Please Select User Status';
                $scope.userCreationError = true;
              }
            } else {

              $scope.createUserError = 'Please Select Application Name';
              $scope.userCreationError = true;
            }
          }else{
            $scope.userCreationError = true;
            $scope.createUserError = 'Please Select atleast one Role';
          }
        }else {
          $scope.userCreationError = true;
          $scope.createUserError = 'Please enter a valid Email';

          //alert("AgencyId FALSE");
        }
      }else {
        $scope.userCreationError = true;
        $scope.createUserError = 'Please enter the Last Name (Alphabets only)';
        //alert("Name FALSE");
      }
    }else{
      $scope.userCreationError = true;
      $scope.createUserError = 'Please enter the First Name ((Alphabets and space only))';
      //alert("Name FALSE");
    }
  };

  $scope.updateUser = {};
  $scope.editPopup = function(userInfo){
    $scope.showPwdField = true;
    $scope.hudPwd = '';
    if(userInfo.userType == 'HudUser') {
      document.getElementById('hudCheckEdit').checked = true;
    }else{
      document.getElementById('hudCheckEdit').checked = false;
    }
    $scope.updateUser = userInfo;
    $scope.getRolesAndApps();
  };

  $scope.editUser = function(){
    $scope.userCreationError = false;
    $scope.createUserError = '';
    hudUserCheck = document.getElementById('hudCheckEdit').checked;
    if($scope.updateUser.name.match(alphabetsWithSpaces) && $scope.updateUser.name.length < 20){
      //alert("Name TRUE");
      if(checkOnlyAlphabets($scope.updateUser.lastName) && $scope.updateUser.lastName.length < 15) {
        //alert("Name TRUE"+$scope.updateUser.applicationName);
        if ($scope.updateUser.applicationName) {
          //alert("AgencyId TRUE");
          if ($scope.updateUser.role.length > 0) {
            if($scope.updateUser.status) {
              //alert(JSON.stringify($scope.updateUser.role));
              if (checkMailFormat($scope.updateUser.email)) {
                //alert("Email TRUE");
                var userEditFunction = function () {
                  $http({
                    method: 'PUT',
                    url: 'api/HpcsUsers/' + $scope.updateUser.id + '?access_token=' + accessToken,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: $scope.updateUser
                  }).success(function (response) {
                    $scope.updateUser = {};
                    $scope.getUsers();
                    $('#userEdit').modal('hide');
                  }).error(function (response) {
                    if (response.error.message) {
                      if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
                        $window.localStorage.removeItem("userInfo");
                        $rootScope.validateSession();
                      } else if (response.error.message == 'Session Expired') {
                        $rootScope.validateSession();
                      }
                    }
                  });
                };

                var pwdMatch;
                if(hudUserCheck == true){
                  pwdMatch = true;
                  $scope.updateUser.password = $scope.hudPwd;
                  $scope.updateUser.userType = 'HudUser';

                }else{
                  if ($scope.updateUser.password) {
                    pwdMatch = $scope.updateUser.password.match(pwdCheck);
                  }
                  $scope.updateUser.userType = 'NonHudUser';
                }

                if ((pwdMatch && pwdMatch != false)  || hudUserCheck == false) {
                  userEditFunction();
                } else {
                  $scope.userCreationError = true;
                  $scope.createUserError = 'Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters';
                }
              } else {
                $scope.userCreationError = true;
                $scope.createUserError = 'Please enter a valid Email';
                //alert("Email FALSE");
              }
            }else {
              $scope.userCreationError = true;
              $scope.createUserError = 'Please Select User Status';
              //alert("Email FALSE");
            }
          } else {
            $scope.userCreationError = true;
            $scope.createUserError = 'Please Select atleast one Role';
            //alert("Role FALSE");
          }
        } else {
          $scope.userCreationError = true;
          $scope.createUserError = 'Please Select Application Name';
          //alert("AgencyId FALSE");
        }
      }else{
        $scope.userCreationError = true;
        $scope.createUserError = 'Please enter the Last Name (Alphabets only)';
        //alert("Name FALSE");
      }
    }else{
      $scope.userCreationError = true;
      $scope.createUserError = 'Please enter the First Name (Alphabets and space only)';
      //alert("Name FALSE");
    }
  };

  $scope.cancelEdit = function(){
    $scope.userCreationError = false;
    $scope.createUserError = '';
    $scope.getUsers();
    $('#userEdit').modal('hide');
  };

  $scope.deletePopup = function(userInfo, status){
    var editUserInfo = userInfo;
    $scope.deleteUserId = editUserInfo.id;
    $scope.disableUserInfo = editUserInfo;
    $scope.userName=editUserInfo.email;
    if(status == 'enable'){
      $scope.enableStatus = true;
    }else if(status == 'disable'){
      $scope.enableStatus = false;
    }

  };

  $scope.disableUser = function(){
    if($scope.enableStatus == true){
      $scope.disableUserInfo.status = 'Active';
    }else if($scope.enableStatus == false){
      $scope.disableUserInfo.status = 'Inactive';
    }
    $http({
      method: 'PUT',
      url: 'api/HpcsUsers/' + $scope.disableUserInfo.id + '?access_token=' + accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data: $scope.disableUserInfo
    }).success(function (response) {
      $scope.getUsers();
      $('#userDelete').modal('hide');
    }).error(function (response) {
      if (response.error.message) {
        if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        } else if (response.error.message == 'Session Expired') {
          $rootScope.validateSession();
        }
      }
    });
  };

  $scope.deleteUser = function(){
    $http({
      method: 'DELETE',
      url: 'api/HpcsUsers/'+$scope.deleteUserId+'?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.getUsers();
      $('#userDelete').modal('hide');
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  //$scope.reset();

  //*************************create user**************************
});
//**********************Role Create Code *********************************
app.controller('rolesController', function($scope,$http,$rootScope, $window) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
  });

  //$rootScope.validateAccessToken();

  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.getRoles = function() {
    $http({
      method: 'GET',
      url: 'api/HpcsRoles?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $rootScope.rolesData = response;

      if($rootScope.rolesData.length > 0){
        for(var i=0; i < $rootScope.rolesData.length; i++){
          if($rootScope.rolesData[i].name == 'Super Admin'){
            $rootScope.rolesData.splice(i,1);
          }
        }
      }
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };
  $scope.getRoles();

  $scope.reset = function() {
    $scope.user = angular.copy($scope.master);
  };
  $scope.role = {};
  var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
  $scope.createRole = function() {
    $scope.roleCreationError = true;
    $scope.createRoleError = '';
    if($scope.role.name.match(alphabetsWithSpaces)){
      //alert("Name TRUE");
      $http({
        method: 'POST',
        url: 'api/HpcsRoles?access_token='+accessToken,
        headers: {"Content-Type": "application/json", "Accept": "application/json"},
        data: $scope.role
      }).success(function (response) {
        $rootScope.rolesData.push(response);
        $scope.role ={};
        $('#myModal').modal('hide');
      }).error(function (response) {
        $scope.roleCreationError = true;
        $scope.createRoleError = 'Role Already Exist';
        if(response.error.message){
          if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
            $window.localStorage.removeItem("userInfo");
            $rootScope.validateSession();
          }else if(response.error.message == 'Session Expired'){
            $rootScope.validateSession();
          }
        }

      });
    }else{
      $scope.roleCreationError = true;
      $scope.createRoleError = 'Please enter the name (Alphabets and Space only)';
      //alert("Name FALSE");
    }
  };

  $scope.updateRole = {};
  $scope.editPopup = function(roleInfo){
    $scope.updateRole = roleInfo;
  };

  $scope.editRole = function(){
    $scope.roleCreationError = true;
    $scope.createRoleError = '';
    if($scope.updateRole.name.match(alphabetsWithSpaces)){
      $http({
        method: 'PUT',
        url: 'api/HpcsRoles/'+$scope.updateRole.id+'?access_token='+accessToken,
        headers: {"Content-Type": "application/json", "Accept": "application/json"},
        data: $scope.updateRole
      }).success(function (response) {
        $scope.updateRole = {};
        $scope.getRoles();
        $('#roleEdit').modal('hide');
      }).error(function (response) {
        if(response.error.message){
          if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
            $window.localStorage.removeItem("userInfo");
            $rootScope.validateSession();
          }else if(response.error.message == 'Session Expired'){
            $rootScope.validateSession();
          }
        }
      });
    }else{
      $scope.roleCreationError = true;
      $scope.createRoleError = 'Please enter the name (Alphabets and space only)';
    }
  };

  $scope.cancelEdit = function(){
    $scope.getRoles();
    $('#roleEdit').modal('hide');
  };

  $scope.deletePopup = function(roleInfo){
    $scope.deleteRoleId = roleInfo.id;
    $scope.roleName=roleInfo.name;
    $rootScope.delRole = true;
    $http({
      method:'GET',
      url: 'api/HpcsUsers?filter={"where":{ "role":"'+$scope.roleName+'"}}&access_token='+accessToken,
      headers:{"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      if(response.length>0){
        $rootScope.delRole = false;
      }

    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

  };

  $scope.deleteRole = function(){
    $http({
      method: 'DELETE',
      url: 'api/HpcsRoles/'+$scope.deleteRoleId+'?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.getRoles();
      $('#roleDelete').modal('hide');
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $scope.reset();
});

//******************************Application Configuration Code*********************************************
app.controller('AppConfigController', function($scope,$http,$rootScope, $window, $routeParams) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
  });

  //$rootScope.validateAccessToken();

  var accessToken = $window.localStorage.getItem('accessToken');
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  var url;

  $scope.getApps = function(appId) {

    if(appId){
      url = 'api/ApplicationConfigurations/'+appId+'?access_token=' + accessToken;
    }else {
      if ($rootScope.superAdminLogin == true) {
        url = 'api/ApplicationConfigurations?access_token=' + accessToken;
      } else {
        url = 'api/ApplicationConfigurations?filter={"where":{"name":"' + $rootScope.userInfo.applicationName + '"}}&access_token=' + accessToken;
      }
    }

    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $rootScope.appsData = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  if($routeParams.tcsAppId){
    $scope.showAllApps = false;
    $scope.getApps($routeParams.tcsAppId);
  }else{
    $scope.showAllApps = true;
    $scope.getApps();
  }

  $scope.reset = function() {
    $scope.user = angular.copy($scope.master);
  };
  var alphabetsWithSpaces = /^[a-zA-Z ]*$/;
  var pwdCheck = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;
  var ipFormat =/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

  $scope.whitelistedIPs = '';
  $scope.app = {
    whitelistedIPs : []
  };

  $scope.createPopup = function(appInfo) {
    $scope.appCreationError = false;
    $scope.createAppError = '';
    $scope.whitelistedIPs = '';
    $scope.app = {
      whitelistedIPs : []
    };
  };

  $scope.createApp = function() {
    var whitelistIps,checkIp = true;
    if($scope.whitelistedIPs.length >0) {
      if ($scope.whitelistedIPs.indexOf(',') > -1){
        whitelistIps = $scope.whitelistedIPs.split(',');
      }else{
        whitelistIps = [];
        whitelistIps.push($scope.whitelistedIPs);
      }
    }
    if($scope.whitelistedIPs.length > 0){
      for(var i = 0; i < whitelistIps.length; i++){
        var validIp = checkIPFormat(whitelistIps[i]);
        if(validIp == false){
          checkIp = false;
          break;
        }
      }
      $scope.app.whitelistedIPs = whitelistIps;
    }
    $scope.appCreationError = false;
    $scope.createAppError = '';
    if(checkIp == true) {
      if (checkOnlyNumbers($scope.app.agencyId) && $scope.app.agencyId.length <= 21) {
        if (checkAlphaNumeric($scope.app.tcsAppId) && $scope.app.tcsAppId.length <= 30) {
          if ($scope.app.name.match(alphabetsWithSpaces) && $scope.app.name.length <= 80) {
            if (checkAlphaNumeric($scope.app.applicationCode) && $scope.app.applicationCode.length <= 30) {
              if ($scope.app.retpmtCfmURL) {
                if (checkUrlFormat($scope.app.retpmtCfmURL)) {
                  //if ($scope.app.whitelistedIPs.match(ipFormat)) {
                  if ($scope.app.resultCfmURLs) {
                    if (checkUrlFormat($scope.app.resultCfmURLs)) {
                      if ($scope.app.settlementURL) {
                        if (checkUrlFormat($scope.app.settlementURL)) {
                          if (checkAlphaNumeric($scope.app.machineId)) {
                            if ($scope.app.password.match(pwdCheck) && $scope.app.password.length >= 8) {
                              $http({
                                method: 'POST',
                                url: 'api/ApplicationConfigurations?access_token=' + accessToken,
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.app
                              }).success(function (response) {
                                $rootScope.appsData.push(response);
                                $scope.app = {};
                                $('#myModal').modal('hide');
                              }).error(function (response) {
                                if (response.error.message) {
                                  if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
                                    $window.localStorage.removeItem("userInfo");
                                    $rootScope.validateSession();
                                  } else if (response.error.message == 'Session Expired') {
                                    $rootScope.validateSession();
                                  } else {
                                    $scope.createAppError = response.error.message;
                                    $scope.appCreationError = true;
                                  }
                                }
                              });
                            } else {
                              $scope.createAppError = 'Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters';
                              $scope.appCreationError = true;
                            }
                          } else {
                            $scope.createAppError = 'Please enter the MachineId (Alphanumerics only)';
                            $scope.appCreationError = true;
                          }
                        } else {
                          $scope.createAppError = 'Please enter the SettlementURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)';
                          $scope.appCreationError = true;
                        }
                      } else {
                        $scope.createAppError = 'Please enter the Settlement URL';
                        $scope.appCreationError = true;
                      }
                    } else {
                      $scope.createAppError = 'Please enter the resultCfmURLs in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)';
                      $scope.appCreationError = true;
                    }
                  } else {
                    $scope.createAppError = 'Please enter the resultCfmURLs';
                    $scope.appCreationError = true;
                  }
                } else {
                  $scope.createAppError = 'Please enter the retpmtCfmURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)';
                  $scope.appCreationError = true;
                }
              } else {
                $scope.createAppError = 'Please enter the retpmtCfmURL';
                $scope.appCreationError = true;
              }
            } else {
              $scope.createAppError = 'Please enter the Application Code in the AlphaNumeric format, Max length 30 characters';
              $scope.appCreationError = true;
            }
          } else {
            $scope.createAppError = 'Please enter the Application Name in the required format(Alphabets and Space only)';
            $scope.appCreationError = true;
          }
        } else {
          $scope.createAppError = 'Please enter the TcsAppId in the AlphaNumeric format';
          $scope.appCreationError = true;
        }
      } else {
        $scope.createAppError = 'Please enter the AgencyId in the Numeric format only, Max length 21 characters';
        $scope.appCreationError = true;
      }
    }else {
      $scope.createAppError = 'Please enter WhitelistIPs in IP format only(Ex:127.0.0.1)';
      $scope.appCreationError = true;
    }
  };

  $scope.updateApp = {
    whitelistedIPs : []
  };

  $scope.editPopup = function(appInfo){
    $scope.appCreationError = false;
    $scope.createAppError = '';
    $scope.whitelistedIPs = '';
    $scope.updateApp = appInfo;
    if($scope.updateApp.whitelistedIPs.length > 0){
      $scope.whitelistedIPs = $scope.updateApp.whitelistedIPs[0];
      for(var i=1;i<$scope.updateApp.whitelistedIPs.length;i++){
        $scope.whitelistedIPs = $scope.whitelistedIPs+','+$scope.updateApp.whitelistedIPs[i];
      }
    }
  };

  $scope.editApp = function() {

    $scope.appCreationError = false;
    $scope.createAppError = '';
    var whitelistIps,checkIp = true;

    if($scope.whitelistedIPs.length >0) {
      if ($scope.whitelistedIPs.indexOf(',') > -1){
        whitelistIps = $scope.whitelistedIPs.split(',');
      }else{
        whitelistIps = [];
        whitelistIps.push($scope.whitelistedIPs);
      }
    }
    if($scope.whitelistedIPs.length > 0){
      for(var i = 0; i < whitelistIps.length; i++){
        var validIp = checkIPFormat(whitelistIps[i]);
        if(validIp == false){
          checkIp = false;
          break;
        }
      }
      $scope.updateApp.whitelistedIPs = whitelistIps;
    }

    if(checkIp == true) {
      if (checkOnlyNumbers($scope.updateApp.agencyId) && $scope.updateApp.agencyId.length <= 21) {
        if (checkAlphaNumeric($scope.updateApp.tcsAppId) && $scope.updateApp.tcsAppId.length <= 30) {
          if ($scope.updateApp.name.match(alphabetsWithSpaces) && $scope.updateApp.name.length <= 80) {
            if (checkAlphaNumeric($scope.updateApp.applicationCode) && $scope.updateApp.applicationCode.length <= 30) {
              if ($scope.updateApp.retpmtCfmURL) {
                if (checkUrlFormat($scope.updateApp.retpmtCfmURL)) {
                  //if ($scope.app.whitelistedIPs.match(ipFormat)) {
                  if ($scope.updateApp.resultCfmURLs) {
                    if (checkUrlFormat($scope.updateApp.resultCfmURLs)) {
                      if ($scope.updateApp.settlementURL) {
                        if (checkUrlFormat($scope.updateApp.settlementURL)) {
                          if (checkAlphaNumeric($scope.updateApp.machineId)) {
                            if ($scope.updateApp.password.match(pwdCheck) && $scope.updateApp.password.length >= 8) {
                              $http({
                                method: 'PUT',
                                url: 'api/ApplicationConfigurations/' + $scope.updateApp.id + '?access_token=' + accessToken,
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.updateApp
                              }).success(function (response) {
                                $scope.updateApp = {};
                                $scope.getApps();
                                $('#appEdit').modal('hide');
                              }).error(function (response) {
                                if (response.error.message) {
                                  if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
                                    $window.localStorage.removeItem("userInfo");
                                    $rootScope.validateSession();
                                  } else if (response.error.message == 'Session Expired') {
                                    $rootScope.validateSession();
                                  } else {
                                    $scope.createAppError = response.error.message;
                                    $scope.appCreationError = true;
                                  }
                                }
                              });
                            } else {
                              $scope.createAppError = 'Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters';
                              $scope.appCreationError = true;
                            }
                          } else {
                            $scope.createAppError = 'Please enter the MachineId (Alphanumerics only)';
                            $scope.appCreationError = true;
                          }
                        } else {
                          $scope.createAppError = 'Please enter the SettlementURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)';
                          $scope.appCreationError = true;
                        }
                      } else {
                        $scope.createAppError = 'Please enter the Settlement URL';
                        $scope.appCreationError = true;
                      }
                    } else {
                      $scope.createAppError = 'Please enter the resultCfmURLs in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)';
                      $scope.appCreationError = true;
                    }
                  } else {
                    $scope.createAppError = 'Please enter the resultCfmURLs';
                    $scope.appCreationError = true;
                  }
                } else {
                  $scope.createAppError = 'Please enter the retpmtCfmURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)';
                  $scope.appCreationError = true;
                }
              } else {
                $scope.createAppError = 'Please enter the retpmtCfmURL';
                $scope.appCreationError = true;
              }
            } else {
              $scope.createAppError = 'Please enter the Application Code in the AlphaNumeric format, Max length 30 characters';
              $scope.appCreationError = true;
            }
          } else {
            $scope.createAppError = 'Please enter the Application Name in the required format(Alphabets and space only, Max length 80 characters)';
            $scope.appCreationError = true;
          }
        } else {
          $scope.createAppError = 'Please enter the TcsAppId in the AlphaNumeric format, Max length 30 characters';
          $scope.appCreationError = true;
        }
      } else {
        $scope.createAppError = 'Please enter the AgencyId in the Numeric format only, Max length 21 characters';
        $scope.appCreationError = true;
      }
    }else {
      $scope.createAppError = 'Please enter WhitelistIPs in IP format only(Ex:127.0.0.1)';
      $scope.appCreationError = true;
    }

  };

  $scope.cancelEdit = function(){
    $scope.getApps();
    $('#appEdit').modal('hide');
  };

  $scope.deletePopup = function(appInfo){
    $scope.deleteAppId = appInfo.id;
    $scope.appName=appInfo.name;
    $rootScope.delApp = true;


    $http({
      method:'GET',
      url: 'api/HpcsUsers?filter={"where":{ "applicationName":"'+$scope.appName+'"}}&access_token='+accessToken,
      headers:{"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      if(response.length>0){
        $rootScope.delApp = false;
      }

    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $scope.deleteApp = function(){
    $http({
      method: 'DELETE',
      url: 'api/ApplicationConfigurations/'+$scope.deleteAppId+'?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.getApps();
      $('#appDelete').modal('hide');
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $scope.reset();
});




app.controller('lineController', function($scope, $window, $http, $rootScope, $location) {

  $scope.loadingImage = true;
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  var accessToken = $window.localStorage.getItem('accessToken');
  var url;
  $rootScope.getDashboardLineData = function() {
    $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
    accessToken = $window.localStorage.getItem('accessToken');
    //console.log(' HERE IN GET DASHBOARD LINE DATA ' + $rootScope.accessibleAppList);
    //console.log(' WINDOW STORAGE ' + $window.localStorage.getItem('accessibleApps'));
    if($rootScope.accessibleAppList == undefined){
      $rootScope.accessibleAppList = JSON.parse($window.localStorage.getItem('accessibleApps'));
      $rootScope.filterOption = JSON.parse($window.localStorage.getItem('filterOption'));
    }
    if ($rootScope.userInfo && accessToken) {
      if ($rootScope.userInfo && $rootScope.userInfo.tcsAppId) {
        url = 'api/Requests/getLineGraphCount?filterAppId=' + $rootScope.userInfo.tcsAppId + '&access_token=' + accessToken;
      } else {
        url = 'api/Requests/getLineGraphCount?access_token=' + accessToken;
      }
      $http({
        method: 'GET',
        url: url,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
        $rootScope.lineTransactionData = response.TransactionCount.transactionsData;
        $rootScope.lineDatesData = response.TransactionCount.dates;
        $scope.loadingImage = false;
        drawPlot();
      }).error(function (response) {
        if(response.error.message){
          if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
            $window.localStorage.removeItem("userInfo");
            $rootScope.validateSession();
          }else if(response.error.message == 'Session Expired'){
            $rootScope.validateSession();
          }
        }
      });
    }
  };

  var ssoMessage= $window.localStorage.getItem('ssoMessage');
  if ($rootScope.userInfo && accessToken) {
    $rootScope.getDashboardLineData();
  }else if(ssoMessage == 'Enter Email' || ssoMessage == 'User Not Found'){
    $scope.showLoginError = true;

    if(ssoMessage == 'Enter Email') {
      $scope.ssoLoginError = 'UserÃ¢â‚¬â„¢s email not configured. Please contact HUD helpdesk.';
    }else if(ssoMessage == 'User Not Found'){
      $scope.ssoLoginError = 'User not registered in HPCS. Please contact HPCS administrator for registration.';
    }else if(ssoMessage == 'Certificate Not Found'){
      $scope.ssoLoginError = 'Authentication failed. Please contact HPCS administrator.';
    }
    $('#ssoError').modal('show');
    $window.localStorage.removeItem("ssoMessage");
  }

  $("#flot-example-2").bind("plothover", function (event, pos, item) {

    $("#tooltip").remove();
    if (item) {
      var x = item.datapoint[0].toFixed(2),y = item.datapoint[1];
      //alert( "" +item.series.label + " = " + y);

      showTooltip(item.pageX, item.pageY,item.series.label + " : " + y);
    }
  });

  $("#flot-example-2").bind("plotclick", function (event, pos, item) {

    $("#flot-example-2").bind("plothover", function (event, pos, item) {
      $("#tooltip").remove();
    });
    $("#tooltip").remove();
    if (item) {
      var currentDate = new Date().setHours(0,0,0,0);
      var clickedDate = currentDate - ((30-(item.datapoint[0]))*86400*1000);
      clickedDate = new Date(clickedDate);
      var clickedEndDate = new Date(clickedDate).setHours(23,59,59,999);
      clickedEndDate = new Date(clickedEndDate);
      var dateRange = [clickedDate, clickedEndDate];
      $window.localStorage.setItem('reportsDateRange', JSON.stringify(dateRange));

      if($rootScope.superAdminLogin == true){
        $location.url('/transactions');
      }else {
        if (item.series.label == 'Total') {
          $window.localStorage.removeItem('transactionStatus');
        } else {
          $window.localStorage.setItem('transactionStatus', item.series.label);
        }
        $location.url('/totaltransactions/'+$rootScope.userInfo.tcsAppId);
      }
    }
  });

  function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css( {
      position: 'absolute', display: 'none', top: y + 5, left: x + 5,
      border: '1px solid #fdd', padding: '2px','font-weight': 'bold', 'background-color': '#fee', opacity: 0.80
    }).appendTo("body").fadeIn(200);
  }

  function drawPlot() {
    $.plot($("#flot-example-2"),
      [
        {
          label: "Total",
          color: "orange",
          shadowSize: 0,
          data: $rootScope.lineTransactionData[0].data,
          lines: {show: true},
          points: {show: true}
        },
        {
          label: "Initiated",
          color: "#9bc747",
          shadowSize: 0,
          data: $rootScope.lineTransactionData[1].data,
          lines: {show: true},
          points: {show: true}
        },
        {
          label: "Submitted",
          color: "#208ed3",
          shadowSize: 0,
          data: $rootScope.lineTransactionData[2].data,
          lines: {show: true},
          points: {show: true}
        },
        {
          label: "Settled",
          color: "#A216BF",
          shadowSize: 0,
          data: $rootScope.lineTransactionData[3].data,
          lines: {show: true},
          points: {show: true}
        },
        {
          label: "Cancelled",
          color: "#778899",
          shadowSize: 0,
          data: $rootScope.lineTransactionData[4].data,
          lines: {show: true},
          points: {show: true}
        },
        {
          label: "Failed",
          color: "#FA150A",
          shadowSize: 0,
          data: $rootScope.lineTransactionData[5].data,
          lines: {show: true},
          points: {show: true}
        }
      ],
      {
        xaxis: {
          ticks: $rootScope.lineDatesData
        },

        grid: {
          borderWidth: 0,
          color: "#aaa",
          clickable: "true"
        }
      }
    );
  }

  $rootScope.setAppLevelApplication = function(selectedApp) {
    //console.log(' SELECTED APP ' + JSON.stringify(selectedApp) );
    $rootScope.filterOption = selectedApp;
    //$scope.selectedApplication = document.getElementById('appFilterOptions').value;
    if(selectedApp != null){
      $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
      $rootScope.userInfo.tcsAppId = selectedApp.id;
      $rootScope.userInfo.applicationName = selectedApp.name;
      $window.localStorage.setItem('filterOption',JSON.stringify(selectedApp));
      $window.localStorage.setItem('userInfo',JSON.stringify($rootScope.userInfo));
      //console.log('SET APP LEVEL APPLICATION :::::::: ' + JSON.stringify($rootScope.userInfo));
      $rootScope.getDashboardLineData();
    }
  }
});

app.factory('Excel',function($window){
  var uri='data:application/vnd.ms-excel;base64,',
    template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
    base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
    format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
  return {
    tableToExcel:function(tableId,worksheetName){
      var table=$(tableId),
        ctx={worksheet:worksheetName,table:table.html()},
        href=uri+base64(format(template,ctx));
      return href;
    }
  };
});
//**********************GET ALL THE ACCESSIBLE APPS AFTER LOGIN******************************//
function getAccessibleApps(userData,scope,rootScope,window) {
  //alert("userData.applicationName.length  "+userData.applicationName.length);
  //alert("userData.appId.length "+ JSON.stringify(userData));

  var accessibleAppList = [];
  //var accessibleAPPIds = [];
  if (userData != null && userData.applicationName != null && userData.applicationId != null && userData.applicationName.length === userData.applicationId.length) {
    rootScope.filterOption = {"id" : userData.applicationId[0], "name" : userData.applicationName[0] };
    for (var a = 0; a < userData.applicationName.length; a++) {
      //  alert("Pushing to list "+userData.applicationId[a]);
      accessibleAppList.push({"id" : userData.applicationId[a], "name" : userData.applicationName[a] });
    }
    //first time set the userInfo to first option in accessable apps
    rootScope.userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
    rootScope.userInfo.tcsAppId = userData.applicationId[0];
    //$rootScope.userInfo.applicationName = userData.applicationName[0];
    window.localStorage.setItem('userInfo',JSON.stringify(rootScope.userInfo));
    window.localStorage.setItem('accessibleApps', JSON.stringify(accessibleAppList));
    window.localStorage.setItem('filterOption', JSON.stringify(rootScope.filterOption));
    //console.log('USER INFO AFTER UPDATE :::::::: ' + JSON.stringify(rootScope.userInfo));

  }
  return accessibleAppList;
}

//************* Validations *************
function checkOnlyAlphabets(checkData){
  var onlyAlphabets = /^[A-Za-z]+$/;
  if(checkData.match(onlyAlphabets)){
    return true;
  }else{
    ////alert('Entered');
    return false;
  }
}

function checkOnlyNumbers(checkData){
  var onlyNumbers = /^[0-9]+$/;
  if(checkData.match(onlyNumbers)){
    return true;
  }else{
    ////alert('Entered');
    return false;
  }
}

function checkOnlyAlphaNumeric(checkData){
  var onlyAlphaNumeric = /^[0-9a-zA-Z]+$/;
  if(checkData.match(onlyAlphaNumeric)){
    return true;
  }else{
    ////alert('Entered');
    return false;
  }
}

function checkMailFormat(checkData){
  var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if(checkData.match(mailFormat)){
    return true;
  }else{
    //////alert('Entered');
    return false;
  }
}

function checkUrlFormat(checkData){
  //var urlFormat = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
  //........./^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test( value ).........
  return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(checkData);
}

//TODO: This is commented as per Vidya's input
//This function validates both IP4 and IP6 formats
function checkIPFormat(checkData){
  //var expression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))|(^\s*((?=.{1,255}$)(?=.*[A-Za-z].*)[0-9A-Za-z](?:(?:[0-9A-Za-z]|\b-){0,61}[0-9A-Za-z])?(?:\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|\b-){0,61}[0-9A-Za-z])?)*)\s*$)/;
  //return expression.test(checkData);
  return true;
}

function checkAlphaNumeric(checkData){
  var alphaNumeric =/^[a-zA-Z0-9]+$/;
  if(checkData.match(alphaNumeric)){
    return true;
  }else{
    return false;
  }
}

function generateHudPwd(){
  var chars = '#aA!';
  var mask = '';
  if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
  if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  if (chars.indexOf('#') > -1) mask += '0123456789';
  if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
  var result = '';
  var result1 = '';
  for (var i = 10; i > 0; --i)
  {
    var r =  mask[getRandomInt(1,99)];
    if(r != undefined)
      result += r;
    else
      r = mask[getRandomInt(1,99)];
  }
  return result;
}

function getRandomInt(min, max) {
  var byteArray = new Uint8Array(1);
  window.crypto.getRandomValues(byteArray);

  var range = max - min + 1;
  var max_range = 256;
  if (byteArray[0] >= Math.floor(max_range / range) * range)
    return getRandomInt(min, max);
  return min + (byteArray[0] % range);
}
//************* Validations *************


(function(){
//export html table to pdf, excel and doc format directive
  var exportTable = function() {
    var link = function ($scope, elm, attr) {
      $scope.$on('export-pdf', function (e, d) {
        elm.tableExport({type: 'pdf', escape: 'false'});
      });
      $scope.$on('export-excel', function (e, d) {
        elm.tableExport({type: 'excel', escape: false});
      });
      $scope.$on('export-doc', function (e, d) {
        elm.tableExport({type: 'doc', escape: false});
      });
      $scope.$on('export-xml', function (e, d) {
        elm.tableExport({type: 'xml', escape: false});
      });
    };
    return {
      restrict: 'C',
      link: link
    }
  };
  app.directive('exportTable', exportTable);
})();

app.controller('postTransactionsController', function($scope, $http, $window, Excel, $rootScope, $timeout, $location) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
  });

  //default initial values for pagination
  $scope.transactionsPerPage = 10;
  $scope.pageNumber = 1;

  $scope.orderByField = '- created_date';
  $scope.reverseSort = false;

  $scope.pagination = {
    current: 1
  };

  $scope.pageChanged = function(newPage) {
    $scope.pageNumber = newPage;
    getTransactions({"tcs_app_id":$rootScope.userInfo.tcsAppId});
  };


  //$rootScope.validateAccessToken();
  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.showReports = false;

  $scope.getApplications = function(filterApp){
    //$rootScope.validateAccessToken();
    var url;
    if(filterApp){
      url = 'api/ApplicationConfigurations?filter={"where":{"tcsAppId":"'+filterApp+'"}}&access_token='+accessToken;
    }else{
      url = 'api/ApplicationConfigurations?access_token='+accessToken;
    }
    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.applications = response;
      $scope.tcsAppId = $scope.applications[0].tcsAppId;
      $scope.settlementUrl = $scope.applications[0].settlementURL;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $(function() {
    $( "#startDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

    $( "#endDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });
  });
  $scope.showTransactions = false;
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  if($rootScope.userInfo && $rootScope.userInfo.tcsAppId){
    $scope.getApplications($rootScope.userInfo.tcsAppId);
  }else {
    $scope.getApplications();
  }
  //
  //var currentTime = new Date();
  //currentTime = currentTime.setHours(0,0,0,0);
  var dateRange = [];

  $scope.filterByDate = function(){
    $scope.dateSelectionError = false;

    $scope.startDate = $("#startDatePicker").datepicker("getDate");
    $scope.endDate = $("#endDatePicker").datepicker("getDate");

    $scope.selectedApplication = document.getElementById('appFilterOptions').value;
    if ($scope.startDate) {
      if ($scope.endDate) {

        if ($scope.startDate > $scope.endDate) {
          var tempDate;
          tempDate = $scope.endDate;
          $scope.endDate = $scope.startDate;
          $scope.startDate = tempDate;
        }

        //alert($scope.startDate +'........'+$scope.endDate);

        $scope.startDate = $scope.startDate.setHours(0,0,0,0);
        $scope.startDate = new Date($scope.startDate);
        $scope.endDate = $scope.endDate.setHours(23,59,59,59);
        $scope.endDate = new Date($scope.endDate);
        //alert($scope.startDate +'........'+$scope.endDate);

        dateRange = [];
        dateRange.push($scope.startDate);
        dateRange.push($scope.endDate);

        var filterArray = [];
        var url;

        filterArray.push({"created_date":{"between": dateRange}});

        if($scope.selectedApplication != ''){
          filterArray.push({"tcs_app_id":$scope.selectedApplication});
        }else if($rootScope.userInfo.tcsAppId != ''){
          $scope.selectedApplication = $rootScope.userInfo.tcsAppId;
          filterArray.push({"tcs_app_id":$rootScope.userInfo.tcsAppId});
        }

        getTransactionsCount(filterArray);
        getTransactions(filterArray);

      } else {
        $scope.dateSelectionError = true;
        $scope.dateError = 'Please Select End Date';
      }
    } else {
      $scope.dateSelectionError = true;
      $scope.dateError = 'Please Select Start Date';
    }
  };

  function getTransactions(filterArray){

    var appFilterArray = [];
    appFilterArray = appFilterArray.concat(filterArray);
    if($window.localStorage.getItem('reportsDateRange')){
      dateRange = JSON.parse($window.localStorage.getItem('reportsDateRange'));
      $window.localStorage.removeItem('reportsDateRange');
    }
    if(dateRange.length > 0){
      appFilterArray.push({"created_date":{"between": dateRange}});
    }
    getTransactionsCount(appFilterArray);
    $scope.pageNumber = $scope.pageNumber-1;
    var skip = $scope.transactionsPerPage * $scope.pageNumber;

    if(skip < 0)
      skip = 0;

    $http({
      method: 'GET',
      //url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(filterArray) + '}}&access_token='+accessToken,
      url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '},"order":"created_date desc","limit":'+ $scope.transactionsPerPage + ',"skip":"'+ skip +'"}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.selectedTransactions = [];
      $scope.showPostButton = true;
      $scope.showTransactions = true;
      $scope.totalTransactions = response;
    }).error(function (response) {

      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  }


  function getTransactionsCount(appFilterArray){
    $http({
      method: 'GET',
      url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '}}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.transactionsCount = Object.keys(response).length;
      console.log('TOTAL TRANSACTIONS COUNT METHOD ' + $scope.transactionsCount);
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

    if($scope.transactionsCount == 0){
      $scope.pageNumber = 0;
    }

  }

  $scope.postSelectedTransactions = function(chkboxName){
    var transactions="";
    for(var i=0;i<=$scope.selectedTransactions.length-2;i++)
    {
      transactions += $scope.selectedTransactions[i] + '&';
    }
    transactions += $scope.selectedTransactions[$scope.selectedTransactions.length-1];


    var postdata = {'transactions':transactions,'settlementurl':$scope.settlementUrl};
    var data = {};
    data.transactions = transactions;
    data.settlementurl = $scope.settlementUrl;

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/postQueryStatus',
      success: function(data) {
        if(data == 'OK') {
          $('#postTransactionSuccess').modal('show');
          $('#postTransactionError').modal('hide');
        }
        else {
          $('#postTransactionError').modal('show');
          $('#postTransactionSuccess').modal('hide');
        }
      }
    });

  };

  $scope.selectedTransactions = [];
  $scope.showPostButton = false;
  $scope.getCheckboxStatus = function(transaction){
    var request = JSON.parse(transaction.request);
    var amount = request['ACHDebit'].transaction_amount;
    var postButton = document.getElementById('postTransactionButton');
    if(postButton != null || postButton != undefined )
      postButton.disabled = true;
    var checked = document.getElementById(transaction.id).checked;
    var reqString;
    if(transaction.status == 'Failed'){
      reqString =  'agency_id=' + request.agency_id + '&agency_tracking_id=' + transaction.agency_tracking_id
        + '&transaction_status=' + transaction.status +'&payment_amount=' + amount + '&payment_type=' + transaction.operationName
        + '&deposit_ticket_number=-1&debit_voucher_number=-1&effective_date=0';
    }else{
      reqString =  'agency_id=' + request.agency_id + '&agency_tracking_id=' + transaction.agency_tracking_id + '&paygov_tracking_id=' + transaction.paygov_tracking_id
        + '&transaction_status=' + transaction.status +'&payment_amount=' + amount + '&payment_type=' + transaction.operationName
        + '&deposit_ticket_number=-1&debit_voucher_number=-1&effective_date=0';
    }
    if(checked == true){
      $scope.selectedTransactions.push(reqString);
    }else if (checked == false) {
      var index = $scope.selectedTransactions.indexOf(reqString);
      if (index > -1) {
        $scope.selectedTransactions.splice(index, 1);
      }
    }

    if($scope.selectedTransactions.length > 0){
      $scope.showPostButton = false;
    }else{
      $scope.showPostButton = true;
    }
    if(postButton != null || postButton != undefined)
      postButton.disabled = false;
  };
});


app.controller('rePostTransactionsController', function($scope, $http, $window, Excel, $rootScope, $timeout, $location) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
  });

  //default initial values for pagination
  $scope.transactionsPerPage = 10;
  $scope.pageNumber = 1;

  $scope.orderByField = '- created_date';
  $scope.reverseSort = false;

  $scope.pagination = {
    current: 1
  };

  $scope.pageChanged = function(newPage) {
    $scope.pageNumber = newPage;
    getFailedTransactions({"tcs_app_id":$scope.selectedApplication});
  };


  //$rootScope.validateAccessToken();
  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.showReports = false;

  $scope.getApplications = function(filterApp){
    //$rootScope.validateAccessToken();
    var url;
    if(filterApp){
      url = 'api/ApplicationConfigurations?filter={"where":{"tcsAppId":"'+filterApp+'"}}&access_token='+accessToken;
    }else{
      url = 'api/ApplicationConfigurations?access_token='+accessToken;
    }
    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.applications = response;
      $scope.tcsAppId = $scope.applications[0].tcsAppId;
      $scope.settlementUrl = $scope.applications[0].settlementURL;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $(function() {
    $( "#startDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

    $( "#endDatePicker" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });
  });
  $scope.showTransactions = false;
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  if($rootScope.userInfo && $rootScope.userInfo.tcsAppId){
    $scope.getApplications($rootScope.userInfo.tcsAppId);
  }else {
    $scope.getApplications();
  }
  //
  //var currentTime = new Date();
  //currentTime = currentTime.setHours(0,0,0,0);
  var dateRange = [];

  $scope.filterByDate = function(){
    $scope.dateSelectionError = false;

    $scope.startDate = $("#startDatePicker").datepicker("getDate");
    $scope.endDate = $("#endDatePicker").datepicker("getDate");

    $scope.selectedApplication = document.getElementById('appFilterOptions').value;
    if ($scope.startDate) {
      if ($scope.endDate) {

        if ($scope.startDate > $scope.endDate) {
          var tempDate;
          tempDate = $scope.endDate;
          $scope.endDate = $scope.startDate;
          $scope.startDate = tempDate;
        }

        //alert($scope.startDate +'........'+$scope.endDate);

        $scope.startDate = $scope.startDate.setHours(0,0,0,0);
        $scope.startDate = new Date($scope.startDate);
        $scope.endDate = $scope.endDate.setHours(23,59,59,59);
        $scope.endDate = new Date($scope.endDate);
        //alert($scope.startDate +'........'+$scope.endDate);

        dateRange = [];
        dateRange.push($scope.startDate);
        dateRange.push($scope.endDate);

        var filterArray = [];
        var url;

        filterArray.push({"created_date":{"between": dateRange}});

        if($scope.selectedApplication != ''){
          filterArray.push({"tcs_app_id":$scope.selectedApplication});
        }else if($rootScope.userInfo.tcsAppId != ''){
          $scope.selectedApplication = $rootScope.userInfo.tcsAppId;
          filterArray.push({"tcs_app_id":$rootScope.userInfo.tcsAppId});
        }
        filterArray.push({"status" : "Failed"})

        getFailedTransactionsCount(filterArray);
        getFailedTransactions(filterArray);

      } else {
        $scope.dateSelectionError = true;
        $scope.dateError = 'Please Select End Date';
      }
    } else {
      $scope.dateSelectionError = true;
      $scope.dateError = 'Please Select Start Date';
    }
  };

  function getFailedTransactions(filterArray){
    var appFilterArray = [];
    appFilterArray = appFilterArray.concat(filterArray);
    if($window.localStorage.getItem('reportsDateRange')){
      dateRange = JSON.parse($window.localStorage.getItem('reportsDateRange'));
      $window.localStorage.removeItem('reportsDateRange');
    }
    if(dateRange.length > 0){
      appFilterArray.push({"created_date":{"between": dateRange}});
    }
    appFilterArray.push({"status" : "Failed"});
    getFailedTransactionsCount(appFilterArray);
    $scope.pageNumber = $scope.pageNumber-1;
    var skip = $scope.transactionsPerPage * $scope.pageNumber;

    if(skip < 0)
      skip = 0;

    $http({
      method: 'GET',
      //url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(filterArray) + '}}&access_token='+accessToken,
      url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '},"order":"created_date desc","limit":'+ $scope.transactionsPerPage + ',"skip":"'+ skip +'"}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.selectedTransactions = [];
      $scope.showPostButton = true;
      $scope.showTransactions = true;
      $scope.totalTransactions = response;
    }).error(function (response) {

      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  }


  function getFailedTransactionsCount(appFilterArray){
    //console.log(JSON.stringify(appFilterArray));
    $http({
      method: 'GET',
      url: 'api/Requests?filter={"where":{"and":' + JSON.stringify(appFilterArray) + '}}&access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.transactionsCount = Object.keys(response).length;
      console.log('TOTAL TRANSACTIONS COUNT METHOD ' + $scope.transactionsCount);
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

    if($scope.transactionsCount == 0){
      $scope.pageNumber = 0;
    }

  }

  $scope.repostSelectedFailedTransactions = function(chkboxName){
    console.log($scope.selectedFailedTransactions);
    console.log($scope.selectedFailedTransactions.length);
    var failedTransactions="";
    for(var i=0;i<=$scope.selectedFailedTransactions.length-2;i++)
    {
      //console.log(" i= "+ i + "   " +$scope.selectedFailedTransactions[i]);
      failedTransactions += '"' +$scope.selectedFailedTransactions[i] + '",';
    }
    //console.log(" i= "+ ($scope.selectedFailedTransactions.length)-1 + "   " +$scope.selectedFailedTransactions[$scope.selectedFailedTransactions.length-1]);
    failedTransactions += '"'+ $scope.selectedFailedTransactions[$scope.selectedFailedTransactions.length-1] + '"';
    console.log(' FAILED TRANSACTIONS ' + failedTransactions);
    if($scope.selectedFailedTransactions.length > 0) {
      $http({
        method: 'POST',
        url: 'api/Requests/update?where={"hpcs_id":{"inq" : ['+failedTransactions+']}}&access_token='+accessToken,
        headers: {"Content-Type": "application/json", "Accept": "application/json"},
        data: {"status" : "Initiated","sent":"false"}
      }).success(function (response) {
        console.log(JSON.stringify(response));
        $('#repostTransactionSuccess').modal('show');
        $('#repostTransactionError').modal('hide');
      }).error(function (response) {
        if (response.error.message) {
          if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
            $window.localStorage.removeItem("userInfo");
            $rootScope.validateSession();
          } else if (response.error.message == 'Session Expired') {
            $rootScope.validateSession();
          }
        }else{
          $('#repostTransactionError').modal('show');
          $('#repostTransactionSuccess').modal('hide');
        }
      });
    }
    failedTransactions = "";
    $scope.selectedFailedTransactions = [];
    getFailedTransactions({"tcs_app_id":$scope.selectedApplication});
  };

  $scope.selectedFailedTransactions = [];
  $scope.showPostButton = false;
  $scope.getCheckboxStatus = function(transaction){
    var request = JSON.parse(transaction.request);
    //console.log(' TRANSACTION  *** '+ JSON.stringify(transaction));
    //console.log(' REQUEST  *** '+ JSON.stringify(request));
    var postButton = document.getElementById('postTransactionButton');
    if(postButton != null || postButton != undefined )
      postButton.disabled = true;
    var checked = document.getElementById(transaction.id).checked;
    var reqString;

    if(checked == true){
      $scope.selectedFailedTransactions.push(transaction.hpcs_id);
    }else if (checked == false) {
      var index = $scope.selectedFailedTransactions.indexOf(transaction.hpcs_id);
      if (index > -1) {
        $scope.selectedFailedTransactions.splice(index, 1);
      }
    }

    if($scope.selectedFailedTransactions.length > 0){
      $scope.showPostButton = false;
    }else{
      $scope.showPostButton = true;
    }
    if(postButton != null || postButton != undefined)
      postButton.disabled = false;
  };
});

app.controller('settlementReportStatusController', function($scope,$http,$rootScope, $window) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(function() {
    $( "#settlementDate" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

  });
  $(function() {
    $( "#settlementDate1" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

  });

  var xml = "<music><album>Beethoven</album><album>Beethoven</album></music>";
  var result = $(xml).find("album");
  console.log("result:::::: ",JSON.stringify(result));

  var accessToken = $window.localStorage.getItem('accessToken');
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));

  $scope.getSettlementReports = function() {

    if ($rootScope.superAdminLogin == true && $rootScope.adminLogin == false) {
      url = 'api/SettlementReportStatuses?access_token=' + accessToken;
    }

    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $rootScope.settlementReportData = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $scope.getSettlementReports();

  $scope.createSettlementReport = function() {

    $scope.settlementReportError = false;
    $scope.createSettlementReportError = '';

    if (checkOnlyNumbers($scope.settlementReport.agencyId) && $scope.settlementReport.agencyId.length <= 21) {
      if (checkAlphaNumeric($scope.settlementReport.tcsAppId) && $scope.settlementReport.tcsAppId.length <= 30) {

        $http({
          method: 'POST',
          url: 'api/SettlementReportStatuses?access_token=' + accessToken,
          headers: {"Content-Type": "application/json", "Accept": "application/json"},
          data: $scope.settlementReport
        }).success(function (response) {
          $rootScope.settlementReportData.push(response);
          $scope.settlementReport = {};
          $('#myModal').modal('hide');
        }).error(function (response) {
          if (response.error.message) {
            if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
              $window.localStorage.removeItem("userInfo");
              $rootScope.validateSession();
            } else if (response.error.message == 'Session Expired') {
              $rootScope.validateSession();
            } else {
              $scope.createSettlementReportError = response.error.message;
              $scope.settlementReportError = true;
            }
          }
        });

      } else {
        $scope.createSettlementReportError = 'Please enter the TcsAppId in the AlphaNumeric format';
        $scope.settlementReportError = true;
      }
    } else {
      $scope.createSettlementReportError = 'Please enter the AgencyId in the Numeric format only, Max length 21 characters';
      $scope.settlementReportError = true;
    }

  };

  $scope.updateSettlementReport = {};
  $scope.editPopup = function(userInfo){
    $scope.showPwdField = true;
    $scope.hudPwd = '';
    if(userInfo.userType == 'HudUser') {
      document.getElementById('settlementReportEdit').checked = true;
    }else{
      document.getElementById('settlementReportEdit').checked = false;
    }

    $scope.updateSettlementReport = userInfo;

  };

  $scope.editSettlementReport = function() {

    $scope.settlementReportError = false;
    $scope.createSettlementReportError = '';

    if (checkOnlyNumbers($scope.updateSettlementReport.agencyId) && $scope.updateSettlementReport.agencyId.length <= 21) {
      if (checkAlphaNumeric($scope.updateSettlementReport.tcsAppId) && $scope.updateSettlementReport.tcsAppId.length <= 30) {

        $http({
          method: 'PUT',
          url: 'api/SettlementReportStatuses/' + $scope.updateSettlementReport.id + '?access_token=' + accessToken,
          headers: {"Content-Type": "application/json", "Accept": "application/json"},
          data: $scope.updateSettlementReport
        }).success(function (response) {

          $scope.updateSettlementReport = {};
          $scope.getSettlementReports();
          $('#settlementReportEdit').modal('hide');
        }).error(function (response) {
          if (response.error.message) {
            if (response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
              $window.localStorage.removeItem("userInfo");
              $rootScope.validateSession();
            } else if (response.error.message == 'Session Expired') {
              $rootScope.validateSession();
            } else {
              $scope.createSettlementReportError = response.error.message;
              $scope.settlementReportError = true;
            }
          }
        });

      } else {
        $scope.createSettlementReportError = 'Please enter the TcsAppId in the AlphaNumeric format';
        $scope.settlementReportError = true;
      }
    } else {
      $scope.createSettlementReportError = 'Please enter the AgencyId in the Numeric format only, Max length 21 characters';
      $scope.settlementReportError = true;
    }

  };

  $scope.cancelEdit = function(){
    $scope.settlementReportError = false;
    $scope.createSettlementReportError = '';
    $scope.getSettlementReports();
    $('#settlementReportEdit').modal('hide');
  };



});
//Settlement Report controller
app.controller('settlementRepController', function($scope,$http,$rootScope, $window) {

  $scope.openTarget = function (id) {
    $('#'+id).toggle();
  }

  var accessToken = $window.localStorage.getItem('accessToken');
  $rootScope.userInfo = JSON.parse($window.localStorage.getItem('userInfo'));
  $scope.getSettlementRep = function() {
    if ($rootScope.superAdminLogin == true && $rootScope.adminLogin == false) {
      url = 'api/SettlementReports?access_token=' + accessToken;
    }

    $http({
      method: 'GET',
      url: url,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $rootScope.settlementRepData = response;
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };

  $scope.getSettlementRep();

  $scope.toggleFlag = function (id,flag) {
    $.blockUI();
    var f = flag;
    if(flag === "true")
      f = "false";
    else
      f = "true";

    $http({
      method: 'PUT',
      url: 'api/SettlementReports/'+id+'?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data: {"fileSentFlag":f}
    }).success(function (response) {
      $.unblockUI();
      $scope.getSettlementRep();
    }).error(function (response) {
      $.unblockUI();
      alert("Error while updating flag status");
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  }

});

var timePassed = function(interval){
  var _timeoutLast = Date.now();
  var _interval = interval;
  var _update = function(){_timeoutLast = Date.now();}
  return function(int){
    //console.log(_interval, (Date.now()-_timeoutLast))
    var bdiff = (int||_interval) <= (Date.now()-_timeoutLast);
    _update()
    return bdiff;
  }
}

//** schedulerStatusController**
app.controller('schedulerStatusController', function($scope,$http,$rootScope, $window) {
  $(document).ready(function () {
    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(function() {
    $( "#schedulerTS" ).datepicker({
      maxDate: 0,
      dateFormat: 'mm/dd/yy'
    });

  });

  //$rootScope.validateAccessToken();

  var accessToken = $window.localStorage.getItem('accessToken');
  $scope.getSchedulerStatus = function() {
    $http({
      method: 'GET',
      url: 'api/SchedulerFlags?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
      $scope.schedulersData = response;

    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });
  };
  $scope.getSchedulerStatus();

  $scope.reset = function() {
    $scope.user = angular.copy($scope.master);
  };


  $scope.updateSchedulerStatus = {};
  $scope.editPopup = function(schedulersInfo){
    $scope.updateSchedulerStatus = schedulersInfo;
  };

  $scope.editSchedulerStatus = function(){
    $scope.statusUpdateError = false;
    $scope.updateStatusError = '';

    $http({
      method: 'PUT',
      url: 'api/SchedulerFlags/'+$scope.updateSchedulerStatus.id+'?access_token='+accessToken,
      headers: {"Content-Type": "application/json", "Accept": "application/json"},
      data: $scope.updateSchedulerStatus
    }).success(function (response) {
      $scope.updateSchedulerStatus = {};
      $scope.getSchedulerStatus();
      $('#schedulerStatusEdit').modal('hide');
    }).error(function (response) {
      if(response.error.message){
        if(response.error.message == 'Invalid Access Token' || response.error.message == 'Authorization Required') {
          $window.localStorage.removeItem("userInfo");
          $rootScope.validateSession();
        }else if(response.error.message == 'Session Expired'){
          $rootScope.validateSession();
        }
      }
    });

  };

  $scope.cancelEdit = function(){
    $scope.getSchedulerStatus();
    $('#schedulerStatusEdit').modal('hide');
  };



  $scope.reset();
});
