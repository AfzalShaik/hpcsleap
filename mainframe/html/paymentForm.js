// create angular app
var validationApp = angular.module('validationApp', []);
validationApp.config(['$locationProvider',function($locationProvider){    
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
// create angular controller
validationApp.controller('mainController', function($scope,$http,$location) {
 alert("Inside main controller"); 
    $scope.user = {};
  // function to submit the form after all validation has occurred

  $scope.formNavStep2 = function() {
    $("#formNavStep1Div").hide();
    $("#formNavStep2Div").show();
    $("#formNavStep3Div").hide();

    $("#formNavStep1").removeClass("active-step");
    $("#formNavStep2").addClass("active-step");
    $("#formNavStep3").removeClass("active-step");

    // check to make sure the form is completely valid
    if ($scope.userForm.$valid) {
      //alert('Form is valid');
    }

  };
  $scope.formNavStep3 = function() {
    
    $("#formNavStep1Div").hide();
    $("#formNavStep2Div").hide();
    $("#formNavStep3Div").show();

    $("#formNavStep1").removeClass("active-step");
    $("#formNavStep2").removeClass("active-step");
    $("#formNavStep3").addClass("active-step");

    // check to make sure the form is completely valid
    if ($scope.userForm.$valid) {
      $scope.location = $location;
      alert('Form is valid'+JSON.stringify($scope.user));
      alert('tcsAppId '+$location);
      var searchObject = $location.search();
    alert("search obj "+JSON.stringify(searchObject));
    alert("parameter is :.."+searchObject.tcsAppId);
    alert("Url typed in the browser is: "+$location.absUrl());

      $http({
        method  : 'POST',
        url     : '/processPayment',
        data    :  $scope.user,  // pass in data as strings
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
      })
        .success(function(data) {
          console.log(data);

          if (!data.success) {
            // if not successful, bind errors to error variables

          } else {
            // if successful, bind success message to message

          }
        });
    }
  };
  
  $scope.formNavStep1 = function() {
    $("#formNavStep1Div").show();
    $("#formNavStep2Div").hide();
    $("#formNavStep3Div").hide();

    
    $("#formNavStep1").addClass("active-step");
    $("#formNavStep2").removeClass("active-step");
    $("#formNavStep3").removeClass("active-step");


  };

});
}])

validationApp.directive('validPasswordC', function() {
  return {
    require: 'ngModel',
    scope: {

      reference: '=validPasswordC'

    },
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue, $scope) {

        var noMatch = viewValue != scope.reference
        ctrl.$setValidity('noMatch', !noMatch);
        return (noMatch)?noMatch:!noMatch;
      });

      scope.$watch("reference", function(value) {;
        ctrl.$setValidity('noMatch', value === ctrl.$viewValue);

      });
    }
  }
});

