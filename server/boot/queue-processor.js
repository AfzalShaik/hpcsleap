var amqp = require('amqplib/callback_api');
var fs = require('fs');
var appServer = require('../../server/server');
var logger = require('./lib/logger');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./lib/hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

var rabbitmqInfo = serviceInfo.rabbitmq;
var rabbitmqUrl = 'amqp://' + rabbitmqInfo.hostname + ":"+ rabbitmqInfo.port;
var dbService = require("./database-services");
var hpcsIdGenerator = require("./lib/hpcs-id-generator");
var payGovClient = require("./paygov-client");

function amqpServer() {
  amqp.connect(rabbitmqUrl, function(err, conn) {
    /* istanbul ignore else*/
    if(conn) {
      conn.createChannel(function (err, ch) {
        var q = 'hpcs_queue';
        ch.assertQueue(q, {durable: false});
        logger.info(" [*] Waiting for messages in %s.", q);
        ch.consume(q, callPayGovClient, {noAck: true});
      });
    }
    else {
      setTimeout(function() {
        amqpServer()}, 20000
      );
    }
  });
}

function callPayGovClient(msg) {

  try {
    var message = msg.content.toString();
    logger.info('[y] Message Recieved %s:', message);
    var soap_req = JSON.parse(message);
    logger.info('Calling method callPayGov...');
    payGovClient.callPayGov(soap_req.service, soap_req.request, function (err, soap_res, err2) {
      /* istanbul ignore if*/
      if (err) {
        logger.info('Error in callPayGov',err);
      } else if(err2){
        var statusStr = 'Initiated';
        var returnDetails = 'Unable to perform the transaction';
        //If we get timeout error transaction should be in initiated status else in Failed status
        if(err2['code'] != null && err2['code'] == 'ETIMEDOUT')
        {
          logger.info("********The status is initiated********");
          statusStr = 'Initiated';
          returnDetails = 'Unable to perform the transaction';
        } else if (err2['root'] != null &&
          err2['root']['Envelope']['Body']['Fault']['faultstring'] == 'Schema validation error (Paygov).')
        {
          statusStr = 'Failed';
          returnDetails = 'Invalid Data';
          logger.info("*********The status is Failed******");
        }

        var operationName = soap_req.service.substring(soap_req.service.indexOf('/')+8);
        var hpcsId = hpcsIdGenerator.getAgencyTrackingId(soap_req.request, operationName);
        dbService.updateStatueForRequest(hpcsId, {
          status: statusStr,
          return_detail: returnDetails
        });

      } else {
        logger.info('Response from pay gov %s', JSON.stringify(soap_res));
        var operationName = soap_req.service.substring(soap_req.service.indexOf("/") + 8);
        updateStatusToDB(soap_res, operationName);
      }
    });
  }catch(err){
    /* istanbul ignore next */
    /* #361 bug istanbul*/
    (function() {
      logger.info("SEVERE: An error occurred while processing queue message", err);
    }());

  }
}

function updateStatusToDB(response, operationName){
  var Request = appServer.models.Request;
  logger.info('Updating the transaction status for operation %s ', operationName);
  var transactionStatus = '';
  var return_detail = '';
  var hpcsId;
  /* istanbul ignore else*/
  if(response != null && response != undefined) {

    // SingleService
    if(response[operationName] != undefined) {
      transactionStatus = response[operationName].transaction_status;
      // Override transaction status based on return_code.
      if(response['return_code'] != undefined
        && response['return_code'] >= 5000
        && response['return_code'] <= 5006){
        transactionStatus = 'Initiated';
      }
      hpcsId = response[operationName].agency_tracking_id;
      var paygov_tracking_id = "";
      if(operationName == "ACHCancel"){
        paygov_tracking_id = response[operationName].orig_paygov_tracking_id;
      } else {
        paygov_tracking_id = response[operationName].paygov_tracking_id;
      }
      dbService.updateStatueForRequest(hpcsId,
        { status: transactionStatus,
          paygov_tracking_id: paygov_tracking_id,
          return_detail: response[operationName].return_detail,
          response: JSON.stringify(response)
        }
      );
    }

    // SingleQuery
    else {
      var utils = require('./lib/hpcs-utils');
      result = utils.extractSingleQueryResponse(response);
      hpcsId = result.hpcs_id;
      transactionStatus = result.status;
      return_detail = result.detail;
      dbService.updateStatueForRequest(result.hpcs_id,
        { status: transactionStatus,
          return_detail: return_detail,
          agency_tracking_id: result.hpcs_id
        }
      );
    }
  }
  else{
    logger.error('Error in receving response from Pay gov. Transaction status not updated in DB');
  }

}

function pushMessageToQueue(data){
  logger.info('Connecting to Queue...');
  amqp.connect(rabbitmqUrl, function(err, conn) {
    if (conn) {
      conn.createChannel(function(err, ch) {
        var q = 'hpcs_queue';
        var msg = JSON.stringify(data);
        ch.assertQueue(q, {durable: false});
        ch.sendToQueue(q, new Buffer(msg));
        logger.info('[x] Message Send to Queue %s', msg);
      });
      setTimeout(function () {
        conn.close();
      }, 500);
    } else {
      /* istanbul ignore next*/
      setTimeout(function () {
        pushMessageToQueue(data);
      }, 20000);
    }
  });
}


// Runs queue-processor on domain, as it required new domain to handle system crash.
var domain = require('domain');
var d = domain.create();
d.run(function() {
  amqpServer();
});

// Handles all exceptions and avoids system crashes
/* istanbul ignore next*/
d.on('error', function(err) {
  logger.error(err);
});


exports.pushMessageToQueue = pushMessageToQueue;
exports.updateStatusToDB = updateStatusToDB;
exports.callPayGovClient = callPayGovClient;
