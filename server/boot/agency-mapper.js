var appServer = require('../../server/server');

function findAgencyByAppName(value, callback){
  var Agency = appServer.models.ApplicationConfiguration;
  Agency.find( {where : { name : value}}, function(err, results){
    /* Agency.find( {where : {and: [ { paygovAppName : value},{whitelistedIPs : ip}]}}, function(err, results){*/
    /* istanbul ignore if */
    if(err){
      callback(err);
    } else {
      callback(null, results[0]);
    }
  })
}

function findAgencyByTCSAppId(value, callback){
  var Agency = appServer.models.Agency;
  Agency.find( {where : { tcsAppID : value}}, function(err, results){
    /* istanbul ignore if */
    if(err){
      callback(err);
    } else {
      callback(null,results[0]);
    }
  })
}
function findAgencyByAppIdAndAgencyId(value1,value2, callback){
  var Agency = appServer.models.ApplicationConfiguration;
  var query1 = {};
  query1["tcsAppId"] = value1;
  var query2 = {};
  query2["agencyId"] = value2;
  console.log("q1",query1,query2);
  Agency.find({where: {and: [query1,query2]}}, function(err, results){
    /* istanbul ignore if */
    if(err){
      callback(err);
    } else {
      console.log("Results ",JSON.stringify(results[0]));
      callback(null, results[0]);
    }
  })
}

exports.findAgencyByAppName = findAgencyByAppName;
exports.findAgencyByTCSAppId = findAgencyByTCSAppId;
exports.findAgencyByAppIdAndAgencyId  = findAgencyByAppIdAndAgencyId;

