module.exports = function (server) {

  var remotes = server.remotes();

  remotes.before('**', function(ctx, next){
    var AppConfiguration = server.models.ApplicationConfiguration;
    if ((ctx.req.originalUrl.toString().indexOf('/api/HpcsUsers/login') > -1 ) || ((ctx.req.originalUrl.toString().indexOf('/api/AccessTokens') > -1) && ctx.req.method.toString() == 'POST')) {
      ctx.req.body['ttl'] = 15000;
     
      next();
    }else if((ctx.req.originalUrl.toString().indexOf('/api/HpcsUsers') > -1) && (ctx.req.method.toString() == 'POST' || ctx.req.method.toString() == 'PUT')){

      if(ctx.req.originalUrl.toString().indexOf('/api/HpcsUsers/logout') > -1) {
        next();
      }else{
        var roles = ctx.req.body.role;
        var roleError = false;
        for (var i = 0; i < roles.length; i++) {
          if (roles[i] == 'Super Admin') {
            roleError = true;
            break;
          }
        }

        if (roleError == true) {
          var roleErrorMessage = {message: 'Super Admin Cannot be Created'};
          next(roleErrorMessage, null);
        } else {
          AppConfiguration.find({"where": {"name": ctx.req.body.applicationName}}, function (err, appData) {
            if (err) {

            } else {
              if (appData.length > 0) {
                ctx.req.body['tcsAppId'] = appData[0].tcsAppId;
              }
            }
            next();
          });
        }
      }
    }else {
      next();
    }
  });

  remotes.afterError('**', function(ctx,next) {
    next();
  });

  remotes.after('**', function (ctx, next) {
    var HpcsUser = server.models.HpcsUser;
    var SessionInfo = server.models.SessionInfo;
    if (ctx.req.originalUrl.toString().indexOf('/api/HpcsUsers/login') > -1) {
      var expireTime = ctx.result.created.getTime() + (ctx.result.ttl * 1000);
      ctx.result['expireTime'] = expireTime;
      HpcsUser.findById(ctx.result.userId, function (err, hpcsUser) {
        if (err) {
          next(err, null);
        } else {
          var currentTime = new Date();
          ctx.result['userInfo'] = hpcsUser;
          SessionInfo.create({"accessToken":ctx.result.id,"lastAccessedTime":currentTime,"sessionDuration":(30*60*1000)},function(err,newSession){
            if(err){
              next(err,null);
            }else{
              next();
            }
          });
        }
      });

    }else if ((ctx.req.originalUrl.toString().indexOf('/api/AccessTokens') > -1) && ctx.req.method.toString() == 'POST') {
      var expireTime = ctx.result.created.getTime() + (ctx.result.ttl * 1000);
      ctx.result['expireTime'] = expireTime;
      next();
    } else {
      next();
    }

  });

};
