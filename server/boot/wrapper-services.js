var parseString = require('xml2js').parseString;
var appServer = require('../../server/server');
var logger = require('./lib/logger');
var fs = require('fs');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./lib/hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

var servicesList = serviceInfo.services;
var postfix = "Wrapper";
var hpcsIdGenerator = require("./lib/hpcs-id-generator");
var validator = require("./lib/validation/validator");
var dbService = require("./database-services");
var payGovClient = require('./paygov-client');
var queueProcessor = require('./queue-processor');

for(var service in servicesList) {
  var xml = fs.readFileSync('wsdls/' + service + '.wsdl', 'utf8');
  parseString(xml, function (err, result) {
    var clsName = result["wsdl:definitions"]["wsdl:service"][0]["$"]["name"];
    var portResult = result["wsdl:definitions"]["wsdl:portType"][0];
    var NewModel = appServer.model(clsName + postfix, {base:'Model', "public": true, "dataSource":null, "acls": [
      {
        "accessType": "*",
        "principalType": "ROLE",
        "principalId": "$authenticated",
        "permission": "ALLOW"
      },
      {
        "accessType": "*",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "DENY"
      }
    ]
    });
    var operations = portResult["wsdl:operation"];
    var mNameO = "";
    for (var op in operations) {
      var operation = operations[op];
      var operationName = operation["$"]["name"].toString();
      /* istanbul ignore next*/
      NewModel[operationName] = function (input, cb) {
        try {
          var serviceAction = clsName + '/' + mNameO;
          var operationName = mNameO.substr(7);
          var localPart = operationName + 'Request';
          var inputJson = JSON.parse(JSON.stringify(input));
          //check for the valid input request
          var isValidRequest = validator.validateInput(clsName,localPart, inputJson);
          if(isValidRequest) {
            var hpcsId = hpcsIdGenerator.generateHPCSId(input, operationName);
            var msg = {'service': serviceAction, 'request': input};
            dbService.isUniqueHPCSId(hpcsId, function (isUnique) {

              if(isUnique) {
                // Persist request into DB.
                dbService.persistMessageToDB(hpcsId, operationName, input, function(err, output){
                  if(output){
                    if(operationName == "TCSSingleQuery" && clsName == "TCSSingleQueryService"){
                      // Search for respective SingleService with TCSSingleQuery paygov_tracking_id.
                      dbService.fetchRequestFromHPCSId(input.paygov_tracking_id, function(err, request){

                        // If respective SingleService found
                        // then update TCSSingleQuery request with paygov_tracking_id & agency_tracking_id.
                        if(request != undefined && request != null){
                          input.paygov_tracking_id = request.paygov_tracking_id;
                          input.agency_tracking_id = request.hpcs_id;
                        }
                        payGovClient.callPayGov(serviceAction, input, function(err, response, err2){
                          if(err){
                            console.log(err);
                          } else if(err2){
                            cb(null, {
                              "hpcs_id": hpcsId,
                              "status": 'Failed',
                              "return_detail": 'Unable to perform operation'
                            });
                          } else {
                            result = require('./lib/hpcs-utils').extractSingleQueryResponse(response);
                            // Update TCSSingleQuery with status, return_details, tag & response.
                            dbService.updateStatueForRequest(hpcsId,{
                              status: result.status,
                              return_detail: result.return_detail,
                              tag: result.hpcs_id,
                              response : JSON.stringify(response)
                            });
                            // If respective SingleService found then
                            if(request != undefined && request != null) {
                              // 1. Update TCSSingleQuery with agency_tracking_id, paygov_tracking_id & Tag.
                              dbService.updateStatueForRequest(hpcsId, {
                                agency_tracking_id: request.agency_tracking_id,
                                paygov_tracking_id: request.paygov_tracking_id,
                                tag: request.hpcs_id
                              });
                              // 2. Update SingleService with latest paygov status and return_detail.
                              dbService.updateStatueForRequest(result.hpcs_id, {
                                status: result.status,
                                return_detail: result.return_detail
                              });
                            }

                            cb(null, {
                              "hpcs_id": result.hpcs_id,
                              "status": result.status,
                              "return_detail": result.return_detail
                            });
                          }
                        });
                      });
                    } else if(operationName == "ACHCancel"){
                      var paygov_tracking_id = input.ACHCancel.orig_paygov_tracking_id;
                      // Fetch actual paygov tracking id from DB.
                      dbService.fetchRequestFromHPCSId(paygov_tracking_id, function(err, result){
                        if(err ){
                          console.log(err );
                          cb(null, {"Error" : "Error while fetching data from database."});
                        } else if ( result == null){
                          dbService.updateStatueForRequest(hpcsId, {
                            status: "Failed",
                            return_detail: "There is no request with paygov_tracking_id provided."
                          });
                          cb(null, {
                            "hpcs_id": hpcsId,
                            "status": "Failed",
                            "return_detail": "There is no request with paygov_tracking_id provided."
                          });
                        } else if(result.paygov_tracking_id == undefined){
                          var msg = "Your request is still in process, Please try later";
                          dbService.updateStatueForRequest(hpcsId,{status: "Failed", return_detail: msg});
                          cb(null,{"hpcs_id": hpcsId, "status": "Failed", "return_detail": msg});
                        } else {
                          // Replace orig_paygov_tracking_id with actual_paygov_tracking_id.
                          dbService.updateStatueForRequest(hpcsId, {
                            agency_tracking_id : result.paygov_tracking_id,
                            tag: result.paygov_tracking_id
                          });
                          input.ACHCancel.orig_paygov_tracking_id = result.paygov_tracking_id;
                          input.ACHCancel.agency_tracking_id = result.hpcs_id;

                          payGovClient.callPayGov(serviceAction, input, function(err, response){
                            if(err){
                              console.log(err);
                            } else {
                              result = require('./lib/hpcs-utils').extractACHCancelResponse(response);
                              // Update status to ACHCancel request.
                              dbService.updateStatueForRequest(hpcsId, {
                                status: result.status,
                                return_detail: result.return_detail
                              });
                              // Update status to ACHDebt request if status is Canceled.
                              if(result.status == 'Canceled'){
                                dbService.updateStatueForRequest(result.hpcs_id, {
                                  status: result.status,
                                  return_detail: result.return_detail
                                });
                              }
                              cb(null, {
                                "hpcs_id": result.hpcs_id,
                                "status": result.status,
                                "return_detail": result.return_detail
                              });
                            }
                          });
                        }
                      })
                    } else if(operationName == "TCSSingleQuery" && clsName == "TCSSingleQueryServiceONAP"){
                      console.log("Inside TCSSingleQueryFor onap");
                      // Search for respective SingleService with TCSSingleQuery paygov_tracking_id.
                      dbService.fetchRequestFromHPCSId(input.paygov_tracking_id, function(err, request){

                        // If respective SingleService found
                        // then update TCSSingleQuery request with paygov_tracking_id & agency_tracking_id.
                        if(request != undefined && request != null){
                          input.paygov_tracking_id = request.paygov_tracking_id;
                          input.agency_tracking_id = request.hpcs_id;
                        }
                        payGovClient.callPayGov(serviceAction, input, function(err, response, err2){
                          if(err){
                            console.log(err);
                          } else if(err2){
                            cb(null, {
                              "hpcs_id": hpcsId,
                              "status": 'Failed',
                              "return_detail": 'Unable to perform operation'
                            });
                          } else {
                            result = require('./lib/hpcs-utils').extractSingleQueryONAPResponse(response);
                            // Update TCSSingleQuery with status, return_details, tag & response.
                            dbService.updateStatueForRequest(hpcsId,{
                              status: result.status,
                              return_detail: result.return_detail,
                              tag: result.hpcs_id,
                              response : JSON.stringify(response)
                            });
                            // If respective SingleService found then
                            if(request != undefined && request != null) {
                              // 1. Update TCSSingleQuery with agency_tracking_id, paygov_tracking_id & Tag.
                              dbService.updateStatueForRequest(hpcsId, {
                                agency_tracking_id: request.agency_tracking_id,
                                paygov_tracking_id: request.paygov_tracking_id,
                                tag: request.hpcs_id
                              });
                              // 2. Update SingleService with latest paygov status and return_detail.
                              dbService.updateStatueForRequest(result.hpcs_id, {
                                status: result.status,
                                return_detail: result.return_detail
                              });
                            }
                            cb(null, {
                              "hpcs_id": result.hpcs_id,
                              "status": result.status,
                              "return_detail": result.return_detail,
                              "paygov_tracking_id":result.paygov_tracking_id,
                              "agency_tracking_id":result.agency_tracking_id,
                              "transaction_amount":result.transaction_amount,
                              "transaction_date":result.transaction_date,
                              "effective_date":result.effective_date,
                              "HUD_Form":result.HUD_Form,
                              "Case_Number":result.Case_Number,
                              "Fee_Amount_for_Mortgage":result.Fee_Amount_for_Mortgage,
                              "Late_Charge_Due":result.Late_Charge_Due,
                              "Interest_Due_Values":result.Interest_Due_Values
                            });
                          }
                        });
                      });
                    }
                    else { // ACHDebit and ACHPrenotification.
                    
                        if (clsName == "TCSSingleServiceONLINE") {
                          console.log("Calling paygov client for online request.");
                          payGovClient.callPayGov(serviceAction, input, function (err, response) {
                            if (err) {
                              console.log(err);
                            } else {
                              if(response != null && response[operationName] != undefined) {
                                transactionStatus = response[operationName].transaction_status;
                                // Override transaction status based on return_code.
                                if (response['return_code'] != undefined
                                  && response['return_code'] >= 5000
                                  && response['return_code'] <= 5006) {
                                  transactionStatus = 'Initiated';
                                }
                                hpcsId = response[operationName].agency_tracking_id;
                                var paygov_tracking_id = "";
                                if (operationName == "ACHCancel") {
                                  paygov_tracking_id = response[operationName].orig_paygov_tracking_id;
                                } else {
                                  paygov_tracking_id = response[operationName].paygov_tracking_id;
                                }
                                var retDetail = response[operationName].return_detail;
                                // Update status to ACHDebit request.
                                dbService.updateStatueForRequestWithCB(
                                  hpcsId,
                                  {
                                    status: transactionStatus,
                                    paygov_tracking_id: paygov_tracking_id,
                                    return_detail: response[operationName].return_detail,
                                    response: JSON.stringify(response),                                    
                                    agency_tracking_id: hpcsId,
                                    request: JSON.stringify(msg.request),                                    
                                    tag: hpcsId
                                  },
                                  function (err, response) {
                                    if (err) {
                                      cb(new Error('Internal Server Error, Please try later.'));
                                    }
                                    else {
                                      //cb(null, {"hpcs_id": hpcsId, "status": "Initiated", "return_detail": returnMsg});
                                          cb(null, {"agency_tracking_id": hpcsId,
                                        "paygov_tracking_id":paygov_tracking_id,
                                        "status": transactionStatus, 
                                        "return_detail": retDetail});
                                    }
                                  }
                                );
                              }
                            }
                          }); 
 
                        }
                        else //Push onto queue
                        {
                           logger.info('Pushing message to Queue....');
                           var returnMsg = "Your request is in process";
                           var agency_tracking_id = msg.request[operationName].agency_tracking_id;
                           msg.request[operationName].agency_tracking_id = hpcsId;

                           dbService.updateStatueForRequestWithCB(
                             hpcsId,
                            {
                               agency_tracking_id : agency_tracking_id,
                               request: JSON.stringify(msg.request),
                               return_detail: returnMsg,
                               tag: hpcsId
                             },
                             function(err, response){
                               if(err){
                                 cb(new Error('Internal Server Error, Please try later.'));
                               }
                               else {
                                 queueProcessor.pushMessageToQueue(msg);
                                 cb(null,{"hpcs_id": hpcsId, "status": "Initiated", "return_detail": returnMsg});
                               }
                             }
                           );
                        }
                    }
                  }
                });
              } else {
                cb(null, {
                  "hpcs_id": hpcsId,
                  "status": "Failed",
                  "return_detail": "Non Unique Tracking Id : Verify your agency_tracking_id"
                });
              }
            });
          }
          else {
            cb(null, {
              "hpcs_id": hpcsId,
              "status": "Failed",
              "return_detail": "Your request is invalid and cannot be processed"
            });
          }
        }catch(err){
          console.log("Error ",err);
          cb(new Error('Internall Server Error, Please try later.'));
        }
      }

      NewModel.remoteMethod(operationName,{
        accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
        returns: {arg: 'result', type: 'body'}
      });

      // this Error message used for authorization of the user  @amit dwivedi
      var notApplicationUser = {
        "name": "Error",
        "status": 401,
        "message": "Application user Role is Required.",
        "statusCode": 401,
        "code": "AUTHORIZATION_REQUIRED"
      }

      var apiAccessError = {
        "name": "Error",
        "status": 401,
        "message": "Application user required permission to execute this Api.",
        "statusCode": 401,
        "code": "AUTHORIZATION_REQUIRED"
      }

      var noUser = {
        "name": "Error",
        "status": 401,
        "message": "No user with this access token was found.",
        "statusCode": 401,
        "code": "AUTHORIZATION_REQUIRED"
      }

      NewModel.beforeRemote('**', function (ctx, object, next) {
        var methodName = ctx.method.name;
        mNameO = methodName;
        if(ctx.req.accessToken.userId != undefined){
          appServer.models.HpcsUser.findById(ctx.req.accessToken.userId, function(err, user) {
            if (err) return next(err);
            if (!user) return next(apiAccessError, null);

            var roles = user.role;

            var api =  user.methodName;

            if (roles.indexOf("Application User") >= 0 ) {
              if (api.indexOf(methodName) < 0 )  return next(apiAccessError, null);
            } else {
              return next(notApplicationUser,null);
            }
          });
        }
        next();
      });
    }
    var NewTempModel = appServer.model("Temp", {base:'Model', "public": true, "dataSource":null});
  });
}

