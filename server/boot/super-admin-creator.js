var fs = require('fs');
var logger = require('./lib/logger');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./lib/hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

var appServer = require('../../server/server');
var HpcsUser = appServer.models.HpcsUser;
var superAdmin = serviceInfo.superAdminEmail;
var superAdminPwd = serviceInfo.superAdminPassword;
var HpcsRole = appServer.models.HpcsRole;
var settlementReport = appServer.models.SettlementReportStatus;
var AppConfig = appServer.models.ApplicationConfiguration;
var HpcsApi = appServer.models.HpcsApi;
var apis = serviceInfo.AccessibleAPIs;
var SchedulerFlags = appServer.models.SchedulerFlags;
require('events').EventEmitter.prototype._maxListeners = 100;

for(var i in apis) {
  var api = {"apiname" : apis[i]};
  HpcsApi.findOrCreate({"where":{"apiname":apis[i]}},api, function(err){
    /* istanbul ignore else */
    if(err){
      logger.error('Error creating API ' + err);
      throw err;
    } else {
      logger.info(' API created in DB - HpcsApi');
    }
  });
}

var roleRecord = {
  "name": "Super Admin",
  "description": "Super Admin for the application"
};


HpcsRole.findOrCreate({"where":{"name":"Super Admin"}},roleRecord, function(err){
  /* istanbul ignore else */
  if(err){
    logger.error('Error creating Super Admin Role ' + err);
    throw err;
  } else {
    logger.info(' Super Admin role created in DB');
  }
});

/*
 var SettlementStatusReport = appServer.models.SettlementReportStatus;
 settlementReport.find({}, function(err, settlementReport){
 if(settlementReport.length<1){
 AppConfig.find({}, function(err, response){
 var d = new Date();
 d.setDate(d.getDate()-1);
 //storing date in mm/dd/yyyy format
 d= (d.getMonth().toString().length==1)?("0"+(d.getMonth()+1))+"/"+d.getDate()+"/"+d.getFullYear():(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear();
 var obj = {
 "agencyId":"1546",
 "tcsAppId":"TCSHUDTID",
 "dateOfLastSuccessfullSettlementReport": d
 }
 SettlementStatusReport.create(obj);

 });
 }

 })
 */

var SettlementStatusReport = appServer.models.SettlementReportStatus;
var settlementReportQuery = {};
settlementReportQuery["tcsAppId"] = "TCSHUDFWEDH";
var d = new Date();
d.setDate(d.getDate()-1);
d= (d.getMonth().toString().length==1)?("0"+(d.getMonth()+1))+"/"+d.getDate()+"/"+d.getFullYear():(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear();
var settlementStatusRecord = {
  "agencyId":"1514",
  "tcsAppId":"TCSHUDFWEDH",
  "dateOfLastSuccessfullSettlementReport": d
}
/*SettlementStatusReport.upsertWithWhere(
 settlementReportQuery,
 settlementStatusRecord, function(err){

 if(err){
 logger.error('Error creating Super Admin User ' + err);
 throw err;
 } else {
 console.log('Settlement Report Status record for 1546 inserted');
 logger.info('Settlement Report Status record for 1546 inserted');
 }
 });
 */

SettlementStatusReport.findOrCreate({"where":{"tcsAppId":"TCSHUDFWEDH"}},settlementStatusRecord, function(err){
  if(err){
    logger.error('Error creating settlement status record for TCSHUDFWEDH ' + err);
    throw err;
  } else {
    logger.info(' Settlement status record created in DB');
    console.log(' Settlement status record  created in DB');
  }
});


var userRecord = {
  "name": "Admin",
  "applicationName": [
    "ALL"
  ],
  "role": [
    "Super Admin"
  ],
  "username": superAdmin,
  "password": superAdminPwd,
  "email": superAdmin,
  "status": "Active"
};

/*
 HpcsUser.findOrCreate({"where":{"username":superAdmin}},userRecord, function(err){
 if(err){
 logger.error('Error creating Super Admin User ' + err);
 throw err;
 } else {
 logger.info(' Super Admin user created in DB');
 console.log(' Super Admin user created ');
 }
 });
 */

// HpcsUser.findOrCreate replaced with upsertWithWhere in V1.1
// Creates new user if it doesn't exists; updates otherwise according to userRcord
var query = {};
query["username"] = superAdmin;
HpcsUser.upsertWithWhere(
  query,
  userRecord, function(err){
    /* istanbul ignore else */
    if(err){
      logger.error('Error creating Super Admin User ' + err);
      throw err;
    } else {
      logger.info('HPCS Admin user created in DB');
      console.log('HPCS Admin user created in DB');
    }
  });


var currDate = new Date();
var statusRecord = {
  "schedulerName":"status",
  "status": "No",
  "schedulerTS": currDate
};

var initiatedRecord = {
  "schedulerName":"initiated",
  "status": "No",
  "schedulerTS":currDate
};

var callbackRecord = {
  "schedulerName":"callback",
  "status": "No",
  "schedulerTS": currDate
};
var settlementPullRecord = {
  "schedulerName":"settlementPull",
  "status": "No",
  "schedulerTS": currDate
};
var settlementPostRecord = {
  "schedulerName":"settlementPost",
  "status": "No",
  "schedulerTS": currDate
};
SchedulerFlags.findOrCreate({"where":{"schedulerName":"status"}},statusRecord, function(err){

  if(err){
    logger.error('Error creating SchedulerFlags status' + err);
    throw err;
  } else {
    logger.info(' SchedulerFlags status created in DB');
  }
});

SchedulerFlags.findOrCreate({"where":{"schedulerName":"initiated"}},initiatedRecord, function(err){
  if(err){
    logger.error('Error creating SchedulerFlags initiated' + err);
    throw err;
  } else {
    logger.info(' SchedulerFlags initiated created in DB');
  }
});
SchedulerFlags.findOrCreate({"where":{"schedulerName":"callback"}},callbackRecord, function(err){
  if(err){
    logger.error('Error creating SchedulerFlags callback' + err);
    throw err;
  } else {
    logger.info(' SchedulerFlags callback created in DB');
  }
});

SchedulerFlags.findOrCreate({"where":{"schedulerName":"settlementPull"}},settlementPullRecord, function(err){
  if(err){
    logger.error('Error creating SchedulerFlags settlementPull' + err);
    throw err;
  } else {
    logger.info(' SchedulerFlags settlementPull created in DB');
  }
});

SchedulerFlags.findOrCreate({"where":{"schedulerName":"settlementPost"}},settlementPostRecord, function(err){
  if(err){
    logger.error('Error creating SchedulerFlags settlementPost' + err);
    throw err;
  } else {
    logger.info(' SchedulerFlags settlementPost created in DB');
  }
});

