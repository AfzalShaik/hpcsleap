function extractSingleQueryResponse(response){
  var result  = {
    hpcs_id : "",
    status : "",
    return_detail : "",
    paygov_tracking_id : ""
  };
  if(response != null || response != undefined) {

    var ach_results = response.tcs_ach_results;
    if (ach_results != undefined) {

      if (ach_results.ach_debit_query_response != undefined) {
        result.hpcs_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
        result.status = ach_results.ach_debit_query_response[0].transaction_status;
        result.return_detail = ach_results.ach_debit_query_response[0].return_detail;
        result.paygov_tracking_id = ach_results.ach_debit_query_response[0].paygov_tracking_id;

      } else if (ach_results.ach_prenotification_query_response != undefined) {
        result.hpcs_id = ach_results.ach_prenotification_query_response[0].agency_tracking_id;
        result.status = ach_results.ach_prenotification_query_response[0].transaction_status;
        result.return_detail = ach_results.ach_prenotification_query_response[0].return_detail;
        result.paygov_tracking_id = ach_results.ach_prenotification_query_response[0].paygov_tracking_id;

      } else if (ach_results.ach_cancel_query_response) {
        result.hpcs_id = ach_results.ach_cancel_query_response[0].agency_tracking_id;
        result.status = ach_results.ach_cancel_query_response[0].transaction_status;
        result.return_detail = ach_results.ach_cancel_query_response[0].return_detail;
        result.paygov_tracking_id = ach_results.ach_cancel_query_response[0].paygov_tracking_id;
      }
    }
    if (response.return_code != undefined) {
      console.log('Return_code : ' + response.return_code);
      if (response.return_code >= 4000 && response.return_code < 5000) {
        result.hpcs_id = response.agency_tracking_id;
        result.status = 'Failed';
        result.return_detail = response.return_detail;
      }
    }
  }

  return result;
}

function extractSingleQueryONAPResponse(response){
  var result  = {
    hpcs_id : "",
    status : "",
    return_detail : "",
    paygov_tracking_id : "",
    agency_tracking_id : "",
    transaction_amount:"",
    transaction_date:"",
    effective_date:"",
    HUD_Form:"" ,
    Case_Number:"",
    Fee_Amount_for_Mortgage:"",
    Late_Charge_Due:"",
    Interest_Due_Values:""
  };

  if(response != null || response != undefined) {

    var ach_results = response.tcs_ach_results;
    if (ach_results != undefined) {

      if (ach_results.ach_debit_query_response != undefined) {
        console.log("Response -------------> ",JSON.stringify(ach_results.ach_debit_query_response[0]));
        result.hpcs_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
        result.status = ach_results.ach_debit_query_response[0].transaction_status;
        result.return_detail = ach_results.ach_debit_query_response[0].return_detail;
        result.agency_tracking_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
        result.paygov_tracking_id = ach_results.ach_debit_query_response[0].paygov_tracking_id;
        result.transaction_amount = ach_results.ach_debit_query_response[0].transaction_amount;
        result.transaction_date = ach_results.ach_debit_query_response[0].transaction_date;
        var eff_date = ach_results.ach_debit_query_response[0].effective_date;
        if( eff_date != undefined && eff_date != null)
        {
          result.effective_date = eff_date;
        }
        var cust_fields = ach_results.ach_debit_query_response[0].custom_fields;
        if (cust_fields != undefined && cust_fields != null) {
          var cust_field1 = cust_fields.custom_field_1;
          var cust_field2 = cust_fields.custom_field_2;
          var cust_field3 = cust_fields.custom_field_3;
          var cust_field4 = cust_fields.custom_field_4;
          var cust_field5 = cust_fields.custom_field_5;
          if (cust_field1 != undefined && cust_field1 != null) {
            result.HUD_Form = cust_field1.value;
          }
          if (cust_field2 != undefined && cust_field2 != null) {
            result.Case_Number = cust_field2.value;
          }
          if (cust_field3 != undefined && cust_field3 != null) {
            result.Fee_Amount_for_Mortgage = cust_field3.value;
          }
          if (cust_field4 != undefined && cust_field4 != null) {
            result.Late_Charge_Due = cust_field4.value;
          }
          if (cust_field5 != undefined && cust_field5 != null) {
            result.Interest_Due_Values = cust_field5.value;
          }
        }

      } else if (ach_results.ach_prenotification_query_response != undefined) {
        result.hpcs_id = ach_results.ach_prenotification_query_response[0].agency_tracking_id;
        result.status = ach_results.ach_prenotification_query_response[0].transaction_status;
        result.return_detail = ach_results.ach_prenotification_query_response[0].return_detail;
        result.agency_tracking_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
        result.paygov_tracking_id = ach_results.ach_prenotification_query_response[0].paygov_tracking_id;
        result.transaction_amount = ach_results.ach_prenotification_query_response[0].transaction_amount;
        result.transaction_date = ach_results.ach_prenotification_query_response[0].transaction_date;
        var eff_date = ach_results.ach_debit_query_response[0].effective_date;
        if( eff_date != undefined && eff_date != null)
        {
          result.effective_date = eff_date;
        }
        var cust_fields = ach_results.ach_debit_query_response[0].custom_fields;
        if (cust_fields != undefined && cust_fields != null) {
          var cust_field1 = cust_fields.custom_field_1;
          var cust_field2 = cust_fields.custom_field_2;
          var cust_field3 = cust_fields.custom_field_3;
          var cust_field4 = cust_fields.custom_field_4;
          var cust_field5 = cust_fields.custom_field_5;
          if (cust_field1 != undefined && cust_field1 != null) {
            result.HUD_Form = cust_field1.value;
          }
          if (cust_field2 != undefined && cust_field2 != null) {
            result.Case_Number = cust_field2.value;
          }
          if (cust_field3 != undefined && cust_field3 != null) {
            result.Fee_Amount_for_Mortgage = cust_field3.value;
          }
          if (cust_field4 != undefined && cust_field4 != null) {
            result.Late_Charge_Due = cust_field4.value;
          }
          if (cust_field5 != undefined && cust_field5 != null) {
            result.Interest_Due_Values = cust_field5.value;
          }
        }

      } else if (ach_results.ach_cancel_query_response) {
        result.hpcs_id = ach_results.ach_cancel_query_response[0].agency_tracking_id;
        result.status = ach_results.ach_cancel_query_response[0].transaction_status;
        result.return_detail = ach_results.ach_cancel_query_response[0].return_detail;
        result.agency_tracking_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
        result.paygov_tracking_id = ach_results.ach_cancel_query_response[0].paygov_tracking_id;
        result.transaction_amount = ach_results.ach_cancel_query_response[0].transaction_amount;
        result.transaction_date = ach_results.ach_cancel_query_response[0].transaction_date;
        var eff_date = ach_results.ach_debit_query_response[0].effective_date;
        if( eff_date != undefined && eff_date != null)
        {
          result.effective_date = eff_date;
        }
        var cust_fields = ach_results.ach_debit_query_response[0].custom_fields;
        if (cust_fields != undefined && cust_fields != null) {
          var cust_field1 = cust_fields.custom_field_1;
          var cust_field2 = cust_fields.custom_field_2;
          var cust_field3 = cust_fields.custom_field_3;
          var cust_field4 = cust_fields.custom_field_4;
          var cust_field5 = cust_fields.custom_field_5;
          if (cust_field1 != undefined && cust_field1 != null) {
            result.HUD_Form = cust_field1.value;
          }
          if (cust_field2 != undefined && cust_field2 != null) {
            result.Case_Number = cust_field2.value;
          }
          if (cust_field3 != undefined && cust_field3 != null) {
            result.Fee_Amount_for_Mortgage = cust_field3.value;
          }
          if (cust_field4 != undefined && cust_field4 != null) {
            result.Late_Charge_Due = cust_field4.value;
          }
          if (cust_field5 != undefined && cust_field5 != null) {
            result.Interest_Due_Values = cust_field5.value;
          }
        }
      }
    }
    if (response.return_code != undefined) {
      console.log('Return_code : ' + response.return_code);
      if (response.return_code >= 4000 && response.return_code < 5000) {
        result.hpcs_id = response.agency_tracking_id;
        result.status = 'Failed';
        result.return_detail = response.return_detail;
      }
    }
  }

  return result;
}

function extractACHCancelResponse(response){
  var result  = {
    hpcs_id : "",
    status : "",
    return_detail : ""
  };
  console.log('extractACHCancelResponse');
  console.log(response.ACHCancel);
  if(response.ACHCancel != undefined){
    result.hpcs_id = response.ACHCancel.agency_tracking_id;
    result.status = response.ACHCancel.transaction_status;
    result.return_detail = response.ACHCancel.return_detail;
  }

  return result;
}
function extend(target) {
  var sources = [].slice.call(arguments, 1);
  sources.forEach(function (source) {
    for (var prop in source) {
      target[prop] = source[prop];
    }
  });
  return target;
}

function extractACHResponse(response){
  var result  = {
    hpcs_id : "",
    status : "",
    return_detail : "",
    paygov_tracking_id : ""
  };

  if(response != null || response != undefined) {
    console.log("Response -----> ",JSON.stringify(response));
    var parsedResponse = JSON.parse(JSON.stringify(response));
    console.log("parsedResponse ----->",parsedResponse.ACHDebit);
    var opResponse = parsedResponse.ACHDebit;
    console.log("paygov tracking id ",opResponse.paygov_tracking_id);

    result.hpcs_id = opResponse.agency_tracking_id;
    result.status = opResponse.transaction_status;
    result.return_detail = opResponse.return_detail;
    result.paygov_tracking_id = opResponse.paygov_tracking_id;

    if (opResponse.return_code != undefined) {
      console.log('Return_code : ' + opResponse.return_code);
      if (opResponse.return_code >= 4000 && opResponse.return_code < 5000) {
        result.hpcs_id = opResponse.agency_tracking_id;
        result.status = 'Failed';
        result.return_detail = opResponse.return_detail;
      }
    }
  }
  console.log("returning result ",JSON.stringify(result));
  return result;
}


exports.extend = extend;
exports.extractSingleQueryResponse = extractSingleQueryResponse;
exports.extractSingleQueryONAPResponse = extractSingleQueryONAPResponse;
exports.extractACHCancelResponse = extractACHCancelResponse
exports.extractACHResponse = extractACHResponse;

