var bunyan = require('bunyan');
var RotatingFileStream = require('bunyan-rotating-file-stream');
var fs = require('fs');

function reqSerializer(req) {
  return {
    method: JSON.stringify(req),
    headers: req.headers
  };
}

var logProperties = fs.readFileSync("./settings/logging.json");
var loggingInfo = JSON.parse(logProperties);
var LOG_PATH = loggingInfo.logging.path;
var LOG_LEVEL = loggingInfo.logging.level;

//"fatal" (60): The service/app is going to stop or become unusable now. An operator should definitely look into this soon.
//"error" (50): Fatal for a particular request, but the service/app continues servicing other requests. An operator should look at this soon(ish).
//"warn" (40): A note on something that should probably be looked at by an operator eventually.
//"info" (30): Detail on regular operation.
//"debug" (20): Anything else, i.e. too verbose to be included in "info" level.
//"trace" (10): Logging from external libraries used by your app or very detailed application logging.

module.exports = bunyan.createLogger({
  name: 'HPCS',
  serializers: {
    req: reqSerializer
  },
  streams: [
    {
      type: 'raw',
      level : LOG_LEVEL,
      stream: new RotatingFileStream({
        path: LOG_PATH,
        period: '1d',          // daily rotation
        totalFiles: 20,        // keep 20 back copies
        rotateExisting: true,  // Give ourselves a clean file when we start up, based on period
        threshold: '30m',      // Rotate log files larger than 20 megabytes
        totalSize: '600m',      // Don't keep more than 900mb of archived log files
        gzip: true             // Compress the archive log files to save space
      })
    }
  ]
});
