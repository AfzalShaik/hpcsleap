var fs = require('fs');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

var encryptionSecret = serviceInfo.encryptionKey;
var crypto = require('crypto');

var algorithm = 'aes256';
encrypt = function(key) {
  var cipher = crypto.createCipher(algorithm, encryptionSecret);
  var encrypted = cipher.update("" + key, 'utf8', 'hex') + cipher.final('hex');
  return encrypted;
};
decrypt = function(encryptedKey) {
  var decipher = crypto.createDecipher(algorithm, encryptionSecret);
  var decrypted = decipher.update(encryptedKey, 'hex', 'utf8') + decipher.final('utf8');
  return decrypted;
};

exports.encrypt = encrypt;
exports.decrypt = decrypt;
