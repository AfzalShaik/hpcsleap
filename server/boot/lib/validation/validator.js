var fs = require("fs");
var Ajv = require('ajv');
var Jsonix = require("jsonix").Jsonix;
var changeCase = require('change-case');
var logger = require('../logger');
var XMLSchemaJsonSchema = JSON.parse(fs.readFileSync('./node_modules/jsonix/jsonschemas/w3c/2001/XMLSchema.jsonschema').toString());
var JsonixJsonSchema = JSON.parse(fs.readFileSync('./node_modules/jsonix/jsonschemas/jsonix/Jsonix.jsonschema').toString());

var ajv = new Ajv();
ajv.addSchema(XMLSchemaJsonSchema);
ajv.addSchema(JsonixJsonSchema);

var changeCamelCase = function(obj){
  var object = {};
  for (var property in obj) {

    if (typeof obj[property] == 'object') {
      obj[property] = changeCamelCase(obj[property]);
    }

    // Change the case of the key of the current property.
    object[changeCase.camelCase(property)] = obj[property];
  }
  return object;
}


function buildJsonToValidate(localPart,value)
{
  var data = {
    local_part: localPart,
    value: value
  }

  var templateMaker = function (object) {
    return function (context) {
      var replacer = function (key, val) {
        if (typeof val === 'function') {
          return context[val()]
        }
        return val;
      }
      return JSON.parse(JSON.stringify(obj, replacer))
    }
  }

  var obj = {
    name: {
      "localPart" : function () { return 'local_part' }},
    "value": function () { return 'value'}
  }

  var template = templateMaker(obj);
  var rendered = template(data);
  return rendered;
}

function validateInput(clsName,localPart,inputValue) {
  logger.info(' Validating the input request ');
  var modelJsonSchema = JSON.parse(fs.readFileSync('./server/boot/lib/validation/jsonschemas/'+ changeCase.paramCase(clsName) +'.jsonschema').toString());
  logger.info(' Loading the Json schema for  %s', changeCase.paramCase(clsName));
  var validate = ajv.compile(modelJsonSchema);
  console.log("LOCAL PART: " + localPart);
  console.log("INPUT FROM REQUEST  " + JSON.stringify(inputValue));
  inputValue = changeCamelCase(inputValue);
  var inputJson = buildJsonToValidate(localPart,inputValue);
  logger.info(' The input JSON for Validation is %s ', JSON.stringify(inputJson))
   console.log("Input: " + JSON.stringify(inputJson));
  var valid = validate(inputJson);
  var errors = [];
  if (!valid) {
  

    for (var err in validate.errors) {
      var error = validate.errors[err];
      if (error["keyword"] == 'required' || error["keyword"] == 'type'
         || error["keyword"] == 'maxLength' || error["keyword"] == 'minLength'
        || error["keyword"] == 'minimum' || error["keyword"] == 'maximum'
         || error["keyword"] == 'enum' ) {
        errors.push(error);
      }
    }
    console.log(errors);
    logger.info('Invalid Input');
    logger.info(errors);
  } else {
    logger.info('Valid Input')
  }
  return valid;
}

exports.validateInput = validateInput;
