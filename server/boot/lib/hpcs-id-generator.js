var logger = require('./logger');
var shortid = require('shortid');

function generateHPCSId(request, operation){
  logger.info({req: request}, 'GENERATING HPCS ID FROM REQUEST');
  // If request is either ACHCancel or TCSSingleQuery then generate unique short id.
  var hpcsId = "";
  /* istanbul ignore if */
  if(operation == "ACHCancel" || operation == "TCSSingleQuery" ){
    hpcsId = shortid.generate();
  } else {
    var agencyName = fetchAgencyName(request);
    var agencyTrackingId = getAgencyTrackingId(request, operation);
    //hpcsId = agencyName + agencyTrackingId ;
     hpcsId = agencyTrackingId ;
  }
  logger.info('The HPCSID generated for AgencyName:%s , AgencyTrackingId:%s  is %s ', agencyName,agencyTrackingId,hpcsId);
  console.log('The HPCSID generated for AgencyName:%s , AgencyTrackingId:%s  is %s ', agencyName,agencyTrackingId,hpcsId);
  return hpcsId;
}

// Function to get the agency name = agency Id
function fetchAgencyName(request){
  var agencyName = request.tcs_app_id;
  logger.info('Agency Name from Request: %s', agencyName );
  // remove common string from TCP_App_ID eg. TCSHUDSFMPCR trimmed to SFMPCR
  /* istanbul ignore if */
  if(agencyName == null || agencyName == undefined ){
    logger.error('Unknown Edge Application Id');
    //throw Error("Unknown Edge Application Id.");
  }
  /* istanbul ignore if */
  if(agencyName.size  < 7 ){
    logger.error("Invalid TCP App ID provided.");
    //throw Error("Invalid TCP App ID provided.")
  }
  agencyName = agencyName.substr(6);
  return agencyName;
}

// Replace agency tracking id with hpcs tracking id.
function updateHPCSId(request, operation) {
  logger.info(' Updating HPCSId for request: %s ', JSON.stringify(request));
  request.agency_id = 950; // TODO replace actual HPCS ID provided by pay.gov for SOAP WS call.
  var hpcdId = generateHPCSId(request, operation);
  /* istanbul ignore if */
  if (request.agency_tracking_id != null) {
    request.agency_tracking_id = hpcdId;
  } else {
    request[operation].agency_tracking_id = hpcdId;
  }
  logger.info(' request after updating HPCSId: %s ', JSON.stringify(request));
  return hpcdId;
}

function getAgencyTrackingId(request, operation){
  var agencyTrackingId = request.agency_tracking_id;
  /* istanbul ignore if */
  if(agencyTrackingId == null) {
    agencyTrackingId = request[operation].agency_tracking_id;
  }
  logger.info('The Agency Tracking Id from request is %s ', agencyTrackingId)
  return agencyTrackingId;
}

exports.updateHPCSId = updateHPCSId;
exports.getAgencyTrackingId = getAgencyTrackingId;
exports.generateHPCSId = generateHPCSId;

