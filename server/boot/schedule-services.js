var appServer = require('../../server/server');
var schedule = require('node-schedule');
var paygovClient = require('./paygov-client');
var fs = require('fs');
var Client = require('ftp');
var SFTPClient = require('ssh2');
var servicesListContent = fs.readFileSync("./settings/services.json");
var utils = require('./lib/hpcs-utils');
var serviceInfo = JSON.parse(servicesListContent);
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = utils.extend({}, serviceInfo, authInfo);
// var batchSize = serviceInfo.batchSize;
var batchSize = 1;
var HPCSAgencyId = serviceInfo.schedulerHPCSAgencyId;
var js2xmlparser = require("js2xmlparser");

var AppConfig = appServer.models.ApplicationConfiguration;
var Request = appServer.models.Request;
var queueProcessor = require('./queue-processor');
var statusSchedulerRecurrence = serviceInfo.statusSchedulerRecurrence;
var callbackSchedulerRecurrence = serviceInfo.callbackSchedulerRecurrence;
var enqueueSchedulerRecurrence = serviceInfo.enqueueSchedulerRecurrence;
var statusSchedulerWindowTime = serviceInfo.statusSchedulerWindowTime;
//var everyDayAtMidnight = {hour: 00, minute: 00};
//var everydayMorning = {hour: 10,minute:30};
//var everydayMorningSeven = "0 */30 * * * *";
var everyDayAtMidnight = serviceInfo.everyDayAtMidnight;//{hour: 00, minute: 00};
var everydayMorning = serviceInfo.everydayMorning;//{hour: 10,minute:30};
var everydayMorningSeven = serviceInfo.everydayMorningSeven; //"0 */30 * * * *";
var logger = require('./lib/logger');
var SchedulerFlags = appServer.models.SchedulerFlags;
var cors = require('cors');
appServer.use(cors());
var proxyURL = "http://"+serviceInfo.proxy_url;
var xml2js = require('xml2js');
var parser = xml2js.Parser({ explicitArray: true });
var A67IsFTP = serviceInfo.A67IsFTP;
var A67FolderLoc = serviceInfo.A67FolderLoc;
var reportPurgeWindowTime = serviceInfo.reportPurgeWindowTime;
//var A67FolderLoc = "/PayDotGov/";
// Scheduler 1
/* istanbul ignore next */
schedule.scheduleJob(statusSchedulerRecurrence, function(){
  try {
    //check if status scheduler is not running already
    var rand = Math.round(Math.random() * (30000 - 500)) + 500;
    console.log("Interval "+rand);
    sleep(rand);
    checkSchedulerStatus("status","No",function(err, results){
      if(results.length > 0) {
        //Mark the scheduler status as running
        updateSchedulerStatus("status", "Yes");
        statusScheduler(); // Get latest status for requests.
      }else {
        //scheduler is running but if the timestamp is older than 3 hr. run the scheduler again
        checkSchedulerStatus("status","Yes",function(err, results) {
          if (results.length > 0) {
            var t1 = results[0]["schedulerTS"];
            var t2 = new Date();
            var difInhours = Math.abs(t1 - t2) / 36e5;
            console.log("hr.s passed for status scheduler "+difInhours);
            if(difInhours > 3.0)
            {
              statusScheduler();
            }
          }
        });
      }

    });
  }catch(err){
    logger.error("Error occurred on statusScheduler() : :" + err);
  }
});

function checkSchedulerStatus(value1,value2, cb){
  var query1 = {};
  query1["schedulerName"] = value1;
  var query2 = {};
  query2["status"] = value2;

  SchedulerFlags.find({where: {and: [query1,query2]}}, function(err, results){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error in checkSchedulerStatus %j', value1);
      console.log("Error");
      cb(err);
    } else {
      logger.info("No. of schedulerFlags found for status '%j' : %s",value1, results);

      cb(null, results);
    }
  });
}

function updateSchedulerStatus(schedulerName, value){
  var name = "status";
  var value = value;
  var name1 = "schedulerTS";
  var value1 = new Date();
  var query = {};
  query[name] = value;
  query[name1] = value1;
  var condiName = "schedulerName";
  var condiValue = schedulerName;
  var condition = {};
  condition[condiName] = condiValue;


  SchedulerFlags.update(condition,query, function (err, record) {
    /* istanbul ignore if */
    if (err) {
      logger.error({err: err}, 'Error On updateSchedulerStatus :'+status);
    } else {
      console.log("Updated scheduler " + schedulerName + ": with status : " + value);
      logger.info("Updated scheduler " + schedulerName + ": with status : " + value);

    }
  });

}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
// Scheduler 2
/* istanbul ignore next */
schedule.scheduleJob(callbackSchedulerRecurrence, function(){
  try {
    //check if status scheduler is not running already
    var rand = Math.round(Math.random() * (30000 - 5)) + 500;
    console.log("Interval for callback "+rand);
    logger.info("Interval for callback", rand);
    sleep(rand);

    checkSchedulerStatus("callback","No",function(err, results) {
      console.log("Result len ",results.length);
      if (results.length > 0) {
        //Mark the scheduler status as running
        updateSchedulerStatus("callback", "Yes");
        callbackScheduler(); // callback completed requests.
      }else {
        //Callback scheduler is running but if the timestamp is older than 3 hr. run the scheduler again
        checkSchedulerStatus("callback","Yes",function(err, results) {
          if (results.length > 0) {
            var t1 = results[0]["schedulerTS"];
            var t2 = new Date();

            var difInhours = Math.abs(t1 - t2) / 36e5;
            console.log("hr "+difInhours);
            if(difInhours > 1.0)
            {
              callbackScheduler();
            }
          }
        });
      }
    });

  }catch(err){
    logger.error("Error occurred on callbackScheduler() : :" + err);
  }
});

// Scheduler 3
/* istanbul ignore next */
schedule.scheduleJob(enqueueSchedulerRecurrence, function(){
  try {
    //check if status scheduler is not running already
    var rand = Math.round(Math.random() * (30000 - 500)) + 500;
    console.log("Interval in initiated scheduler"+rand);
    sleep(rand);
    checkSchedulerStatus("initiated","No",function(err, results) {

      if (results.length > 0) {
        //Mark the scheduler status as running
        updateSchedulerStatus("initiated", "Yes");
        enqueueInitiatedRequests();
      }else {
        //scheduler is running but if the timestamp is older than 3 hr. run the scheduler again
        checkSchedulerStatus("initiated","Yes",function(err, results) {
          if (results.length > 0) {
            var t1 = results[0]["schedulerTS"];
            var t2 = new Date();
            var difInhours = Math.abs(t1 - t2) / 36e5;
            console.log("hr.s passed for initiated scheduler "+difInhours);
            if(difInhours > 1.0)
            {
              enqueueInitiatedRequests();
            }
          }
        });
      }

    });
  }catch (err){
    logger.error("Error occurred on enqueueScheduler() :" + err);
  }
});

// Cleanup Scheduler
/* istanbul ignore next */
schedule.scheduleJob(everyDayAtMidnight, function() {
  try {
    purgeRequests();
    purgeSettlementRep();
  }catch(err){
    console.log("Error occurred on purgeRequests()");
    console.log(err);
    logger.error({err: err}, 'Error occurred on purgeRequests()');
  }
});

// pull settlement reports scheduler
var settlementReportStatus = appServer.models.SettlementReportStatus;
var settlementReport = appServer.models.SettlementReport;
var ApplicationConfig = appServer.models.ApplicationConfiguration;
var appConfigresp;
// pullSettlementReports function to save xml files in the db
function pullSettlementReports() {
  logger.info("            ((( Settlement Reports Pull Scheduler started ...  )))             ");
// fetching machineId and machine_password from db
  ApplicationConfig.find({where: {"agencyId": "1514", "tcsAppId": "TCSHUDFWEDH"}}, function (error, appConfigData) {
    if (error) {
      logger.error("Error occurred at ApplicationConfig.find() :" + error);
      //throw error;
    } else {
      appConfigresp = appConfigData[0];
      // fetching date Of Last Successful SettlementReport
      settlementReportStatus.find({
        where: {
          "agencyId": "1514",
          "tcsAppId": "TCSHUDFWEDH"
        }
      }, function (err, SettlementReportStatus) {
        if (err) {
          logger.error("Error occurred at settlementReportStatus.find() :" + err);
        } else {
          var settlemetD = SettlementReportStatus[0].dateOfLastSuccessfullSettlementReport;
          logger.info("Settlement report was pulled last on " + settlemetD);
          settlemetD = new Date(settlemetD);
          var loopDate = new Date(settlemetD);
          //calculating the difference between dateOfLastSuccessfullSettlementReport to current date
          var dd = new Date();
          /* var timeDiff = Math.abs(dd.getTime() - settlemetD.getTime());
           var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));*/
          var _MS_PER_DAY = 1000 * 60 * 60 * 24;
          var utc1 = Date.UTC(settlemetD.getFullYear(), settlemetD.getMonth(), settlemetD.getDate());
          var utc2 = Date.UTC(dd.getFullYear(), dd.getMonth(), dd.getDate());
          var diffDays = Math.floor((utc2 - utc1) / _MS_PER_DAY);
          var machineId = appConfigresp.machineId;
          var machine_password = appConfigresp.password;
          var report_name = "CollectionsActivityFile";// appConfigresp.report_name;
          var agency_id = appConfigresp.agencyId;
          var tcsApp_id = appConfigresp.tcsAppId;
          var SettlementReportModel = appServer.models.SettlementReport;
          var request = require('request');
          var requestForm = require('request');
          var formdata;
          var count = 1;
          var bodyData;
          var bodyFormData;
          var finalDate;

          //looping and hitting requests for in between dates
          var collectionRequests = function (i) {
            if (i < diffDays) {
              var d = new Date();
              loopDate = new Date(settlemetD);
              loopDate.setDate(loopDate.getDate() + i);
              loopDate = (loopDate.getMonth() + 1) + "/" + loopDate.getDate() + "/" + loopDate.getFullYear();
              finalDate = loopDate;
              formdata = {
                username: machineId,
                password: machine_password,
                agency_id: agency_id,
                report_name: "CollectionsActivityFile",
                date: loopDate
              };
              formActivedata = {
                username: machineId,
                password: machine_password,
                agency_id: agency_id,
                report_name: "FormActivityFile",
                date: loopDate
              };
              //hitting to paygov with CollectionsActivityFile
              logger.info("Getting collections file");
              request.post({
                url: serviceInfo.paygovReportUrl,
                form: formdata,
                followAllRedirects: true
                
              
              }, function (err, httpResponse, body) {
                var testVariable = -1;
                if(body != null && body != undefined) {
                   testVariable = body.indexOf("<?xml");
                }
                console.log("testVariable %%%%%%%%%%%%%%%%%%%%%%",testVariable);
                if (testVariable < 0) {
                  collectionRequests(i + 1);
                }
                else if (err) {
                  logger.info("CollectionsActivityFile Error: ", err);
                  collectionRequests(i + 1);
                  //throw err;
                }
                else {
                  var bodyVar = body.indexOf("<?xml");
                  console.log("BodyVar &&&&&&&&&&&&&&&&&&&&&&&",bodyVar);
                  var collectionXml = body;
                  var validCollectioAppName = false;
                  var validFormAppName =  false;
                  parser.parseString(collectionXml, function (err, result) {
                    if (err) {
                      logger.info("Error while getting collection activity file",err);
                    }
                    else if (result != null && result != undefined) {
                      var collectionAppName0 = "";
                      var collectionAppName1 = "";
                      var collectionAppName2 = "";
                      collectionAppName0 = result.Collections_Info.file_header[0].application_name[0];
                      collectionAppName1 = result.Collections_Info.file_header[1].application_name[0];
                      collectionAppName2 = result.Collections_Info.file_header[2].application_name[0];
                      if ((collectionAppName0.indexOf("Excess Income Report") > -1 ||
                          collectionAppName0.indexOf("HUD Fort Worth Section 201 Flexible Subsidy") > -1 ||
                          collectionAppName0.indexOf("HUD Fort Worth Section 202 Elderly and Disabled Housing") > -1)
                          &&
                          (collectionAppName1.indexOf("Excess Income Report") > -1 ||
                          collectionAppName1.indexOf("HUD Fort Worth Section 201 Flexible Subsidy") > -1 ||
                          collectionAppName1.indexOf("HUD Fort Worth Section 202 Elderly and Disabled Housing") > -1)
                          &&
                          (collectionAppName2.indexOf("Excess Income Report") > -1 ||
                          collectionAppName2.indexOf("HUD Fort Worth Section 201 Flexible Subsidy") > -1 ||
                          collectionAppName2.indexOf("HUD Fort Worth Section 202 Elderly and Disabled Housing") > -1)
                       )

                      {
                        validCollectioAppName = true;
                      } else {
                        validCollectioAppName = false;
                        logger.info("Invalid Collections file");
                      }

                      if (bodyVar == 0 && validCollectioAppName != false) {
                        bodyData = body;
                        //Getting FormActivityFile from paygov
                        console.log("Getting form file");
                        requestForm.post({
                          url: serviceInfo.paygovReportUrl,
                          form: formActivedata,
                          followAllRedirects: true
                         
                          
                        }, function (error, httpResp, bodyForm) {
                           var testVar = -1;
                          if(body != null && body != undefined) {
                            testVar = bodyForm.indexOf("<?xml");
                          }
                          if (testVar < 0) {
                            collectionRequests(i + 1);
                          }
                          else if (error) {
                            logger.info("FormActivityFile Error", error);
                            collectionRequests(i + 1);
                            //throw error;
                          } else {
                            var bodyFormVar = bodyForm.indexOf("<?xml");
                            var formXml = bodyForm;
                            parser.parseString(formXml, function (err, result) {
                              if (err) {
                                logger.info("Error while getting form activity file",err);
                              }
                              else if (result != null && result != undefined) {
                                var formAppName0 = "";
                                var formAppName1 = "";
                                var formAppName2 = "";

                                formAppName0 = result.form_info.file_header[0].agency_app_name[0];
                                formAppName1 = result.form_info.file_header[1].agency_app_name[0];
                                formAppName2 = result.form_info.file_header[2].agency_app_name[0];
                                if ((formAppName0.indexOf("Excess Income Report") > -1 ||
                                    formAppName0.indexOf("HUD Fort Worth Section 201 Flexible Subsidy") > -1 ||
                                    formAppName0.indexOf("HUD Fort Worth Section 202 Elderly and Disabled Housing") > -1)
                                    &&
                                    (formAppName1.indexOf("Excess Income Report") > -1 ||
                                    formAppName1.indexOf("HUD Fort Worth Section 201 Flexible Subsidy") > -1 ||
                                    formAppName1.indexOf("HUD Fort Worth Section 202 Elderly and Disabled Housing") > -1)
                                    &&
                                    (formAppName2.indexOf("Excess Income Report") > -1 ||
                                    formAppName2.indexOf("HUD Fort Worth Section 201 Flexible Subsidy") > -1 ||
                                    formAppName2.indexOf("HUD Fort Worth Section 202 Elderly and Disabled Housing") > -1)
                                )
                                {
                                  validFormAppName = true;
                                } else {
                                  validFormAppName = false;
                                  logger.info("Invalid Form Activity File");
                                }
                                if (bodyFormVar == 0 && validFormAppName != false) {
                                  bodyFormData = bodyForm;
                                  var collectionFile = {
                                    "CollectionsActivityFile": bodyData,
                                    "FormActivityFile": bodyFormData
                                  };
                                  d.setDate(d.getDate());
                                  d = (d.getMonth().toString().length == 1) ? ("0" + (d.getMonth() + 1)) + "/" + d.getDate() + "/" + d.getFullYear() : (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();

                                  var settlementReportData = {
                                    "agencyId": agency_id,
                                    "tcsAppId": tcsApp_id,
                                    "reportDate": loopDate,
                                    "reportPulledDate": d,
                                    "settlementReportXmlFile": collectionFile,
                                    "fileSentFlag": "false"
                                  };
                                  // saving agencyId, tcsAppId, reportDate,reportPulledDate and xml files
                                  // in the db and updating the dateOfLastSuccessfullSettlementReport
                                  logger.info("Saving report files in DB");

                                  var settlementReportQuery = {};
                                  settlementReportQuery["tcsAppId"] = "TCSHUDFWEDH";
                                  settlementReportQuery["reportDate"] = d;

                                  SettlementReportModel.find({where: {and: [{"tcsAppId": "TCSHUDFWEDH"}, {"reportDate": loopDate}]}}, function (err, SettlementReportRec) {
                                    if (SettlementReportRec.length > 0) {
                                      console.log("Updating record ");
                                      SettlementReportRec[0].updateAttributes({
                                        "settlementReportXmlFile": collectionFile, "fileSentFlag": "false",
                                        "reportPulledDate": d
                                      }, function (err, sucuss) {
                                        if (err) {
                                          logger.info("Error white updating settlement report ", err);
                                          collectionRequests(i + 1);
                                        } else {
                                          collectionRequests(i + 1);
                                        }
                                      })
                                    } else {
                                      logger.info("Inserting settlement report record");
                                      SettlementReportModel.create(settlementReportData);
                                    }

                                  })

                                  settlementReportStatus.find({where: {"tcsAppId": "TCSHUDFWEDH"}}, function (err, SettlementReportSt) {
                                    SettlementReportSt[0].updateAttributes({dateOfLastSuccessfullSettlementReport: loopDate}, function (err, sucuss) {

                                      if (err) {
                                        logger.info("error white updating date of last successful settlement report ", err);
                                        collectionRequests(i + 1);
                                      } else {
                                        requestForm = require('request');
                                        collectionRequests(i + 1);
                                      }
                                    })
                                  })
                                  delete bodyFormVar;
                                } else {
                                  logger.info("Form Activity Settlement report is not correct:");
                                  collectionRequests(i + 1);
                                }

                              }
                            });

                          }

                        })
                      }
                      else {
                        logger.info("Collection Activity Settlement report is not valid:");
                        collectionRequests(i + 1);
                      }
                    }
                  });
                }
              });
            }
            if (i == diffDays) {
              updateSchedulerStatus("settlementPull", "No");
            }
          }
          collectionRequests(1);
        }
      });
    }
  });
}
//---------------------------------Start Settlement Report Pull Scheduler------------------------------------------------
/* istanbul ignore next */
schedule.scheduleJob(everydayMorning, function () {
//schedule.scheduleJob("0 */3 * * * *", function () {
  try {
    //check if status scheduler is not running already
    var rand = Math.round(Math.random() * (30000 - 500)) + 500;
    sleep(rand);
    checkSchedulerStatus("settlementPull", "No", function (err, results) {
      if (results.length > 0) {
        //Mark the scheduler status as running
        updateSchedulerStatus("settlementPull", "Yes");
        pullSettlementReports();
      } else {
        //scheduler is running but if the timestamp is older than 3 hr. run the scheduler again
        checkSchedulerStatus("settlementPull", "Yes", function (err, results) {
          if (results.length > 0) {
            var t1 = results[0]["schedulerTS"];
            var t2 = new Date();
            var difInhours = Math.abs(t1 - t2) / 36e5;
            console.log("hr.s passed for initiated scheduler " + difInhours);
            if (difInhours > 1.0) {
              pullSettlementReports();
            }
          }
        });
      }

    });
  } catch (err) {
    logger.error("Error occurred on enqueueScheduler() :" + err);
  }
});
//---------------------------------End Settlement Report Pull Scheduler------------------------------------------------


var xmlReports = appServer.models.SettlementReport;
var flagStatus = appServer.models.SettlementReport;
// pullSettlementFlags method for posting CollectionsActivityFile and FormActivityFile to ftp location.
function pullSettlementFlags() {
  logger.info("            ((( Settlement Report Post Scheduler started ...  )))             ");
  var completeCollectionsActivityFile;
  var completeFormActivityFile;
  var reportId;
  //var Client = require('ftp');
  // var fs = require('fs');
  var collectionsFile;
  var formFile;
  var destFTP;
  var filetrue = false;
  var fileresponse;
  var ApplicationConfig = appServer.models.ApplicationConfiguration;
  var ftpDetails;
  // Find settlement reports not yet sent
  xmlReports.find({where: {"fileSentFlag": "false"}}, function (error, response) {
    if (error) {
      logger.info("Error ",error);
    } else {
      fileresponse = response;
      //ftp information pulling from AppConfig model
      ApplicationConfig.find({where:{ "agencyId": "1514","tcsAppId": "TCSHUDFWEDH"}},function (error,appConfigData) {
        if(error){
          logger.info("Error ",error);
        }else{
          ftpDetails = appConfigData[0].settlementURL;
          var ftpDetailz = ftpDetails.split("/");
          destFTP = {
            host     : ftpDetailz[2].split(":")[0],
            port     : ftpDetailz[2].split(":")[1],
            user     : ftpDetailz[3],
            password : ftpDetailz[4]
          }

          /*destFTP = {
           host     : "HWVANDD4183.hud.gov",
           port     : 21,
           user     : "X01637",
           password : ""
           }*/

          console.log("Connection String------> ",destFTP);
          //logger.info("Connection String------> ",destFTP);

          //looping each file to sent to ftp location
          function ftpfiles(i) {
            if (i < response.length) {

              completeCollectionsActivityFile = response[i].settlementReportXmlFile.CollectionsActivityFile;
              completeFormActivityFile = response[i].settlementReportXmlFile.FormActivityFile;
              reportId = response[i].id;
              // parsing collectionsFile
              parser.parseString(completeCollectionsActivityFile, function (err, result) {
                if (err) {
                  ftpfiles(i + 1);
                  //throw err;
                }
                else {
                  // parsing form activity file
                  parser.parseString(completeFormActivityFile, function (error, results) {
                    if (error) {
                      ftpfiles(i + 1);
                      //throw error;
                    } else {
                      collectionsFile = result.Collections_Info.file_footer[0].file_name[0];
                      formFile = results.form_info.file_footer[0].file_name[0];
                      var c;
                       logger.info("A67IsFTP --------->",A67IsFTP);
                       logger.info("Folder location ------>",A67FolderLoc);
                      if(A67IsFTP.toString().toUpperCase().trim() === "SFTP")
                        c = new SFTPClient();
                      else
                        c = new Client();
                      var path = require('path');
                      var fullPathCollection = path.join(__dirname, 'collectionFile.xml');
                      var pathForm = require('path');
                      var fullPathForm = pathForm.join(__dirname, 'form.xml');
                      //writing into a collectionsFile
                      fs.writeFile(fullPathCollection, completeCollectionsActivityFile, function (err) {
                        if (err) {
                          ftpfiles(i + 1);
                          return console.error(err);
                        } else {

                          //writing into FormActivityFile
                          fs.writeFile(fullPathForm, completeFormActivityFile, function (errr) {
                            if (errr) {
                              ftpfiles(i + 1);
                              //throw errr;
                            } else {
                              filetrue = true;
                            }
                          })
                        }
                        //writing into the file
                        if (collectionsFile && (A67FolderLoc != null && A67FolderLoc != undefined)
                                            &&(A67IsFTP != null && A67IsFTP != undefined)) {
                          c.on('ready', function () {
                            if(A67IsFTP.toString().toUpperCase().trim() === "FTP"){
                              console.log("Connecting to FTP server");
                              c.put(fullPathCollection, A67FolderLoc+collectionsFile, function (err) {
                                if (err) {
                                  logger.info("collection activity file error:", err);
                                  ftpfiles(i + 1);
                                  //throw err;
                                } else {
                                  console.log("FTP Collection Activity file sent successfully",A67FolderLoc+collectionsFile);
                                  logger.info("FTP collection activity file sent successfully ",A67FolderLoc+collectionsFile);
                                  c.put(fullPathForm,A67FolderLoc+formFile, function (err) {
                                    if (err) {
                                      ftpfiles(i + 1);
                                      logger.info("Form activity file error:", err)
                                     // throw err;
                                    }
                                    else {
                                      console.log("FTP Form activity file sent ",A67FolderLoc+formFile);
                                      logger.info("FTP Form activity file sent successfully ", A67FolderLoc+formFile);
                                      flagStatus.find({where: {"id": reportId}}, function (error, FlagStatus) {
                                        if (error) {
                                          ftpfiles(i + 1);
                                          console.log("error in sending form activity file ", error);
                                          //throw error;
                                        } else {
                                          FlagStatus[0].updateAttributes({"fileSentFlag": "true"}, function (err, sucuss) {
                                            if (err) {
                                              ftpfiles(i + 1);
                                              //throw err;
                                            }
                                            ftpfiles(i + 1);
                                          })
                                        }
                                      })
                                    }
                                  })

                                }
                                c.end();
                              });
                            }else {
                              console.log("SFTP to ------> ",A67FolderLoc);
                              c.sftp(function (err, sftp) {
                                if (err) {
                                  logger.info("collection activity file error:", err);
                                  ftpfiles(i + 1);
                                  //throw err;
                                }
                                else {
                                  var fs = require("fs");
                                  var readStream = fs.createReadStream( fullPathCollection);
                                  var writeStream = sftp.createWriteStream(A67FolderLoc+collectionsFile);

                                  var readStream1 = fs.createReadStream( fullPathForm);
                                  var writeStream1 = sftp.createWriteStream(A67FolderLoc+formFile);

                                  writeStream.on('close', function () {
                                    console.log("- file transferred succesfully");
                                    sftp.end();
                                    flagStatus.find({where: {"id": reportId}}, function (error, FlagStatus) {
                                      if (error) {
                                        ftpfiles(i + 1);
                                        console.log("SFTP error in sending settlement report files ", error);
                                        //throw error;
                                      } else {
                                        FlagStatus[0].updateAttributes({"fileSentFlag": "true"}, function (err, sucuss) {
                                          if (err) {
                                            ftpfiles(i + 1);
                                            //throw err;
                                          }
                                          ftpfiles(i + 1);
                                        })
                                      }
                                    })
                                  });
                                  writeStream.on('end', function () {
                                    console.log("sftp connection closed");
                                    c.close();
                                  });
                                }
                                // initiate transfer of file
                                readStream.pipe( writeStream );
                                readStream1.pipe( writeStream1 );

                              }); //End of sftp
                            }
                          });
                          c.connect(destFTP);
                        }
                      })
                    }
                  })
                }
              })
            }
            if (i == fileresponse.length) {
              //update the scheduler with status as no.
              logger.info("End Settlement Report Post scheduler Marking the status scheduler status with No");
              updateSchedulerStatus("settlementPost", "No");
            }
          }

          ftpfiles(0);
        }

      })
    }
  })
}
//------------------------------Start settlement report post scheduler------------------------------------------------
/* istanbul ignore next */
schedule.scheduleJob(everydayMorningSeven, function () {
//schedule.scheduleJob("0 */1 * * * *", function () {
  try {
    //check if status scheduler is not running already
    var rand = Math.round(Math.random() * (30000 - 500)) + 500;
    console.log("Interval in settlement scheduler" + rand);
    sleep(rand);
    checkSchedulerStatus("settlementPost", "No", function (err, results) {
      console.log("results value in settlement scheduler:: ");
      if (results.length > 0) {
        //Mark the scheduler status as running
        updateSchedulerStatus("settlementPost", "Yes");
        pullSettlementFlags();
      } else {
        //scheduler is running but if the timestamp is older than 3 hr. run the scheduler again
        checkSchedulerStatus("settlementPost", "Yes", function (err, results) {
          if (results.length > 0) {
            var t1 = results[0]["schedulerTS"];
            var t2 = new Date();
            var difInhours = Math.abs(t1 - t2) / 36e5;
            console.log("hr.s passed for settlement Post scheduler " + difInhours);
            if (difInhours > 1.0) {
              pullSettlementFlags();
            }
          }
        });
      }

    });
  } catch (err) {
    logger.error("Error occurred on enqueueScheduler() :" + err);
  }
});
//---------------------------------End settlement report post  scheduler------------------------------------------------

function fetchTransactionsByStatus(statuses, cb){
  Request.find({where: {and : [{sent : false}, statuses, {or : [{operationName : "ACHDebit"}, {operationName: 'ACHPrenotification'}]}]}}, function(err, results){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error fetching request of status %j', statuses);
      cb(err);
    } else {
      logger.info("No. of requests found for status '%j' : %s",statuses, results.length);
      cb(null, results);

    }
  });
}


function fetchTransactionsForStatusScheduler(cb){
  var servicesJSON = fs.readFileSync("./settings/services.json");
  var servicesSettings = JSON.parse(servicesJSON);
  var days = servicesSettings.statusSchedulerWindowTime;
  var today = new Date()
  var priorDate = new Date().setDate(today.getDate() - days);

  Request.find({where: {and : [{"created_date": { gte : priorDate }}, {or : [{operationName : "ACHDebit"}, {operationName: 'ACHPrenotification'}]}]}}, function(err, results){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error fetching requests past '+days+' days');
      cb(err);
    } else {
      logger.info("No. of requests found for past "+days+" days : "+results.length);
      cb(null, results);

    }
  });
}


function sendTransactionStatus(url, transactions, cb ){
  var request = require('request');
  request({
    url: url,
    method: "POST",
    body: transactions,
    headers:{
      'Content-Type': 'application/xml'
    }
  }, function(err, response, body) {
    /* istanbul ignore if */
    if (err) {
      cb(err);
    } else {
      cb(null, body);
    }
  });
}

function updateTransactionStatus(hpcs_id, transaction_status, return_detail,paygov_tracking_id){
  logger.info('Inside function updateTransactionStatus ' + hpcs_id);
  if(hpcs_id != undefined && hpcs_id != null && hpcs_id.trim().length > 0) {
    hpcs_id = hpcs_id.trim();
    Request.update({hpcs_id: hpcs_id}, {
      status: transaction_status,
      return_detail: return_detail,
      paygov_tracking_id: paygov_tracking_id
    }, function (err, record) {
      /* istanbul ignore if */
      if (err) {
        logger.error({err: err}, 'Error On STATUS Update HPCS ID:'+hpcs_id);
      } else {
        logger.error("Updated HPCS Request ID: " + hpcs_id + " with status : " + transaction_status+
          " paygov tracking id : "+paygov_tracking_id + " reutrn detail : "+return_detail);
      }
    });
  }
}

function updateTransactionResponse(hpcs_id, response, transaction_status, return_detail,paygov_tracking_id){
  logger.info('Inside function updateTransactionResponse ' + hpcs_id);
  if(hpcs_id != undefined && hpcs_id != null && hpcs_id.trim().length > 0) {
    hpcs_id = hpcs_id.trim();
    Request.update({hpcs_id: hpcs_id}, {
      response: JSON.stringify(response),
      status: transaction_status,
      return_detail: return_detail,
      paygov_tracking_id: paygov_tracking_id
    }, function (err, record) {
      /* istanbul ignore if */
      if (err) {
        logger.error({err: err}, 'Error On STATUS Update HPCS ID:'+hpcs_id);
      } else {
        logger.error("Updated HPCS Request ID: " + hpcs_id + " with status : " + transaction_status +"paygov ID "+paygov_tracking_id);
      }
    });
  }
}


function updateTransactionSent(hpcs_id, transaction_sent){
  Request.update( {hpcs_id : hpcs_id}, {sent : transaction_sent}, function(err, record){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error On SENT Update HPCS ID:'+hpcs_id);
    } else {
      logger.info("Updated!!!");
    }
  });
}

var statusSchedulerRS = [];
var statusIndex = 0;
function statusScheduler(){
  logger.info("            ((( Status Scheduler started ...  )))             ");
  // fetch Submitted transactions
  fetchTransactionsForStatusScheduler(function(err, results){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error while fetch request which in Received & Failed.');
    } else{
      // fetch latest transaction status
      statusSchedulerRS = results;
      statusIndex = 0;
      if(statusSchedulerRS.length > 0){
        syncStatusLoop();
      }
      else
      {
        console.log("End status scheduler Marking the status scheduler status with No");
        logger.info("End status scheduler Marking the status scheduler status with No");
        updateSchedulerStatus("status", "No");
      }
    }
  });
}

function syncStatusLoop(){
  if(statusIndex < statusSchedulerRS.length) {
    var hpcs_id = statusSchedulerRS[statusIndex].hpcs_id;
    var request = {
      "agency_id": HPCSAgencyId,
      "tcs_app_id": statusSchedulerRS[statusIndex].tcs_app_id,
      "agency_tracking_id": hpcs_id,
      "paygov_tracking_id": statusSchedulerRS[statusIndex].paygov_tracking_id
    };
    queryLatestStatus(request, function(err, response){
      statusIndex++;
      /* istanbul ignore if */
      if(err){
        syncStatusLoop();
      } else {
        var result = utils.extractSingleQueryResponse(response);
        // update latest transaction to DB
        updateTransactionStatus(result.hpcs_id, result.status, result.return_detail,result.paygov_tracking_id);
        syncStatusLoop();
      }
    });
  }else {
    console.log("End status scheduler Marking the status scheduler status with No");
    logger.info("End status scheduler Marking the status scheduler status with No");
    updateSchedulerStatus("status", "No");
  }
}


var enqueueRequests = [];
var enqueueIndex = 0;
function enqueueInitiatedRequests(){
  enqueueRequests = [];
  logger.info("            ((( Enqueue Scheduler started ...  )))             ");
  fetchTransactionsByStatus({"status" : "Initiated"}, function(err, results){
    /* istanbul ignore if */
    if(err){
      logger.info('Error in fetchTransactionsByStatus',err);
    } else {
      // If there are 'Initiated' request and add it to enqueueRequest & proper enqueue based on condition.
      if(results.length > 0 ) {
        enqueueRequests = results;
        enqueueIndex = 0;
        enqueueSyncLoop();
      }else{
        console.log("End of initiated scheduler Marking the initiated scheduler status with No");
        logger.info("End of initiated scheduler Marking the initiated scheduler status with No");
        updateSchedulerStatus("initiated", "No");
      }
    }
  });
}




// This function process the 'Initiated' requests to enqueue.
function enqueueSyncLoop() {
  if (enqueueIndex < enqueueRequests.length) {
    // Build SingleQuery request to check whether Request is available with PayGov.
    var enqueueRequest = enqueueRequests[enqueueIndex];
    if (enqueueRequest != null && enqueueRequest != undefined) {
      var hpcs_id = enqueueRequest.hpcs_id;
      var request = {
        "agency_id": HPCSAgencyId,
        "tcs_app_id": enqueueRequest.tcs_app_id,
        "agency_tracking_id": hpcs_id,
        "paygov_tracking_id": enqueueRequest.paygov_tracking_id
      };
      // Fetch latest status for Request.
      queryLatestStatus(request, function (err, response) {
        if (err) {
          logger.error("SingleQuery Error occurred while calling PayGov for HPCS ID.", hpcs_id);
          enqueueIndex++;
          enqueueSyncLoop();
        } else {
          if (response != null) {
            // If Request is not found with PayGov then enqueue the Request.
            if (response.return_code == '4061'
              && response.return_detail == 'No Data Found for Search Criteria Entered') {
              logger.info("No data found for hpcs id:", hpcs_id);
              enqueueRequestIntoQueue(enqueueRequest.operationName, enqueueRequest.request);
              logger.info("Enqueued the Request hpcs_id:", hpcs_id);
              enqueueIndex++;
              enqueueSyncLoop();
            } else {
              // If Request is found with PayGov.
              var req_transaction_amount = '';
              var req_account_number = '';
              var req_routing_transit_number = '';

              var res_transaction_amount = '';
              var res_account_number = '';
              var res_routing_transit_number = '';
              // requestObj is actual ACH or Prenotification Request.
              var requestObj = JSON.parse(enqueueRequest.request);

              if (enqueueRequest.operationName == 'ACHDebit') {
                req_transaction_amount = requestObj.ACHDebit.transaction_amount;
                req_account_number = requestObj.ACHDebit.account_number;
                req_routing_transit_number = requestObj.ACHDebit.routing_transit_number;
                if(response.tcs_ach_results != undefined){
                  var ach_debit_result =  response.tcs_ach_results.ach_debit_query_response[0];
                  res_transaction_amount = ach_debit_result.transaction_amount;
                  res_account_number = ach_debit_result.masked_account_number;
                  res_routing_transit_number = ach_debit_result.routing_transit_number;
                }
              } else{
                req_transaction_amount = requestObj.ACHPrenotification.transaction_amount;
                req_account_number = requestObj.ACHPrenotification.account_number;
                req_routing_transit_number = requestObj.ACHPrenotification.routing_transit_number;
                if(reponse.tcs_ach_results != undefined){
                  var ach_prenotification_result = response.tcs_ach_results.ach_prenotification_query_response[0];
                  res_transaction_amount = ach_prenotification_result.transaction_amount;
                  res_account_number = ach_prenotification_result.masked_account_number;
                  res_routing_transit_number = ach_prenotification_result.routing_transit_number;
                }
              }
              if(req_account_number.length > 4) {
                req_account_number = req_account_number.substr(req_account_number.length -4);
              }
              if(res_account_number.length > 4){
                res_account_number = res_account_number.substr(res_account_number.length - 4);
              }

              // If transaction amount & routing transit number are not same
              // then enqueue message to Queue.
              if(!(req_routing_transit_number == res_routing_transit_number
                && req_transaction_amount == res_transaction_amount
                && req_account_number == res_account_number)){
                logger.info("Mismatch in Request data for hpcs_id:", hpcs_id);
                enqueueRequestIntoQueue(enqueueRequest.operationName, enqueueRequest.request);
                logger.info("Enqueued the Request hpcs_id:", hpcs_id);
                enqueueIndex++;
                enqueueSyncLoop();
              } else {
                // else Update response & status to DB.
                var result = utils.extractSingleQueryResponse(response);
                updateTransactionResponse(result.hpcs_id, response, result.status, result.return_detail,result.paygov_tracking_id);
                enqueueIndex++;
                enqueueSyncLoop();
              }
            }
          } else {
            enqueueIndex++;
            enqueueSyncLoop();
          }
        }
      });
    } else {
      enqueueIndex++;
      enqueueSyncLoop();
    }
  }
  else {
    console.log("End of initiated scheduler Marking the initiated scheduler status with No");
    logger.info("End of initiated scheduler Marking the initiated scheduler status with No");
    updateSchedulerStatus("initiated", "No");
  }
}

function enqueueRequestIntoQueue(operationName, request){
  try {
    var serviceAction = "TCSSingleService/process" + operationName;
    var msg = {'service': serviceAction, 'request': JSON.parse(request)};
    queueProcessor.pushMessageToQueue(msg);
  }catch(err){
    logger.info("Error while processing enqueue..",err);
  }
}



function queryLatestStatus(request, cb){
  logger.info("Calling PayGov with input %j:", request);
  paygovClient.callPayGov("TCSSingleQueryService/processTCSSingleQuery", request, function(err, response){
    /* istanbul ignore if */
    if(err){
      logger.error("Error occurred while calling PayGov.");
      cb(err);
    } else {
      cb(null, response);
    }
  })

}

function transformJSONtoXML(requestSet){
  var total_amount = 0;
  var data = {
    control_total: {
      "transaction_count" : {
        "@" : {
          "value": requestSet.length
        }
      }
    },
    "collection_auth_result": []
  };
  for(i = 0 ; i < requestSet.length ; i++){
    data.collection_auth_result[i] = {
      agency_tracking_id : {
        "@" : {
          "value": requestSet[i].agency_tracking_id
        }
      },
      paygov_tracking_id : {
        "@" : {
          "value": requestSet[i].paygov_tracking_id
        }
      },
      payment_status : {
        "@" : {
          "value": getPayGovStatus(requestSet[i].status)
        }
      }
    };

    if(requestSet[i].request != undefined){
      var requestjs = JSON.parse(requestSet[i].request);
      if(requestjs != undefined){
        total_amount +=getTransactionAmount(requestjs);
      }
    }
    data.control_total.total_amount = {
      "@" : {
        "value": total_amount
      }
    }
    data.control_total.net_amount = {
      "@" : {
        "value": total_amount
      }
    }

    if(requestSet[i].status == 'Failed'){
      data.collection_auth_result[i].error_detail = {
        "@" : {
          "value": requestSet[i].return_detail
        }
      }
      data.collection_auth_result.error_message = {
        "@" : {
          "value": requestSet[i].return_detail
        }
      }
    }
  }
  var outputXML = js2xmlparser("collection_result", data);
  logger.info("Callback Scheduler Output XML ",outputXML);
  return outputXML;
}
/* istanbul ignore next*/
function getPayGovStatus(status)
{
  if(status != 'Failed')
  {
    return 'Completed';
  }else{
    return 'Failed';
  }
}
function getTransactionAmount(requestjs){
  if(requestjs.ACHDebit != undefined) {
    return requestjs.ACHDebit.transaction_amount;
  } else if(requestjs.ACHPrenotification != undefined){
    return requestjs.ACHPrenotification.transaction_amount;
  }
}

var s2ResultSet = [];
var index = 0;
var AppConfigMap = {};
var ListOfURLs = [];
var urlToRequestGroups = [];
var batchRequests = [];
var batchURLs = [];

function callbackScheduler(){
  // fetch Completed transactions not published.
  logger.info("            ((( Callback Scheduler started ...  )))             ");

  fetchTransactionsByStatus({or: [{status : "Received"},{status: "Failed"}]}, function(err, results){
    /* istanbul ignore if */
    if(err){
      logger.info('Error in callback scheduler fetchTransactionsByStatus ',err);
    } else {
      if(results != undefined && results != null && results.length > 0) {
        index = 0;
        s2ResultSet = results;
        AppConfigMap = {};
        urlToRequestGroups = [];
        batchRequests = [];
        batchURLs = [];
        syncFetchUrlLoop();
      }
      else
      {
        updateSchedulerStatus("callback", "No");
      }
    }
  });
}

function fetchResultCfmUrlForTCSAppId(tcs_app_id, cb){
  // Fetch ResultCfmUrl for TCSAppId from Map.
  var url = AppConfigMap[tcs_app_id];
  // If not ResultCfmUrl found, then Search in database.
  if(url == null){
    AppConfig.find({where: {tcsAppId : tcs_app_id}}, function(err, response){
      /* istanbul ignore if */
      if(err){
        logger.info('Error in AppConfig.find',err);
        cb(err);
      } else {
        if(response != null && response.length == 1 &&
          response[0].resultCfmURLs != undefined && response[0].resultCfmURLs != null){
          AppConfigMap[tcs_app_id] = response[0].resultCfmURLs;
          ListOfURLs[ListOfURLs.length] = response[0].resultCfmURLs;
          cb(null, response[0].resultCfmURLs);
        } else {
          // if No ResultCfmUrl found for TCSAppId then return null.
          cb(null, null);
        }
      }
    });
  } else { // ResultCfmUrl Found, return the same.
    cb(null, url);
  }
}

function syncFetchUrlLoop(){

  if(index < s2ResultSet.length){
    fetchResultCfmUrlForTCSAppId(s2ResultSet[index].tcs_app_id, function(err, cbUrl){
      /* istanbul ignore if */
      if(err){
        logger.info('Error in fetchResultCfmUrlForTCSAppId',err);
      }
      else {
        // Mark Non-Mainframe Request's sent as true.
        if(cbUrl == null){
          updateTransactionSent(s2ResultSet[index].hpcs_id, true);
        } else {
          if(urlToRequestGroups[cbUrl] == undefined){
            // Create empty Array for new callbackURL.
            urlToRequestGroups[cbUrl] = [];
          }
          // Group all requests for each callbackURL.
          urlToRequestGroups[cbUrl][urlToRequestGroups[cbUrl].length] = s2ResultSet[index];
        }
        index++;
        syncFetchUrlLoop();
      }
    });
  } else {
    // Loop over all URLs
    var k = 0;
    for(var i = 0 ; i < ListOfURLs.length ; i++){
      // Fetch all request of each URL
      var cbUrl = ListOfURLs[i];
      var urlRequests = urlToRequestGroups[cbUrl];
      // Break requests into batch as configured in services.json.
      if(urlRequests != undefined || urlRequests != null) {
        for(var j = 0 ; j < urlRequests.length ; j++) {
          if(batchRequests[k] == undefined){
            batchRequests[k] = [];
            batchURLs[k] = cbUrl
          }
          batchRequests[k][batchRequests[k].length] = urlRequests[j];
          if(batchRequests[k].length == batchSize && j < urlRequests.length - 1){
            k++;
          }
        }
        k++;
      }
      else{
        logger.info("Callback Scheduler Error : No request found for url ",cbUrl);
      }
    }
    index = 0;
    syncCallBackLoop();
  }
}


function syncCallBackLoop(){
  if(index < batchRequests.length){
    var requestList = batchRequests[index];
    var xmlResult = transformJSONtoXML(requestList);
    sendTransactionStatus(batchURLs[index], xmlResult, function (err, response) {
      /* istanbul ignore if */
      if (err) {
        index++;
        syncCallBackLoop();
      } else {
        // update transactions with sent as true.
        if(response != undefined && response.endsWith('OK')) {
          for (var j = 0; j < requestList.length; j++) {
            updateTransactionSent(requestList[j].hpcs_id, true);
          }
        }
        index++;
        syncCallBackLoop();
      }
    });
  } else {
    logger.info("***************************(End of Callback Scheduler)*******************************)");
    updateSchedulerStatus("callback", "No");
  }
}

function purgeRequests(){
  var days = serviceInfo.purgeWindowTime;
  var today = new Date()
  var priorDate = new Date().setDate(today.getDate() - days);
  Request.deleteAll({
    "created_date": { lte : priorDate }
  }, function(err, response){
    /* istanbul ignore if */
    if(err){
      logger.info('Purge completed with error(s). Please check log file for error(s).',err);
    } else {
      logger.info('Purge completed successfully.',response);
    }
  });
}

function purgeSettlementRep(){
  var settlementReport = appServer.models.SettlementReport;
  var days = serviceInfo.reportPurgeWindowTime;
  var today = new Date()
  var priorDate = new Date().setDate(today.getDate() - days);
  settlementReport.deleteAll({
    "reportDate": { lte : priorDate }
  }, function(err, response){
    /* istanbul ignore if */
    if(err){
      logger.info('Purge completed with error(s). Please check log file for error(s).',err);
    } else {
      logger.info('Purge completed successfully.',response);
    }
  });
}


exports.statusScheduler = statusScheduler;
exports.callbackScheduler = callbackScheduler;
exports.purgeRequests = purgeRequests;
exports.fetchResultCfmUrlForTCSAppId = fetchResultCfmUrlForTCSAppId;
exports.getTransactionAmount = getTransactionAmount;
exports.fetchTransactionsByStatus = fetchTransactionsByStatus;
exports.sendTransactionStatus = sendTransactionStatus;
exports.queryLatestStatus = queryLatestStatus;
exports.updateTransactionSent = updateTransactionSent;
exports.updateTransactionStatus = updateTransactionStatus;
exports.updateTransactionResponse = updateTransactionResponse;
exports.enqueueInitiatedRequests =  enqueueInitiatedRequests;
exports.fetchTransactionsForStatusScheduler = fetchTransactionsForStatusScheduler;
exports.pullSettlementReports = pullSettlementReports;
exports.pullSettlementFlags = pullSettlementFlags;
exports.purgeSettlementRep = purgeSettlementRep;

