var fs = require('fs');
var soap = require('soap');
var logger = require('./lib/logger');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./lib/hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

function callPayGov(service, input, cb){
  var serviceList = serviceInfo.services;
  var action = service.substring(service.indexOf("/")+1);
  service = service.substring(0, service.indexOf('/'));
  var url = serviceList[service];

  var pfx = fs.readFileSync(serviceInfo.pfx);
  var passphrase = serviceInfo.passphrase;
  var proxyurl = serviceInfo.proxy_url;
  var wsSecurity = new soap.ClientSSLSecurityPFX(pfx, passphrase);
  logger.info('Connecting to pay.gov...' + proxyurl);
  var request = require('request');
  if(!(proxyurl == "" || proxyurl == null)){
    proxyurl = "http://"+proxyurl;
    var request_with_defaults = request.defaults({'proxy': proxyurl, 'timeout': 120000, 'connection': 'keep-alive'});
    logger.info("proxyurl from paygov ",proxyurl);

    var soap_client_options = {'request': request_with_defaults,
      wsdl_options: {
        pfx: fs.readFileSync(serviceInfo.pfx),
        passphrase: serviceInfo.passphrase
      }};
    soap.createClient(url, soap_client_options, function(err, client) {
      /* istanbul ignore if */
      if(err)
      {
        logger.error({err: err}, 'Error creating soap client to connect to pay gov');
        cb(err);
      }
      client.setSecurity(new soap.ClientSSLSecurityPFX(pfx, passphrase));
      var args = input;
      logger.info('Consuming the service from : ' + url);
      client[action](args, function(err2, result){
        /* istanbul ignore if */
        if(err2){
          logger.error({err2: err2}, 'Unable to perform operation');
          cb(null, null, err2);
        } else {
          logger.info('Soap Action : ' + service+'/'+action +' Response : ');
          logger.info('Response from Pay Gov: ' + JSON.stringify(result));
          logger.info("Result=========>"+ JSON.stringify(result));
          cb(null,result);
        }
      })
    });
  }
  else
  {
    logger.info("No Proxy set");
    var wsdl_options = {
      pfx:fs.readFileSync(serviceInfo.pfx),
      passphrase: serviceInfo.passphrase
    }
    soap.createClient(url,wsdl_options,function(err, client) {
        /* istanbul ignore if */
        if(err)
        {
          logger.error({err: err}, 'Error creating soap client to connect to pay gov');
          cb(err);
        }
        client.setSecurity(new soap.ClientSSLSecurityPFX(pfx, passphrase));
        var args = input;
        logger.info('Consuming the service from : ' + url);
        client[action](args, function(err2, result){
          /* istanbul ignore if */
          if(err2){
            logger.error({err2: err2}, 'Unable to perform operation');
            cb(null, null, err2);
          } else {
            logger.info('Soap Action : ' + service+'/'+action +' Response : ');
            logger.info('Response from Pay Gov: ' + JSON.stringify(result));
            logger.info("Result=========>"+ JSON.stringify(result));
            cb(null,result);
          }
        }, {timeout: 120000})
      }
    );
  }

}
exports.callPayGov = callPayGov;
