var appServer = require('../../server/server');
var logger = require('./lib/logger');
function persistMessageToDB(hpcsId, operationName, input, cb){
  logger.info('Inside persistMessageToDB.... ',hpcsId);
  var Request = appServer.models.Request;
  var record = {
    "hpcs_id": hpcsId,
    "status": "Initiated",
    "operationName": operationName,
    "created_date": new Date(),
    "tcs_app_id": input.tcs_app_id,
    "request" : JSON.stringify(input)
  };
  Request.create(record, function(err, output){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error creating request record to DB. Request Not Persisted.');
      cb(err);
    } else {
      logger.info(' Request persisted to DB ');
      cb(null, output);
    }
  });
}

function updateStatueForRequest(hpcsId, set_object){
  logger.info('Inside updateStatueForRequest HPCS_ID = %s  ', hpcsId);
  console.log('Inside updateStatueForRequest HPCS_ID = %s  ', hpcsId);
 console.log("Inside if",set_object.status,set_object.return_detail);

  //Do not update if the status is Failed and return detail is duplicate agency id
  if(set_object.status != null && set_object.status == "Failed" &&
    set_object.return_detail != null && set_object.return_detail == "The value supplied for the agency_tracking_id is not unique.")
  { console.log("Inside if",set_object.status,set_object.return_detail);
    logger.info("status ",set_object.status);
    logger.info("return detail ",set_object.return_detail);
    console.log("status "+set_object.status);
    console.log("return detail "+set_object.return_detail);

    setTimeout(function(){
      var Request = appServer.models.Request;
      var query1 = {};
      query1["hpcs_id"] = hpcsId;
      var query2 = {};
      query2["paygov_tracking_id"] = { "eq":  null };

      Request.find({where: {and: [query1,query2]}}, function(err, results){
        /* istanbul ignore if */
        if(err){
          logger.error({err: err}, 'Error in finding req');
        } else {
          if(results.length > 0) {
            logger.info("Updating request in DB ", hpcsId);
            request.update({"hpcs_id": hpcsId},
              set_object, function (err) {
                /* istanbul ignore if */
                if (err) {
                  logger.error({err: err}, 'Error updating request record to DB. Request Not updated.');
                  console.log({err: err}, 'Error updating request record to DB. Request Not updated.');

                } else {
                  logger.info(' Request (HPCS_ID = %s) Updated into DB ', hpcsId);
                  console.log(' Request (HPCS_ID = %s) Updated into DB ', hpcsId);

                }
              })
          }
        }
      });
    }, 1000);
  }
  else{
    logger.info("status ",set_object.status);
    logger.info("return detail ",set_object.return_detail);
    console.log("status "+set_object.status);
    console.log("return detail "+set_object.return_detail);

    var Request = appServer.models.Request;
    Request.update({hpcs_id: hpcsId}, set_object, function (err) {
      /* istanbul ignore if */
      if (err) {
        logger.error({err: err}, 'Error updating request record to DB. Request Not updated.');
      } else {
        logger.info(' Request (HPCS_ID = %s) Updated into DB ', hpcsId);
      }
    })
  }
}

function updateStatueForRequestWithCB(hpcsId, set_object, cb){
  logger.info('Inside updateStatueForRequestWithCB! HPCS_ID = %s  ', hpcsId);
  var Request = appServer.models.Request;
  Request.update( {hpcs_id : hpcsId}, set_object, function(err, response){
    /* istanbul ignore if */
    if(err){
      logger.error({err: err}, 'Error updating request (HPCS_ID = %s) record to DB. Request Not updated.', hpcsId);
      cb(err);
    } else {
      logger.info(' Request (HPCS_ID = %s) Updated into DB ', hpcsId);
      cb(null, response);
    }
  })
}

function isUniqueHPCSId(hpcsId, callback){
  var Request = appServer.models.Request;
  Request.find({where: {hpcs_id : hpcsId}}, function(err, result){
    /* istanbul ignore if */
    if(err){
      logger.info('Error in isUniqueHPCSId request find',err);
    } else {
      logger.info(' UNIQUE HPCS ID ' +result.length);
      if(result.length == 0){
        logger.info(' UNIQUE HPCS ID true ',hpcsId);
        callback(true);
      } else {
        logger.info(' UNIQUE HPCS ID false ',hpcsId);
        callback(false);
      }
    }
  });
}

function fetchRequestFromHPCSId(hpcsId, callback){
  var Request = appServer.models.Request;
  Request.find({where: {hpcs_id : hpcsId}}, function(err, result){
    /* istanbul ignore if */
    if(err){
      console.log(err);
      logger.error({err: err}, 'Cannot find request in DB');
      callback(err);
    } else {
      console.log('FetchRequestFromHPCSId size:');
      console.log(result.length);
      logger.info(' FetchRequestFromHPCSId size: ' + result.length);
      callback(null, result[0]);
    }
  });
}

function getPayGovTrackingId(hpcsId, callback){
  fetchPayGovId(hpcsId,function(err, paygovId){
    if(err){
      callback(err);
    } else if(paygovId != null){
      logger.info("PayGov Id : "+paygovId +" found, for HPCS ID: "+ hpcsId);
      callback(null, paygovId);
    } else {
      console.log("Inside else");
      setTimeout(function(){
        fetchPayGovId(hpcsId, function(err, paygovId){
          if(err){
            callback(err);
          } else if(paygovId != null){
            console.log("PayGov Id : "+paygovId +" found, for HPCS ID: "+ hpcsId);
            logger.info("PayGov Id : "+paygovId +" found, for HPCS ID: "+ hpcsId);
            callback(null, paygovId);
          } else {
            console.log("PayGov Id not found for HPCS ID "+ hpcsId);
            logger.info("PayGov Id not found for HPCS ID "+ hpcsId);
            callback(null, {"paygov_tracking_id": "", "status": "", "return_detail": ""});
          }
        })
      }, 5000);
    }
  });
}

function fetchPayGovId(hpcsId, callback){
  var Request = appServer.models.Request;
  Request.find({where: {hpcs_id: hpcsId}}, function (err, result) {
    /* istanbul ignore if */
    if (err) {
      console.log(err);
      logger.error({err: err}, 'Cannot find request in DB');
      callback(err);
    } else {
      /* istanbul ignore if */
      if (result.length == 1) {
        console.log("paygov_tracking_id: " + result[0].paygov_tracking_id);
        if (result[0].paygov_tracking_id != null || result[0].status == 'Failed') {
          callback(null, result[0]);
        } else {
          callback(null, null);
        }
      } else {
        callback(null, null);
      }
    }
  });
}

exports.fetchRequestFromHPCSId = fetchRequestFromHPCSId;
exports.isUniqueHPCSId = isUniqueHPCSId;
exports.updateStatueForRequest = updateStatueForRequest;
exports.updateStatueForRequestWithCB = updateStatueForRequestWithCB;
exports.persistMessageToDB = persistMessageToDB;
exports.getPayGovTrackingId = getPayGovTrackingId;
exports.isUniqueHPCSId = isUniqueHPCSId;
