var fs = require('fs');

// Read the certificate authority
var ca = fs.readFileSync("/paygov/mongodb.pem");
var cert = fs.readFileSync("/paygov/client.pem");
var key = fs.readFileSync("/paygov/mongodb.pem");

console.log("Loading datasource.local.js....");

module.exports = {
  db: {
  "name": "db",
    "connector": "memory"
},
  hpcs_db: {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "test",
    "name": "hpcs_db",
    "username": "admin",
    "password": "password",
    "connector": "mongodb",
    "ssl":true,
    "server": {
      "auto_reconnect": true,
      "reconnectTries": 100,
      "reconnectInterval": 1000,
      "sslValidate":false,
      "checkServerIdentity":false,
      "sslKey":key,
      "sslCert":cert,
      "sslCA":ca,
      "sslPass":"mongodb"

    }
  }
};

