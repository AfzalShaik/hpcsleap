var loopback = require('loopback');
var boot = require('loopback-boot');
var bunyan = require('bunyan');
var logger = require('./boot/lib/logger');
var fs = require('fs');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./boot/lib/hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

var url = require('url');
var http = require('http');
var https = require('https');
var sslOption = require('./ssl/ssl-cert');
var app = module.exports = loopback();
var domain = require('domain');
var d = domain.create();
var bodyParser = require('body-parser');
var xml2js = require('xml2js');
var parser = xml2js.Parser({ explicitArray: true });
var js2xmlparser = require("js2xmlparser");
var dbService = require("./boot/database-services");
var proxyURL = "http://"+serviceInfo.proxy_url;
var env = serviceInfo.env;

app.use(bodyParser.urlencoded({ extended: false }));

/************** Dashboard Integration code **********/
app.use('**', function (req,res,next) {
  if(req['query'].access_token) {

    var accessToken = req['query'].access_token;
    var SessionInfo = app.models.SessionInfo;
    SessionInfo.find({"where":{"accessToken":accessToken}}, function (err, data){
      if(err){
        next(err,null);
      }else if (data) {
        var currentTime = new Date();
        if(data.length > 0) {
          var currentSession = data[0];
          var lastAccessedTime = currentSession.lastAccessedTime;
          var timeInterval;
          timeInterval = currentTime.getTime() - lastAccessedTime.getTime();
          if(timeInterval < currentSession.sessionDuration){
            currentSession.updateAttributes({"lastAccessedTime":currentTime},function(err,updatedInfo){
              if(err){
                next(err,null);
              }else {
                next();
              }
            });
          }else{
            var error = {'message':'Session Expired'};
            next(error,null);
          }
        }else{
          SessionInfo.create({"accessToken":accessToken,"lastAccessedTime":currentTime,"sessionDuration":(30*60*1000)},function(err,newSession){
            if(err){
              next(err,null);
            }else{
              next();
            }
          });
        }
      }
    });
  }else{
    next();
  }
});
app.use(loopback.context());
app.use(loopback.token());
app.use(function setCurrentUser(req, res, next) {
  if (!req.accessToken) {
    return next();
  } else {
    var loopbackContext = loopback.getCurrentContext();
    if (loopbackContext) {
      loopbackContext.set('accessToken', req.accessToken);
    }
    next();
  }
});


var express = require('express');
var path = require('path');
var request = require('request');
var querystring = require('querystring');
var cors = require('cors');
var http = require('http');
var bodyParser = require('body-parser');
var xml2js = require('xml2js');
var parser = xml2js.Parser({ explicitArray: true });
var path = require('path');
var fs = require('fs');
app.use(cors());

app.post('/settlementReport', cors(),function(req,res){
  console.log ("=====================Inside app settlement=====================");
//  console.log("Body ",JSON.stringify(req.body));

  var machineId =req.body.username;
  var machine_password =req.body.password;
  var report_name = req.body.report_name;
  var agency_id = req.body.agency_id;
  var sdate = req.body.date;
  var HpcsUser = app.models.HpcsUser;
  var request = require('request');

  var formdata = {
    username: machineId,
    password: machine_password,
    agency_id: agency_id,
    report_name: report_name,
    date: sdate
  };
  logger.info("Machine ID  ",machineId);
  logger.info("report name ",report_name);
  logger.info("date " ,sdate);
  logger.info("agency ID",agency_id);
  logger.info("Posting to servlet url ",serviceInfo.paygovReportUrl);
  logger.info("proxy url",proxyURL);
  /*request.post({url:'https://qa.pay.gov/paygov/ReportDownloadServlet',*/
  request.post({url:serviceInfo.paygovReportUrl,
    form: formdata,
    followAllRedirects: true,
     proxy: proxyURL
  }, function(err,httpResponse,body){
    if(body) {
      console.log("Got settlement file",body);
      logger.info("Got settlement file");
      /* parser.parseString(body, function (err, result) {
       if(err){
       console.log("Error ");
       res.end("Error ",body);
       }else{
       var reportFile = result;
       console.log("Report file ---------->",reportFile);
       res.end(body);
       }
       });*/
      res.end(body);
    }
  });

});

app.post('/settlement', cors(),function(req,res){
  logger.info("=====================Inside settlement=====================");
  var username =req.body.username;
  var password =req.body.password;
  var report_name = req.body.report_name;
  var agency_id = req.body.agency_id;
  var sdate = req.body.date;
  var HpcsUser = app.models.HpcsUser;
  var request = require('request');
  logger.info("username =======>",username);
  logger.info("password =======>",password);

  HpcsUser.login({"email":username,"password":password}, function(loginErr, loginResult){
    if(loginErr){
      logger.error("Error while login " +loginErr);
      res.send("Error while fetching user details");
      res.end();
    } else
    {
      HpcsUser.find({where: {email : username}}, function(err, hu_result){
        if(err){
          logger.error('Error while fetching user details '+err);
          res.send("Error while fetching user details");
          res.end();
        } else {
          logger.info("hpcs user ",JSON.stringify(hu_result));
          if(hu_result.length != 1){
            logger.info("Invalid Username or Password.");
            res.send("Unauthorized Access");
            res.end("Unauthorized Access");
          } else if(hu_result.length == 1 && hu_result[0].applicationId != null){
            logger.info("Valid Username or Password.");
            var hUser = hu_result[0];
            logger.info("hu_result "+JSON.stringify(hu_result[0]));
            try{
              if( hUser.applicationId.length > 1){
                logger.info("Invalid Service Account");
                res.send("Invalid Service Account");
                res.end("Invalid Service Account");
              }
              else {
                var tcsAppId = hUser.applicationId[0];
                var appConfig = app.models.ApplicationConfiguration;
                logger.info("tcsAppId ----> " ,tcsAppId);
                appConfig.find({where : {tcsAppId : tcsAppId}}, function(err, ac_result){
                  if(err){
                    logger.error(err);
                    res.end("Server Error");
                  } else {
                      // logger.info("ac_result"+JSON.stringify(ac_result));
                    if(ac_result.length != 1){
                      logger.info("Invalid details.");
                      res.end("Invalid TCSAppId.");
                    } else {
                      var machineId = ac_result[0].machineId;
                      var machine_password = ac_result[0].password;
                      logger.info("machine password",machine_password);
                      if(agency_id == null){
                        agency_id = ac_result[0].agencyId;
                      }
                      var formdata = {
                        username: machineId,
                        password: machine_password,
                        agency_id: agency_id,
                        report_name: report_name,
                        date: sdate
                      };
                      logger.info("Machine ID  ",machineId);
                      logger.info("report name ",report_name);
                      logger.info("date " ,sdate);
                      logger.info("agency ID",agency_id);
                      logger.info("Posting to servlet url ",serviceInfo.paygovReportUrl);
                      logger.info("proxy url",proxyURL);
                      /*request.post({url:'https://qa.pay.gov/paygov/ReportDownloadServlet',*/
                      request.post({url:serviceInfo.paygovReportUrl,
                        form: formdata,
                        followAllRedirects: true,
                        proxy: proxyURL
                      }, function(err,httpResponse,body){
                        if(body) {
                          console.log(body);
                          logger.info("Got settlement file");
                          res.end(body);
                        }
                      });
                    }
                  }

                });
              }
            }catch(err){
              logger.error("Error while processing settlement : "+err);
            }
          }
        }
      });
    }
  });
});


app.post('/ssoLoginSubmit',function(req, res, next) {
  var ssoIDPUrl = getPropValue("ssoIDPUrl", './settings/services.json');
  res.send(ssoIDPUrl);
});

app.use(express.static(path.join(__dirname.replace('server',''), 'client')));


app.set('views', path.join(path.join(__dirname.replace('server',''), 'client'), 'views'));
app.set('view engine', 'jade');
/*SSO redirection code - it is using login.jade to login to dashboard with email id
 TODO : parse the SAML response inside this URL */

app.post('/ssoLogin',function(req, res, next) {

  var b64string = req.body.SAMLResponse ;
  var samlResponse = new Buffer(b64string, 'base64');

  var x509CertString = null;
  var userEmail = null;
  var ssoAuthentication = false;

  parser.parseString(samlResponse, function (err, result) {

    var jsonStr = JSON.stringify(result);

    var jsonObj = JSON.parse(jsonStr);

    for (var key in jsonObj) {

      var responseObj = jsonObj[key];

      for(var innerKey in responseObj){

        if (innerKey == 'ns2:Assertion'){

          var innerKeyObj = responseObj[innerKey];

          for(var innerAssertionKey in innerKeyObj){
            var innerAssertionKeyObj = innerKeyObj[innerAssertionKey];

            for(var subInnerAssertionKey in innerAssertionKeyObj){

              /*signature*/
              if(subInnerAssertionKey == 'ds:Signature'){

                var dsSignatureObj = innerAssertionKeyObj[subInnerAssertionKey];

                for(var dsSignatureKey in dsSignatureObj){
                  var innerDsSignatureObj = dsSignatureObj[dsSignatureKey];

                  for(var subInnerDsSignatureKey in innerDsSignatureObj){

                    if(subInnerDsSignatureKey == 'ds:KeyInfo'){

                      var subInnerDsSignatureObj = innerDsSignatureObj[subInnerDsSignatureKey];

                      for(var subInnerDsSignaturekey in subInnerDsSignatureObj){

                        var subInDsSignatureObj = subInnerDsSignatureObj[subInnerDsSignaturekey];

                        for(var subInDsSignatureKey in subInDsSignatureObj){

                          var signatureX509Obj = subInDsSignatureObj[subInDsSignatureKey];

                          for(var signatureX509Key in signatureX509Obj){

                            var x509CertificateObj = signatureX509Obj[signatureX509Key];

                            for(var x509CertificateKey in x509CertificateObj){

                              var x509CertStr = x509CertificateObj[x509CertificateKey];

                              x509CertString = x509CertStr.toString();
                              x509CertString = x509CertString.replace(/\n/g,""); /*replace new line char with blank*/
                            }

                          }

                        }
                      }

                    }


                  }
                }

              }

              /* Attribute statement */
              if(subInnerAssertionKey == 'ns2:AttributeStatement'){
                var attributeStmtObj = innerAssertionKeyObj[subInnerAssertionKey];

                for(var attributeStmtkey in attributeStmtObj){
                  var innerAttributeStmtObj = attributeStmtObj[attributeStmtkey];
                  for(var innerAttributeStmtkey in innerAttributeStmtObj){
                    var subAttributeStmtObj = innerAttributeStmtObj[innerAttributeStmtkey];
                    for(var subAttributeStmtkey in subAttributeStmtObj){
                      if(subAttributeStmtkey == '4'){
                        var subInAttributeStmtObj = subAttributeStmtObj[subAttributeStmtkey];

                        for(var subInAttributeStmtKey in subInAttributeStmtObj){
                          if(subInAttributeStmtKey == 'ns2:AttributeValue'){
                            userEmail = subInAttributeStmtObj[subInAttributeStmtKey].toString();
                          }
                        }
                      }
                    }
                  }
                }

              }


            }
          }
        }
      }

    }


    try{

      if (x509CertString != undefined) {

        var ssoCert = getPropValue("ssoCert", './settings/services.json');

        if(ssoCert != null){
          if(x509CertString == ssoCert){
            ssoAuthentication = true;
          } else {
            ssoAuthentication = false;
          }
        } else {
          ssoAuthentication = false;
        }
      }
    }catch (ex) {
      res.render('login', {
        message: 'Certificate Not Found',
        userLogin: 'false',
        userName: 'Not Found',
        sso: 'true',
        accessToken: 'Not Found',
        accessTokenExpireTime: 'Not Found',
        userEmail: 'Not Found',
        userId: 'Not Found'
      });

    }
  });

  if(ssoAuthentication){
    /* Call special method and pass emailID.*/
    console.log("ssoAuthentication -->"+ssoAuthentication+"<-- userEmail --->"+userEmail+"<--");

  } else {
    /*Call login erro method*/
    userEmail = "";
  }


  /*var userEmail = "";*/ /*TODO:This email should be from SAML response*/
  var HpcsUser = app.models.HpcsUser;
  var AccessToken = app.models.AccessToken;
  var SessionInfo = app.models.SessionInfo;

  if(userEmail) {
    HpcsUser.find({where: {email: userEmail}}, function (err, userData) {
      if (err) {
        return err;
      } else {
        if (userData.length > 0) {
          AccessToken.create({userId: userData[0].id, ttl: 15000}, function (err, accessTokenInfo) {
            if (err) {
              return err;
            } else {
              var expireTime = accessTokenInfo.created.getTime() + (accessTokenInfo.ttl * 1000);
              var currentTime = new Date();

              SessionInfo.create({"accessToken": accessTokenInfo.id, "lastAccessedTime": currentTime, "sessionDuration": (30 * 60 * 1000)}, function (err, newSession) {

              });
              res.render('login', {userLogin: 'true', userName: userData[0].name, sso: 'true', accessToken: accessTokenInfo.id, accessTokenExpireTime: expireTime, userEmail: userData[0].email, userId: accessTokenInfo.userId});
            }
          });
        } else {
          res.render('login', {message: 'User Not Found', userLogin: 'false', userName: 'Not Found', sso: 'true', accessToken: 'Not Found', accessTokenExpireTime: 'Not Found', userEmail: 'Not Found', userId: 'Not Found'});
        }
      }
    });
  }else{
    res.render('login', {
      message: 'Enter Email',
      userLogin: 'false',
      userName: 'Not Found',
      sso: 'true',
      accessToken: 'Not Found',
      accessTokenExpireTime: 'Not Found',
      userEmail: 'Not Found',
      userId: 'Not Found'
    });
  }
});
/************** Dashboard Integration code **********/


app.start = function(httpOnly) {
  if (httpOnly === undefined) {
    httpOnly = process.env.Http;
  }
  var server = null;
  if (!httpOnly){

    var options = {
      key: sslOption.key,
      cert: sslOption.cert,
      requestCert:true,
      ca:sslOption.ca,
      rejectUnauthorized:false
    };
    server = https.createServer(options,app);
  } else {
    server = http.createServer(app);
  }
  server.listen(app.get('port'), function () {
    app.emit('started');
    logger.info('httpOnly variable is set to %s ', httpOnly);
    var baseUrl = (httpOnly ? 'http://' : 'https://') + app.get('host') + ':' +app.get('port');
    logger.info('Web server listening at: %s', baseUrl)
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      logger.info('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  return server ;
};

/* Runs the server on domain*/
d.run(function() {
  boot(app, __dirname, function(err) {
    if (err) throw err;
    if (require.main === module)
      app.start();
  });
});

/* All exception are captured by block and avoids system crash.*/
d.on('error', function(err) {
  logger.error(err);
});



var agencyUtil = require("./boot/agency-mapper.js");


var HpcsId =[];
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/mainframe',function(req,res){

  res.sendfile("./mainframe/html/index.html");
});
app.get('/settlementReport',function(req,res){
  res.sendfile("./mainframe/html/settlement.html");
});

/*app.get('/result',function(req,res){
  res.status(400).send(HpcsId);
});

app.get('/paymentAgency',function(req,res){
  res.sendfile("./mainframe/html/paymentAgency.html");
});
app.get('/paymentForm',function(req,res){
    console.log("req ",req.query.agency_tracking_id);
    res.sendfile("./mainframe/html/paymentForm.html");
});
app.get('/transactionURL',function(req,res){
     console.log("Inside get transactionURL...Sending response",req.query.action_value);
     res.sendfile("./mainframe/html/transactionURL.html");
});
app.use(express.static(path.join(__dirname.replace('server',''), 'client')));
app.set('views', path.join(path.join(__dirname.replace('server',''), 'client'), 'views'));
app.set('view engine', 'jade');
*/

app.get('/intr',function(req,res){
  var agencyDataResponse = "";
  var agencyId =req.param('agency_id');
  var trackingId =req.param('agency_tracking_id');
  var appName = req.param('app_name');
  console.log("agencyId ",agencyId,trackingId,appName);
  agencyUtil.findAgencyByAppName(appName, function(findErr, agency){
    if(findErr){
      throw findErr;
    } else {
         if(agency != null ) {
            console.log("Agency Found ",agency);
            logger.info("Intr: Agency found ",appName,"with trackingId ",trackingId);
            var transactionURL = agency["retpmtCfmURL"];
            console.log("transactionURL ",transactionURL);
             var hostname = url.parse(transactionURL).hostname;
             var pathname = url.parse(transactionURL).pathname;
             var port = url.parse(transactionURL).port;
             console.log("host ",hostname,pathname);
            var intrPostData = querystring.stringify({
              'agency_id' : agencyId,
              'agency_tracking_id': trackingId,
              'app_name': appName
             });
           //Post to a predefined URL found in appConfig of agency
           //TODO: Replace host and port with pre-registed URL
           var post_options = {
           host: hostname,
           port: port,
           path: pathname,
           method: 'POST',
           headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Content-Length': Buffer.byteLength(intrPostData)
              }
          };

          var post_req = http.request(post_options, function(res) {
          res.setEncoding('utf8');
            res.on('data', function (chunk) {
              agencyDataResponse = agencyDataResponse +chunk;
            });

          res.on('end', function () {
              console.log("End request from LEAP server ",agencyDataResponse);
          });
          });
         /* post_req.on('socket', function (socket) {
                socket.setTimeout(1000);
                socket.on('timeout', function() {
                post_req.abort();
             });
         });*/
          post_req.on('error', function(err) {
                console.log("Timeout occurs");
                agencyDataResponse = "ERRORONDATA";
          });
  
        post_req.write(intrPostData);
        post_req.end();
      }
    }
 
  });
     waitTillReqEnd(res, function () {

    });

    /* istanbul ignore next*/
    function waitTillReqEnd(rs,callback)
    {
      setTimeout(function(){
        if(agencyDataResponse !== "" && agencyDataResponse !== "ERRORONDATA"){
          //if the response from agency is in XML format   
          if(agencyDataResponse.indexOf("xml") != -1){
           try {
                  parser.parseString(agencyDataResponse, function (err, result) {
                  var action = result['collection_request'].action[0].$.value;
                  var protocol_version = result['collection_request'].protocol_version[0].$.value;
                  var response_message = result['collection_request'].response_message[0].$.value;
                  var form_id = result['collection_request'].form_id[0].$.value;
                  var payment_amount = result['collection_request'].payment_amount[0].$.value;
                  var payment_date = result['collection_request'].payment_date[0].$.value;
                  var agency_tracking_id = result['collection_request'].agency_tracking_id[0].$.value;
                  rs.render('paymentForm',{action : action,protocol_version:protocol_version,
                                           response_message : response_message, agencyId : '950',
                                           form_id : form_id,payment_amount : payment_amount,
                                           payment_date : payment_date,agency_tracking_id : agency_tracking_id})
                  rs.end();
                  callback();
              });
              }catch(e){
                   console.log("Error while parsing agency data from LEAP",e);
                   logger.info("Error while parsing agency data from LEAP",e);
                   rs.render('layout',{title : 'Error Page'})
                   rs.end();
              }
           }else {
                  //response is name-value pair
                  var parsedData = JSON.parse(agencyDataResponse);
                  var action = parsedData['action'];
                  var protocol_version = parsedData['protocol_version'];
                  var response_message = parsedData['response_message'];
                  var form_id = parsedData['form_id'];
                  var payment_amount = parsedData['payment_amount'];
                  var payment_date = parsedData['payment_date'];
                  var agency_tracking_id = parsedData['agency_tracking_id'];
                  rs.render('paymentForm',{action : action,protocol_version:protocol_version,
                                           response_message : response_message, agencyId : '950',
                                           form_id : form_id,payment_amount : payment_amount,
                                           payment_date : payment_date,agency_tracking_id : agency_tracking_id})
                  rs.end();
                  callback();

           }
        }else if(agencyDataResponse === "ERRORONDATA") {
          rs.render('layout',{title : 'Error Page'})
          rs.end();
        } 
        else {
          waitTillReqEnd(rs,callback);
        }
      }, 2000);
    }

}); //end intr post

app.get('/processPayment',cors(),function(req,res){
 /*  console.log("Processing payment -----------> " +JSON.stringify(req.body));
   var routingNum = req.body['userData[routing]'];
   var acctNum = req.body['userData[account]'];
   var acctType = req.body['userData[accountType]'];
   var transactionAmt = req.body['userData[paymentAmount]'];
   if(transactionAmt != null || transactionAmt != undefined)
       transactionAmt =   parseFloat(transactionAmt);
   var accountHolder = req.body['userData[accountHolder]'];
   var agencyTrackingId = req.body['agency_tracking_id'];
   var agencyId = req.body['agencyId'];*/
   
   var url_parts = url.parse(req.url, true);
   var query = url_parts.query;
   var parseData = JSON.parse(query["userData"]);
   var successFailPath = "";
   var responseMsg = "";
   var routingNum = parseData['routing'];
   var acctNum = parseData['account'];
   var acctType = parseData['accountType'];
   var transactionAmt = parseData['paymentAmount'];
   if(transactionAmt != null || transactionAmt != undefined)
       transactionAmt =   parseFloat(transactionAmt);
   var accountHolder = parseData['accountHolder'];
   var agencyTrackingId = req.query['agency_tracking_id'];
   var agencyId = req.query['agencyId'];


   var leapTransaction = {
    "agency_id": agencyId,
    "tcs_app_id": "TCSHUDSFMPCR",
    "ACHDebit": {
      "agency_tracking_id": agencyTrackingId,
      "account_number": acctNum,
      "routing_transit_number": routingNum,
      "account_type": acctType,
      "transaction_amount":transactionAmt ,
      "first_name": accountHolder,
      "last_name" : "doe",
      "business_name": "AAAE"
    }
  }
   //console.log("Transaction ---> ",JSON.stringify(leapTransaction));
   callPayGovWrapper(leapTransaction,'/api/TCSSingleServiceONLINEWrappers/processACHDebit', function (err,ackHpcsId) {

    if(err){
      logger.info('Error while getting HPCS ID back from paygov',err);
      throw err;
    } else {
      if (ackHpcsId != null) {
          dbService.getPayGovTrackingId(ackHpcsId.result.hpcs_id, function (paygovIdErr, resp) {
          var paygovTrackingId = "";
          paygovTrackingId = resp.paygov_tracking_id;
          var trackingId = resp.agency_tracking_id;
          console.log("paygov tracking id------> ",paygovTrackingId);

          if (paygovTrackingId == 'null' || !paygovTrackingId.trim())
          {
              successFailPath = "http://96.127.58.209/leapSimulator/fail.html";
          }
          else 
          {
              successFailPath = "http://96.127.58.209/leapSimulator/success.html";
          }
          if(successFailPath !== "")
          {
             //Send response to agency results page
              console.log("Sending response to results page ....",successFailPath);
               var resultPostData = querystring.stringify({
              'paygov_tracking_id' : paygovTrackingId,
              'submitted_on': '',
              'payment_amount':'',
              'account_id_data':'',
              'agency_id':agencyId,
              'payment_status':'',
              'payer_name':'',
              'payment_date':'',
              'agency_tracking_id':''              
             });
           /* paygov_tracking_id=3FO91M1H&return_account_id_data=Partial&submit
ted_on=01%2F10%2F2005+19%3A15%3A56+UTC&payment_amount=10000.99&ac
count_id_data=XXXXXXXXXXXX1111&agency_id=81101&payment_type=Credi
tCard&payer_address=123+S.+18th+Street&payment_status=Completed&p
ayer_name=John+Doe&payment_date=01%2F10%2F2005&agency_tracking_id
=9999&approval_code=548828&avs_response_code=A&auth_response_code
=00&auth_response_text=Approved*/

               var result_post_options = {
               host: '96.127.58.209',
               port: 80,
               path: '/leapSimulator/resultURL',
               method: 'POST',
               headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'Content-Length': Buffer.byteLength(resultPostData)
               }
              };

              var result_post_req = http.request(result_post_options, function(res) {
              res.setEncoding('utf8');
               res.on('data', function (chunk) {
                 responseMsg = responseMsg +chunk;
               });

               res.on('end', function () {
                 console.log("End request from LEAP Results Page ",responseMsg);
               });

             });
             result_post_req.on('error', function(err) {
                responseMsg = "ERRORONDATA";
              });
              result_post_req.write(resultPostData);
              result_post_req.end();

          }
       })
      }else{
        throw new Error("Unable to post transaction to PAYGOV");
      }
    }

  })
    waitTillReqEnd(res, function () {

    });

    /* istanbul ignore next*/
    function waitTillReqEnd(rs,callback)
    {
      setTimeout(function(){
        if(successFailPath !== "" && responseMsg !== "" ){
           rs.send(successFailPath)
           rs.end();
           callback();
        }else {
          waitTillReqEnd(rs,callback);
        }
      }, 2000);
    }
    

});//End processPayment

app.post('/cfm',cors(),function(req,res){
  var agencyId =req.body.agency_id;
  var trackingId =req.body.agency_tracking_id;
  var appName = req.body.app_name;
  var transactionInfo = null;
  var statusResult = "OK";
  var reqEndFlag = false;
  ip = req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;
  logger.info('IP address of the request %s ', ip);
  logger.info('IP address %s ', ip);
  logger.info('Agency Id %s ', agencyId);
  logger.info('Tracking Id %s ', trackingId);
  logger.info('App Name %s ', appName);

  var post_data = querystring.stringify({
    'agency_id' : agencyId,
    'agency_tracking_id': trackingId,
    'app_name': appName
  });

  agencyUtil.findAgencyByAppName(appName, function(findErr, agency){
    if(findErr){
      throw findErr;
    } else {

      if(agency != null && agency["whitelistedIPs"] != null){
        //var whitelistedArray = agency["whitelistedIPs"];
        /* if(agency != null && whitelistedArray.indexOf(ip) > -1) {*/
        logger.info('Agency Found ===> ');
        logger.info('tcsAppId %s ', agency["tcsAppId"]);
        var retpmtCfmURL = agency["retpmtCfmURL"];
        var resultCfmUrl = agency["resultCfmURLs"];
        /*var proxyURL = "https://"+serviceInfo.proxy_url;
         logger.info("proxyURL %s",proxyURL);
         var hostname = url.parse(proxyURL).hostname;
         var port = url.parse(proxyURL).port;*/

        var hostname = url.parse(retpmtCfmURL).hostname;
        var pathname = url.parse(retpmtCfmURL).pathname;
        logger.info('Hostname from retpmtCfmURL  %s ', hostname);
        logger.info('pathname %s ', pathname);
        var tcsAppId = agency["tcsAppId"];

        console.log("Env ------------>",env);
        logger.info("Env ---------->",env);
        var options = {};
        if (env.toString().toUpperCase().trim() === "PROD"
            || env.toString().toUpperCase().trim() === "STAGE"){
          options = {
            host: hostname,
            port: 443,
            path: pathname,
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Content-Length': Buffer.byteLength(post_data)
            }
          };
        }
        else  if (env.toString().toUpperCase().trim() === "QA"){
          pathname = "/cfmSimulator/Retpmtcfm";
          options = {
            host: '10.215.11.18',
            port: 8080,
            path: pathname,
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Content-Length': Buffer.byteLength(post_data)
            }
          };
          https = http;
        }

        logger.info ("options ---->",JSON.stringify(options));
        console.log("options ----->",JSON.stringify(options));

        logger.info("Calling ENTP server....",reqEndFlag,trackingId);
        var post_req = https.request(options, function (res) {

          res.setEncoding('utf8');
          var completeResponse = "";
          res.on('data', function(chunk){
            completeResponse = completeResponse + chunk;
          });

          res.on('end', function () {
            logger.info('Response from coldfusion ',trackingId, completeResponse);
            try {
              parser.parseString(completeResponse, function (err, result) {
                transactionInfo = result;
                var tcsOperation = "";
                try {
                  if (transactionInfo['collection_request'] != undefined) {
                    //var singleTrans = transactionInfo['collection_request'].action[0].$.value;
                    var ociTrans = transactionInfo['collection_request'].batch_request[0].collection_auth;
                    var collectionCount = ociTrans.length;

                    for (var c = 0; c < collectionCount; c++) {

                      var ociOperation = ociTrans[c].account_data[0].payment_type;

                      var ociOp = JSON.stringify(ociOperation[0].$.value);
                      logger.info('ociOp %s ', ociOp);

                      tcsOperation = getPropValue(ociOp.toString(),'./settings/operations.json');
                      logger.info('tcsOperaion ', tcsOperation);
                      tcsServiceUrl = getPropValue(tcsOperation.toString(), './settings/serviceURL.json');

                      var tcsTransaction = convertToTcs(agencyId, tcsAppId,tcsOperation,ociTrans[c]);

                      callPayGovWrapper(tcsTransaction,tcsServiceUrl, function (err,ackHpcsId) {

                        if(err){
                          logger.info('Error while getting HPCS ID back from paygov',err);
                          logger.info('Error while getting HPCS ID back from paygov');
                          throw err;
                        } else {
                          if (ackHpcsId != null) {
                            if (ackHpcsId.result.hpcs_id != null) {
                              dbService.getPayGovTrackingId(ackHpcsId.result.hpcs_id, function (paygovIdErr, resp) {

                                var xmlResult = transformJSONtoXML(tcsTransaction, tcsOperation, ackHpcsId, resp);
                                callResultCfm(resultCfmUrl, xmlResult, function (err) {
                                  if (err) {
                                    logger.info(err);
                                  } else {
                                    logger.info("Response from Result cfm");
                                  }
                                })
                              })
                            }
                          }else{
                            throw new Error("Unable to post transaction to PAYGOV");
                          }
                        }

                      })
                    }
                  }

                }
                catch (ex) {
                  logger.info('Error while fetching data from ENTP server ',trackingId);
                  statusResult = "Error while fetching data from ENTP server";
                  reqEndFlag = true;
                }
              });
            } catch(e)
            {
              logger.info('Error while parsing data from ENTP server ',trackingId);
              statusResult = "Error while parsing data from ENTP server";
              reqEndFlag = true;

            }
            logger.info("Finished Processing coldfusion req ",trackingId,statusResult);
            reqEndFlag = true;
          });

        });
        post_req.write(post_data);
        post_req.on('error', function () {
          logger.info('Problem with connecting the ENTP server');
          statusResult = "Problem with connecting the ENTP server";
          reqEndFlag = true;
        })

        post_req.end();
        /* }else{
         statusResult = "IP is not whitelisted";
         logger.info("End Request with IP not whitelisted  =======>",statusResult);
         reqEndFlag = false;
         res.end("IP not whitelisted");

         }*/
      }else{
        statusResult = "Agency Not Found";
        logger.info("End Request with Agency Not Found  =======>",statusResult);
        reqEndFlag = true;
        res.end("Agency Not Found");
      }
    }

    waitTillReqEnd(res, function () {

    });

    /* istanbul ignore next*/
    function waitTillReqEnd(rs,callback)
    {
      setTimeout(function(){
        logger.info("Inside wait flag",reqEndFlag);

        if(reqEndFlag){
          logger.info("End Request main  =======>",trackingId, statusResult);
          rs.end(statusResult);
          callback();
        }else{
          waitTillReqEnd(rs,callback);
        }
      }, 2000);
    }

  });
});

/* istanbul ignore next*/
function transformJSONtoXML(requestSet,opName,response,respPaygov){
  var total_amount = 0;
  var data = {
    control_total: {
      "transaction_count" : {
        "@" : {
          "value": 1
        }
      }
    },
    "collection_auth_result": {
      agency_tracking_id: {
        "@": {
          "value": getAgencyTrackingId(requestSet, opName)
        }
      },
      paygov_tracking_id: {
        "@": {
          "value": respPaygov.paygov_tracking_id
        }
      },
      payment_status: {
        "@": {
          "value": getPayGovStatus(respPaygov.status)
        }
      }
    }
  };
  if(requestSet!= undefined){
    total_amount +=getTransactionAmount(requestSet);
  }
  data.control_total.total_amount = {
    "@" : {
      "value": total_amount
    }
  }
  data.control_total.net_amount = {
    "@" : {
      "value": total_amount
    }
  }
  if(respPaygov.status == 'Failed'){
    data.collection_auth_result.error_detail = {
      "@" : {
        "value": respPaygov.return_detail
      }
    }
    data.collection_auth_result.error_message = {
      "@" : {
        "value": respPaygov.return_detail
      }
    }
  }

  var outputXML = js2xmlparser("collection_result", data);
  logger.info("output XML ",outputXML);
  return outputXML;
}
/* istanbul ignore next*/
function getPayGovStatus(status)
{
  if(status == 'Received')
  {
    return 'Completed';
  }else{
    return status;
  }
}
/* istanbul ignore next*/
function getTransactionAmount(requestjs){
  if(requestjs.ACHDebit != undefined) {
    return requestjs.ACHDebit.transaction_amount;
  } else if(requestjs.ACHPrenotification != undefined){
    return requestjs.ACHPrenotification.transaction_amount;
  }
}
/* istanbul ignore next*/
function getAgencyTrackingId(request, operation){
  var agencyTrackingId = request.agency_tracking_id;
  if(agencyTrackingId == null) {
    agencyTrackingId = request[operation].agency_tracking_id;
  }
  logger.info('The Agency Tracking Id from request is %s ', agencyTrackingId)
  return agencyTrackingId;
}
app.use(bodyParser.json());


app.post('/postQueryStatus', function(req, res){
  var obj = {};
  var url = req.body.settlementurl;
  var transactions = req.body.transactions;
  callSettlementUrl(url, transactions, function (err, response) {
    if (err) {
      logger.info(err);
      statusResult = "Error";
      res.send("Error While Connecting to the server");
    } else {
      logger.info("Response from status cfm");
      res.send(response);
    }
  })
});

/* istanbul ignore next*/
function callSettlementUrl(url, transactions, cb ){
  var request = require('request');
  logger.info('Status CFM URL ',url);
  var statusURL = url +"?"+transactions;
  logger.info("staustURL ",statusURL);
  request({
    url: statusURL,
    method: "POST",
    body: transactions,
    headers:{
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }, function(err, response, body) {
    if (err) {
      cb(err);
    } else {
      logger.info('Response from CFM ',body);
      cb(null, body);
    }
  });
}

/* istanbul ignore next*/
function callResultCfm(url, transactions, cb ){
  var request = require('request');
  logger.info('Result CFM URL ',url);
  request({
    url: url,
    method: "POST",
    body: transactions,
    headers:{
      'Content-Type': 'application/xml'
    }
  }, function(err, response, body) {
    if (err) {
      cb(err);
    } else {
      logger.info('Response from CFM ',body);
      cb(null, body);
    }
  });
}
/* istanbul ignore next*/
function convertToTcs(agencyId,tcsAppId,tcsOperation,ociTrans)
{

  var collection_req = {
  };

  collection_req["agency_id"] = agencyId;
  collection_req["tcs_app_id"] = tcsAppId;

  var transData = {};
  transData[tcsOperation] = [];

  var t = {};

  var pa = parseFloat(ociTrans.payment_amount[0].$.value);
  t["agency_tracking_id"] = ociTrans.agency_tracking_id[0].$.value;
  t["account_number"] = ociTrans.account_data[0].bank_account_number[0].$.value;
  t["routing_transit_number"] = ociTrans.account_data[0].bank_number[0].$.value;
  var acctType = ociTrans.account_data[0].bank_account_type[0].$.value;
  if(acctType != null && acctType == 'Checking'){
    t["account_type"] = "BusinessChecking";
  }
  else {
    t["account_type"] = acctType;
  }

  t["transaction_amount"] = pa;


  if(tcsOperation === "ACHDebit") {
    t["first_name"] = ociTrans.account_data[0].payer_name[0].$.value;
    t["last_name"] = ociTrans.account_data[0].payer_name[0].$.value;
    t["business_name"] = ociTrans.account_data[0].payer_name[0].$.value;
  }

  transData[tcsOperation].push(t);

  collection_req[tcsOperation]=t;

  //collection_req = JSON.stringify(collection_req).replace(/[\[\]']+/g,'');
  return collection_req;
}

/* istanbul ignore next*/
function convertCancelToTcs(agencyId,tcsAppId,tcsOperation,ociTrans)
{

  var collection_req = {
  };

  collection_req["agency_id"] = agencyId;
  collection_req["tcs_app_id"] = tcsAppId;

  var transData = {};
  transData[tcsOperation] = [];

  var t = {};

  var pa = parseFloat(ociTrans.payment_amount[0].$.value);
  t["orig_paygov_tracking_id"] = ociTrans.agency_tracking_id[0].$.value;
  t["transaction_amount"] = pa;
  t["agency_tracking_id"] = ociTrans.agency_tracking_id[0].$.value;


  transData[tcsOperation].push(t);

  collection_req[tcsOperation]=t;

  collection_req = JSON.stringify(collection_req).replace(/[\[\]']+/g,'');
  return collection_req;
}

function stripAlphaChars(source) {
  var out = source.replace(/[^0-9]/g, '');
  return out;
}
/* istanbul ignore next*/
function callPayGovWrapper(input,serviceurl,cb){
  logger.info('****************callPayGovWrapper*********************');

  var sslOption = require('./ssl/ssl-cert');
  /*var baseUrl =  'https://' + app.get('host') + ':' + app.get('port');
   var url = baseUrl + serviceurl;*/

  getAccessTokenForApi('mainframe@hpcs.com', function(tokenErr, token){
    if(tokenErr){
      throw tokenErr;
    } else {
      var baseUrl = 'https://' + app.get('host') + ':' + app.get('port');
      var url = baseUrl + serviceurl + '?access_token='+token;

      var request = require('request');
      logger.info('Request Sending to  %s ', url);
      logger.info('Request ====> %s ', input);
      request({
        url: url,
        method: "POST",
        json: true,
        body: input,
        key: sslOption.key,
        cert: sslOption.cert,
        requestCert:true,
        ca:sslOption.ca,
        rejectUnauthorized:false
      }, function(err, response, body) {
        if(err) { cb(err); }
        cb(null,body,input.agency_tracking_id);
      });


    }
  });
}

/* istanbul ignore next*/
function getAccessTokenForApi(email, callback){
  var userEmail = email;
  var HpcsUser = app.models.HpcsUser;
  var AccessToken = app.models.AccessToken;

  HpcsUser.find({where:{email:userEmail}}, function(err, userData){
    if(err){
      callback(err);
    }else{
      if(userData.length > 0){
        AccessToken.create({userId:userData[0].id}, function(err, accessTokenInfo){
          if(err){
            callback(err);
          }else{
            logger.info('Access token created %s ', JSON.stringify(accessTokenInfo));
            callback(null, accessTokenInfo.id);
          }
        });
      }else{
        var error = {message: 'User Not Found'};
        callback(err);
      }
    }
  });
}
/* istanbul ignore next*/
function getPropValue(keyName,configurationFile)
{
  var keyName = keyName.replace(/\s+/g, '').replace(/\"/g, "");

  var tcsValue = null;
  try{
    var prop = fs.readFileSync(configurationFile);
    var tcsData = JSON.parse(prop);
    for (name in tcsData) {
      if (name == keyName) {
        tcsValue = tcsData[keyName];
      }
    }
  }catch (e){
    throw  e;
  }
  return tcsValue;

}


