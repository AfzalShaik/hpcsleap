/**
 * file: mongo-ssl-cert.js is deprecated for now ... please do not use
 **/

var fs  = require('fs');

var options = {

  // Specify the client key file for the server
  key: fs.readFileSync('./server/ssl/client1-key.pem'),

  // Specify the client certificate file
  cert: fs.readFileSync('./server/ssl/clnt.pem'),

  //cat client1-key.pem client1-crt.pem > clnt.pem
  // This is where the magic happens in Node.  All previous
  // steps simply setup SSL (except the CA).  By requesting
  // the client provide a certificate, we are essentially
  // authenticating the user.
  requestCert:true,

  // Specify the Certificate Authority certificate
  ca: [fs.readFileSync('./server/ssl/dem-ca-crt.pem')],

  // If specified as "true", no unauthenticated traffic
  // will make it to the route specified.
  rejectUnauthorized:false

  // This is the password used when generating the server's key
  // that was used to create the server's certificate.
  // And no, I do not use "password" as a password.
  //passphrase: "password"
}

module.exports = options;
