/**
 * file: ssl_cert.js
 **/

/*
var crypto = require('crypto'),
  fs = require("fs"),
  path = require('path');

exports.privateKey = fs.readFileSync(path.join(__dirname, 'dem-serv-private-key.pem')).toString();
exports.certificate = fs.readFileSync(path.join(__dirname, 'dem-serv-cert.pem')).toString();

exports.credentials = crypto.createCredentials({
  key: exports.privateKey,
  cert: exports.certificate
});
*/
var fs  = require('fs');
var options = {

  // Specify the key file for the server
  key: fs.readFileSync('./server/ssl/dem-serv-private-key.pem'),

  // Specify the certificate file
  cert: fs.readFileSync('./server/ssl/dem-serv-cert.pem'),

  // This is where the magic happens in Node.  All previous
  // steps simply setup SSL (except the CA).  By requesting
  // the client provide a certificate, we are essentially
  // authenticating the user.
  requestCert:true,

  // Specify the Certificate Authority certificate
  ca: [fs.readFileSync('./server/ssl/dem-ca-crt.pem')],

  // If specified as "true", no unauthenticated traffic
  // will make it to the route specified.
  rejectUnauthorized:false

  // This is the password used when generating the server's key
  // that was used to create the server's certificate.
  // And no, I do not use "password" as a password.
  //passphrase: "password"
}


module.exports = options;
