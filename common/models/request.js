var server = require('../../server/server');
var encryptUtil = require('../../server/boot/lib/encrypt-util');
/* istanbul ignore next*/
module.exports = function(Request) {


  Request.observe('persist', function(ctx, next) {
    //console.log("-------persist--------");
    if(ctx.data != undefined){
      if(ctx.data.request != undefined) {
        ctx.data.request = encryptUtil.encrypt(ctx.data.request);
      }
      if(ctx.data.response != undefined) {
        ctx.data.response = encryptUtil.encrypt(ctx.data.response);
      }
      //console.log(ctx.data);
    }
    next();
  });

  Request.observe('loaded', function(ctx, next) {
    //console.log("-------loaded--------");
    if (ctx.data != undefined) {
      if (ctx.data.request != undefined) {
        ctx.data.request = encryptUtil.decrypt(ctx.data.request);
      }
      if(ctx.data.response != undefined) {
        ctx.data.response = encryptUtil.decrypt(ctx.data.response);
      }
      //console.log(ctx.data);
    } else if(ctx.instance != undefined) {
      if (ctx.instance.request != undefined) {
        ctx.instance.request = encryptUtil.decrypt(ctx.instance.request);
      }
      if(ctx.instance.response != undefined) {
        ctx.instance.response = encryptUtil.decrypt(ctx.instance.response);
      }
      //console.log(ctx.instance);
    }
    next();
  });

  Request.getTransactionsCount = function (filterQuery, filterDateQuery, filterAgencyTrackingId ,cb) {
    var cb = cb;
    var AppConfiguration = server.models.ApplicationConfiguration;

    var filterObj = {};
    if(filterQuery){
      filterObj = {"where":{"tcsAppId":filterQuery}};
    }

    AppConfiguration.find(filterObj,function(err, apps){
      if(err){
        cb(err,null);
      }else {
        if(apps.length > 0){
          var transactionCount = [];
          var j=0;
          function getCount(i){
            if(i < apps.length){
              var countObj = {};
              countObj['appName'] = apps[i].name;
              countObj['appId'] = apps[i].tcsAppId;
              var totalCount = [], initiatedCount = [], submittedCount = [], settledCount = [], failedCount = [], cancelCount = [];
              if(filterDateQuery.length>0){
                totalCount.push({created_date :{between:filterDateQuery}});
                initiatedCount.push({created_date :{between:filterDateQuery}});
                submittedCount.push({created_date :{between:filterDateQuery}});
                failedCount.push({created_date :{between:filterDateQuery}});
                cancelCount.push({created_date :{between:filterDateQuery}});
                settledCount.push({created_date :{between:filterDateQuery}});
                //}else{
                //  var currentTime = new Date();
                //  var currentDate = new Date().setHours(0,0,0,0);
                //  currentDate = new Date(currentDate);
                //  var dateRange = [];
                //  dateRange.push(currentDate);
                //  dateRange.push(currentTime);
                //  console.log('Date Range...:'+JSON.stringify(dateRange));
                //  totalCount.push({created_date :{between:dateRange}});
                //  initiatedCount.push({created_date :{between:dateRange}});
                //  submittedCount.push({created_date :{between:dateRange}});
                //  failedCount.push({created_date :{between:dateRange}});
              }
              totalCount.push({tcs_app_id :apps[i].tcsAppId});
              initiatedCount.push({tcs_app_id :apps[i].tcsAppId},{"status":"Initiated"});
              submittedCount.push({tcs_app_id :apps[i].tcsAppId},{"or":[{"status":"Submitted"},{"status":"Received"}]});
              failedCount.push({tcs_app_id :apps[i].tcsAppId},{"status":"Failed"});
              cancelCount.push({tcs_app_id :apps[i].tcsAppId},{"status":"Cancelled"});
              settledCount.push({tcs_app_id :apps[i].tcsAppId},{"status":"Settled"});

              if(filterAgencyTrackingId){
                totalCount.push({agency_tracking_id :{"like":filterAgencyTrackingId}});
                initiatedCount.push({agency_tracking_id :{"like":filterAgencyTrackingId}});
                submittedCount.push({agency_tracking_id :{"like":filterAgencyTrackingId}});
                failedCount.push({agency_tracking_id :{"like":filterAgencyTrackingId}});
                cancelCount.push({agency_tracking_id :{"like":filterAgencyTrackingId}});
                settledCount.push({agency_tracking_id :{"like":filterAgencyTrackingId}});
                countObj['agencyTrackingId'] = filterAgencyTrackingId;
              }

              Request.count({"and":totalCount}, function(err, transactions) {
                if(err){
                  cb(err,null);
                }else{
                  if(transactions > 0) {
                    countObj['totalTransactions'] = transactions;
                  }else{
                    countObj['totalTransactions'] = 0;
                  }
                }
                Request.count({"and":initiatedCount}, function(err, queueCount) {
                  if(err){
                    cb(err,null);
                  }else{
                    countObj['initiated'] = queueCount;
                  }
                  Request.count({"and":submittedCount}, function(err, sentPayGovCount) {
                    if(err){
                      cb(err,null);
                    }else{
                      countObj['submitted'] = sentPayGovCount;
                    }
                    Request.count({"and":settledCount}, function(err, settled) {
                      if (err) {
                        cb(err, null);
                      } else {
                        countObj['settled'] = settled;
                      }
                      Request.count({"and": cancelCount}, function (err, cancelledCount) {
                        if (err) {
                          cb(err, null);
                        } else {
                          countObj['cancelled'] = cancelledCount;
                        }
                        Request.count({"and": failedCount}, function (err, responsePayGovCount) {
                          if (err) {
                            cb(err, null);
                          } else {
                            countObj['failed'] = responsePayGovCount;
                          }
                          transactionCount.push(countObj);
                          j++;
                          if ((i + 1) == apps.length) {
                            cb(null, transactionCount);
                          } else {
                            getCount(i + 1);
                          }
                        });
                      });
                    });
                  });
                });
              });
            }
          }
          getCount(0);
        }else{
          cb(null,'No Applications Found');
        }
      }
    });

  };
  /* istanbul ignore next*/
  Request.remoteMethod('getTransactionsCount', {
    description: "To get all the transactions count based on status and application",
    returns: {
      arg: 'TransactionCount',
      type: 'array'
    },
    accepts: [{arg: 'filterQuery', type: 'string', http: {source: 'query'}},
      {arg: 'filterDateQuery', type: 'array', http: {source: 'query'}},
      {arg: 'filterAgencyTrackingId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getTransactionsCount',
      verb: 'get'
    }
  });
  /* istanbul ignore next*/
  Request.getGraphData = function (filterAppId, cb) {

    var HpcsUser = server.models.HpcsUser;
    var HpcsRole = server.models.HpcsRole;

    var graphStatistics = {
      transactions: {
        title: 'Transactions',
        count: []
      },
      users: {
        title: 'Users',
        count: []
      },
      todayTransactions:{
        title: 'Today Transactions',
        count: []
      }
    };

    var todayStartTime = new Date().setHours(0,0,0,0);
    todayStartTime = new Date(todayStartTime);
    var todayEndTime = new Date().setHours(23,59,59,59);
    todayEndTime = new Date(todayEndTime);
    var currentDayFilter = [todayStartTime, todayEndTime];

    var totalCount = [], initiatedCount = [], submittedCount = [], failedCount = [], cancelCount = [];
    if(filterAppId){
      totalCount.push({"tcs_app_id":filterAppId});
      initiatedCount.push({tcs_app_id :filterAppId},{"status":"Initiated"});
      submittedCount.push({tcs_app_id :filterAppId},{"or":[{"status":"Submitted"},{"status":"Received"}]});
      failedCount.push({tcs_app_id :filterAppId},{"status":"Failed"});
      cancelCount.push({tcs_app_id :filterAppId},{"status":"Cancelled"});
    }else{
      totalCount.push({});
      initiatedCount.push({"status":"Initiated"});
      submittedCount.push({"or":[{"status":"Submitted"},{"status":"Received"}]});
      failedCount.push({"status":"Failed"});
      cancelCount.push({"status":"Cancelled"});
    }
    /* istanbul ignore next*/
    Request.count({"and":totalCount}, function(err, transactions) {
      if(err){
        cb(err,null);
      }else{
        if(transactions > 0) {
          graphStatistics.transactions.count.push({key:'Total Transactions',count:transactions});
        }else{
          graphStatistics.transactions.count.push({key:'Total Transactions',count:0});
        }
      }

      Request.count({"and":initiatedCount}, function(err, queueCount) {
        if(err){
          cb(err,null);
        }else{
          graphStatistics.transactions.count.push({key:'Ready to Process Transactions',count:queueCount});
        }
        Request.count({"and":submittedCount}, function(err, sentPayGovCount) {
          if(err){
            cb(err,null);
          }else{
            graphStatistics.transactions.count.push({key:'Send to Pay.gov Transactions',count:sentPayGovCount});
          }
          Request.count({"and":cancelCount}, function(err, cancelledCount) {
            if (err) {
              cb(err, null);
            } else {
              graphStatistics.transactions.count.push({key: 'Cancelled Transactions', count: cancelledCount});
            }
            Request.count({"and": failedCount}, function (err, responsePayGovCount) {
              if (err) {
                cb(err, null);
              } else {
                graphStatistics.transactions.count.push({key: 'Failed Transactions', count: responsePayGovCount});
              }

              /***************** For current day transactions START*************************************************/
              totalCount.push({created_date :{between:currentDayFilter}});
              initiatedCount.push({created_date :{between:currentDayFilter}});
              submittedCount.push({created_date :{between:currentDayFilter}});
              failedCount.push({created_date :{between:currentDayFilter}});
              cancelCount.push({created_date :{between:currentDayFilter}});
              Request.count({"and": totalCount}, function(err, transactions) {
                if (err) {
                  cb(err, null);
                } else {

                  if (transactions > 0) {
                    graphStatistics.todayTransactions.count.push({key: 'Total Transactions', count: transactions});
                  } else {
                    graphStatistics.todayTransactions.count.push({key: 'Total Transactions', count: 0});
                  }
                }

                Request.count({"and": initiatedCount}, function (err, queueCount) {
                  if (err) {
                    cb(err, null);
                  } else {
                    graphStatistics.todayTransactions.count.push({key: 'Ready to Process Transactions', count: queueCount});
                  }
                  Request.count({"and": submittedCount}, function (err, sentPayGovCount) {
                    if (err) {
                      cb(err, null);
                    } else {
                      graphStatistics.todayTransactions.count.push({
                        key: 'Send to Pay.gov Transactions',
                        count: sentPayGovCount
                      });
                    }
                    Request.count({"and": cancelCount}, function (err, cancelledCount) {
                      if (err) {
                        cb(err, null);
                      } else {
                        graphStatistics.todayTransactions.count.push({key: 'Cancelled Transactions', count: cancelledCount});
                      }
                      Request.count({"and": failedCount}, function (err, responsePayGovCount) {
                        if (err) {
                          cb(err, null);
                        } else {
                          graphStatistics.todayTransactions.count.push({
                            key: 'Failed Transactions',
                            count: responsePayGovCount
                          });
                        }
                        /************** For current day transactions END*************************************************/
                        HpcsRole.find({}, function (err, hpcsRoles) {
                          if (err) {
                            cb(err, null);
                          } else {
                            if (hpcsRoles.length > 0) {
                              function getUsersCount(i) {
                                HpcsUser.count({"role": hpcsRoles[i].name}, function (err, userCount) {
                                  if (err) {
                                    cb(err, null);
                                  } else {
                                    graphStatistics.users.count.push({key: hpcsRoles[i].name, count: userCount});
                                  }

                                  if ((i + 1) == hpcsRoles.length) {
                                    cb(null, graphStatistics);
                                  } else {
                                    getUsersCount(i + 1);
                                  }
                                });
                              }

                              getUsersCount(0);
                            }
                          }
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  };
  /* istanbul ignore next*/
  Request.remoteMethod('getGraphData', {
    description: "To get the data of transactions and users, to display in graphs",
    returns: {
      arg: 'graphData',
      type: 'object'
    },
    accepts: [{arg: 'filterAppId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getGraphData',
      verb: 'get'
    }
  });

  Request.getTransactionsDetails = function (transactionId, cb) {
    Request.findById(transactionId,function(err, transactionData){
      if(err){
        cb(err,null);
      }else{
        if (transactionData != undefined) {
          //console.log('Transaction Details:'+JSON.stringify(transactionData));
          //if (transactionData.request != undefined) {
          //  console.log('Transaction Details:'+JSON.stringify(transactionData.request));
          //  transactionData.request = encryptUtil.decrypt(transactionData.request);
          //}
          //if(transactionData.response != undefined) {
          //  transactionData.response = encryptUtil.decrypt(transactionData.response);
          //}
        }
        //console.log('Transaction Details:'+JSON.stringify(transactionData));
        cb(null,transactionData);
      }
    });
  };
  /* istanbul ignore next*/
  Request.remoteMethod('getTransactionsDetails', {
    description: "To get the details of a transaction",
    returns: {
      arg: 'transactionData',
      type: 'object'
    },
    accepts: [{arg: 'transactionId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getTransactionsDetails',
      verb: 'get'
    }
  });
  /* istanbul ignore next*/
  Request.getLineGraphCount = function (filterAppId, cb) {

    var graphStatistics = {
      transactionsData: [],
      dates: []
    };

    var todayStartTime = new Date().setHours(0,0,0,0);
    var days_30 = todayStartTime - 29*86400*1000;

    var totalCountArray = [], initiatedCountArray = [], submittedCountArray = [], failedCountArray = [], settledCountArray = [], cancelCountArray = [], datesArray=[];

    function getData(i){
      var totalCount = [], initiatedCount = [], submittedCount = [], failedCount = [], settledCount = [], cancelCount = [];

      var filterStartDate = new Date(days_30).setHours(0,0,0,0);
      filterStartDate = new Date(filterStartDate);
      var filterEndDate = new Date(days_30).setHours(23,59,59,999);
      filterEndDate = new Date(filterEndDate);
      var currentDayFilter = [filterStartDate, filterEndDate];

      //var dateString = (filterStartDate.getMonth()+1)+'/'+filterStartDate.getDate()+'/'+filterStartDate.getFullYear();
      var dateString = (filterStartDate.getMonth()+1)+'/'+filterStartDate.getDate();
      datesArray.push([i,dateString]);

      if(filterAppId){
        totalCount.push({"tcs_app_id":filterAppId},{created_date :{between:currentDayFilter}});
        initiatedCount.push({tcs_app_id :filterAppId},{"status":"Initiated"},{created_date :{between:currentDayFilter}});
        submittedCount.push({tcs_app_id :filterAppId},{"or":[{"status":"Submitted"},{"status":"Received"}]},{created_date :{between:currentDayFilter}});
        failedCount.push({tcs_app_id :filterAppId},{"status":"Failed"},{created_date :{between:currentDayFilter}});
        cancelCount.push({tcs_app_id :filterAppId},{"status":"Cancelled"},{created_date :{between:currentDayFilter}});
        settledCount.push({tcs_app_id :filterAppId},{"status":"Settled"},{created_date :{between:currentDayFilter}});
      }else{
        totalCount.push({created_date :{between:currentDayFilter}});
        initiatedCount.push({"status":"Initiated"},{created_date :{between:currentDayFilter}});
        submittedCount.push({"or":[{"status":"Submitted"},{"status":"Received"}]},{created_date :{between:currentDayFilter}});
        failedCount.push({"status":"Failed"},{created_date :{between:currentDayFilter}});
        cancelCount.push({"status":"Cancelled"},{created_date :{between:currentDayFilter}});
        settledCount.push({"status":"Settled"},{created_date :{between:currentDayFilter}});
      }

      Request.count({"and": totalCount}, function(err, transactions) {
        if (err) {
          cb(err, null);
        } else {
          totalCountArray.push([i,transactions]);
        }

        Request.count({"and": initiatedCount}, function (err, queueCount) {
          if (err) {
            cb(err, null);
          } else {
            initiatedCountArray.push([i,queueCount]);
          }
          Request.count({"and": submittedCount}, function (err, sentPayGovCount) {
            if (err) {
              cb(err, null);
            } else {
              submittedCountArray.push([i,sentPayGovCount]);
            }
            Request.count({"and": settledCount}, function (err, settleCount) {
              if (err) {
                cb(err, null);
              } else {
                settledCountArray.push([i, settleCount]);
              }
              Request.count({"and": cancelCount}, function (err, cancelledCount) {
                if (err) {
                  cb(err, null);
                } else {
                  cancelCountArray.push([i, cancelledCount]);
                }
                Request.count({"and": failedCount}, function (err, responsePayGovCount) {
                  if (err) {
                    cb(err, null);
                  } else {
                    failedCountArray.push([i, responsePayGovCount]);

                    if (i == 30) {
                      totalCountArray.push([31]);
                      initiatedCountArray.push([31]);
                      submittedCountArray.push([31]);
                      cancelCountArray.push([31]);
                      failedCountArray.push([31]);

                      graphStatistics.transactionsData.push(
                        {
                          label: 'Total Transactions',
                          data: totalCountArray
                        }, {
                          label: 'Ready to Process Transactions',
                          data: initiatedCountArray
                        }, {
                          label: 'Send to Pay.gov Transactions',
                          data: submittedCountArray
                        }, {
                          label: 'Settled Transactions',
                          data: settledCountArray
                        }, {
                          label: 'Cancelled Transactions',
                          data: cancelCountArray
                        }, {
                          label: 'Failed Transactions',
                          data: failedCountArray
                        }
                      );

                      graphStatistics.dates = datesArray;
                      cb(null, graphStatistics);
                    } else {
                      i = i + 1;
                      days_30 = days_30 + 86400 * 1000;
                      getData(i);
                    }
                  }
                });
              });
            });
          });
        });
      });
    }
    getData(1);

  };
  /* istanbul ignore next*/
  Request.remoteMethod('getLineGraphCount', {
    description: "To get all the transactions count based on status and application",
    returns: {
      arg: 'TransactionCount',
      type: 'array'
    },
    accepts: [{arg: 'filterAppId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getLineGraphCount',
      verb: 'get'
    }
  });

};
