var server = require('../../server/server');
var fs = require('fs');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);

var util = require('./../../server/boot/lib/hpcs-utils');
var authContent = fs.readFileSync("./settings/auth.json");
var authInfo = JSON.parse(authContent);
serviceInfo = util.extend({}, serviceInfo, authInfo);

var nodeMailerEmail = serviceInfo.nodeMailerEmail;
var nodeMailerPassword = serviceInfo.nodeMailerPassword;
var nodeMailerHost = serviceInfo.nodeMailerHost;
var nodeMailerPort = serviceInfo.nodeMailerPort;

/* istanbul ignore next*/
module.exports = function(HpcsEmail) {

  HpcsEmail.observe('before save', function (ctx, next) {
    console.log('Email Data..:'+JSON.stringify(ctx));
    var nodemailer = require('nodemailer');
    var smtpTransport = require('nodemailer-smtp-transport');
    var transporter = nodemailer.createTransport(smtpTransport({
      host: nodeMailerHost,
      port: nodeMailerPort,
      secure: true,
      auth: {
        user: nodeMailerEmail,
        pass: nodeMailerPassword
      }
    }));
    var mailOptions = {
      from: ctx.instance.from,
      to: ctx.instance.to,
      subject: ctx.instance.subject,
      text: ctx.instance.text
    };

    transporter.sendMail(mailOptions, function (err) {
      if (err) {
        next(err,null);
      }else {
        next();
      }
    });
  });
};
