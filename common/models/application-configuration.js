var server = require('../../server/server');
/* istanbul ignore next*/
module.exports = function(Applicationconfiguration) {
  Applicationconfiguration.validatesUniquenessOf('name', {message: 'Name Already Exists'});
  Applicationconfiguration.validatesUniquenessOf('tcsAppId', {message: 'Name Already Exists'});

  Applicationconfiguration.observe('after save', function (ctx, next) {
    var HpcsUser = server.models.HpcsUser;
    //console.log('App Data..:'+JSON.stringify(ctx));
    //if(ctx.isNewInstance){
    next();
    //}
    // Application ID and Application Name are readonly in paygov 1.1 so there is no need update user, hence commenting the logic
    //else{
    //  //console.log('User Data:'+JSON.stringify(ctx.instance));
    //  HpcsUser.find({where:{applicationId:ctx.instance.id}}, function(err, userData){
    //    if(err){
    //      next(err, null);
    //    }else{
    //      if(userData.length > 0){
    //        //console.log('User Data:'+JSON.stringify(userData));
    //        var updateParams = {
    //          "applicationName": ctx.instance.name,
    //          "tcsAppId": ctx.instance.tcsAppId,
    //          "editCheck":true
    //        };
    //        function updateUser(i) {
    //          var userInstance = userData[i];
    //          userInstance.updateAttributes(updateParams, function(err){
    //            if(err){
    //              next(err, null);
    //            }else{
    //              i = i+ 1;
    //              if(i == userData.length){
    //                next();
    //              }else{
    //                updateUser(i);
    //              }
    //            }
    //          });
    //        }
    //        updateUser(0);
    //      }else{
    //        //console.log('No data');
    //        next();
    //      }
    //    }
    //  });
    //}
  });
};
