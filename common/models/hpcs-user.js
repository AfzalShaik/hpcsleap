var server = require('../../server/server');
var fs = require('fs');
var servicesListContent = fs.readFileSync("./settings/services.json");
var serviceInfo = JSON.parse(servicesListContent);
var nodeMailerEmail = serviceInfo.nodeMailerEmail;
/* istanbul ignore next*/
module.exports = function(HpcsUser) {
  HpcsUser.observe('before save', function (ctx, next) {
    var AppConfiguration = server.models.ApplicationConfiguration;
    var HpcsRole = server.models.HpcsRole;
    if(ctx.instance){
      if (ctx.instance.applicationName.length > 0) {
        var appId = [];
        function createAppId(i) {
          if(ctx.instance.applicationName[0] !== "ALL") {
            AppConfiguration.find({where: {name: ctx.instance.applicationName[i]}}, function (err, appData) {
              if (err) {
                next(err, null);
              } else {
                if (appData.length > 0) {
                  appId.push(appData[0].tcsAppId);
                }
                i = i + 1;
                if (i == ctx.instance.applicationName.length) {
                  //console.log('App Ids:' + JSON.stringify(appId));
                  ctx.instance['applicationId'] = appId;
                  //console.log('App Ids:' + JSON.stringify(appId));
                  //console.log('Before Save with APP Ids..:' + JSON.stringify(ctx.instance));
                  //next();
                  if (ctx.instance.role.length > 0) {
                    var roleId = [];

                    function createRoleId(i) {
                      HpcsRole.find({where: {name: ctx.instance.role[i]}}, function (err, roleData) {
                        if (err) {
                          next(err, null);
                        } else {
                          if (roleData.length > 0) {
                            roleId.push(roleData[0].id);
                          }
                          i = i + 1;
                          if (i == ctx.instance.role.length) {
                            //console.log('Role Ids:'+JSON.stringify(roleId));
                            ctx.instance['roleId'] = roleId;
                            //console.log('Role Ids:'+JSON.stringify(roleId));
                            //console.log('Before Save with Role Ids..:' + JSON.stringify(ctx.instance));
                            next();
                          } else {
                            createRoleId(i);
                          }
                        }
                      });
                    }
                    createRoleId(0);
                  }
                } else {
                  createAppId(i);
                }
              }
            });
          } else {
            appId.push("ALL");
            next();
          }
        }
        createAppId(0);
      }
    }else if(ctx.data) {
      //console.log('Before User Save:'+JSON.stringify(ctx));
      if(ctx.data.editCheck){
        delete ctx.data['editCheck'];
        //console.log('Before :'+JSON.stringify(ctx.data));
        next();
      }else {
        if (ctx.data.applicationName.length > 0) {
          var appId = [];
          function editAppId(i) {
            console.log('Edit Data:'+JSON.stringify(ctx.data.applicationName[0]));
            if(ctx.data.applicationName[0] !== "ALL") {
              AppConfiguration.find({where: {name: ctx.data.applicationName[i]}}, function (err, appData) {
                if (err) {
                  next(err, null);
                } else {
                  if (appData.length > 0) {
                    appId.push(appData[0].tcsAppId);
                  }
                  i = i + 1;
                  if (i == ctx.data.applicationName.length) {
                    //console.log('App Ids:' + JSON.stringify(appId));
                    //ctx.instance['appId'] = appId;

                    ctx.data.applicationId = appId;
                    //console.log('App Ids:' + JSON.stringify(appId));
                    //console.log('Before Save with Ids..:' + JSON.stringify(ctx.data));
                    //next();
                    if (ctx.data.role && ctx.data.role.length > 0) {
                      var roleId = [];
                      //console.log('Roles:' + ctx.data.role);
                      function editRoleId(i) {
                        HpcsRole.find({where: {name: ctx.data.role[i]}}, function (err, roleData) {
                          if (err) {
                            next(err, null);
                          } else {
                            if (roleData.length > 0) {
                              roleId.push(roleData[0].id);
                            }
                            i = i + 1;
                            if (i == ctx.data.role.length) {
                              //console.log('Role Ids:'+JSON.stringify(roleId));
                              ctx.data.roleId = roleId;
                              //console.log('Role Ids:'+JSON.stringify(roleId));
                              //console.log('Before Save with Ids..:' + JSON.stringify(ctx.data));
                              next();
                            } else {
                              editRoleId(i);
                            }
                          }
                        });
                      }
                      editRoleId(0);
                    }
                  } else {
                    editAppId(i);
                  }
                }
              });
            }else{
              appId.push("ALL");
            }
          }
          editAppId(0);
        }

      }
    }
  });

  /* istanbul ignore next*/
  HpcsUser.forgotPassword = function (email, cb) {
    var HpcsEmail = server.models.HpcsEmail;
    HpcsUser.find({where:{"email":email}},function(err, userData){
      if(err){
        cb(err,null);
      }else{
        if(userData.length > 0) {
          HpcsEmail.create({to:nodeMailerEmail, from:email, subject:'Request for Reset Password', text: 'Reset password request from: '+email}, function(err, emailData){
            if(err){

            }else{
              HpcsEmail.create({to:email, from:nodeMailerEmail, subject:'Reset Password', text: 'Your request for Reset Password is received. Admin will contact you shortly.'}, function(err, emailData){

              });
            }
          });
          cb(null, 'Reset Password Request Submitted Successfully');
        }else{
          cb(null,'User Not Found');
        }
      }
    });
  };
  /* istanbul ignore next*/
  HpcsUser.remoteMethod('forgotPassword', {
    description: "To get the details of a transaction",
    returns: {
      arg: 'status',
      type: 'string'
    },
    accepts: [{arg: 'email', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/forgotPassword',
      verb: 'get'
    }
  });
};

