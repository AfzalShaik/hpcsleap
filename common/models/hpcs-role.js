var server = require('../../server/server');
/* istanbul ignore next*/
module.exports = function(HpcsRole) {
  HpcsRole.validatesUniquenessOf('name', {message: 'Name Already Exists'});

  HpcsRole.observe('after save', function (ctx, next) {
    var HpcsUser = server.models.HpcsUser;
    if(ctx.isNewInstance){
      next();
    }else{
      var filterVar = [];
      filterVar.push(ctx.instance.id);
      HpcsUser.find({where:{roleId:ctx.instance.id}}, function(err, userData){
        if(err){
          next(err, null);
        }else{
          if(userData.length > 0){
            function updateUser(i) {
              var userInstance = userData[i];
              var userRoles = userData[i].roleId;
              var roles = [];
              function updateRoles(j){
                HpcsRole.findById(userRoles[j],function(err, roleData){
                  if(err){
                    next(err, null);
                  }else{
                    if(roleData){
                      roles.push(roleData.name);
                    }
                    j = j+1;
                    if(j == userRoles.length){
                      userInstance.updateAttributes({'role': roles,'editCheck':true}, function(err, updatedUserData){
                        if(err){
                          next(err, null);
                        }else{
                          i = i+ 1;
                          if(i == userData.length){
                            next();
                          }else{
                            updateUser(i);
                          }
                        }
                      });
                    }else{
                      updateRoles(j);
                    }
                  }
                });
              }
              updateRoles(0);
            }
            updateUser(0);
          }else{
            next();
          }
        }
      });
    }
  });
};
