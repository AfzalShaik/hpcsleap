var server = require('../server/server.js');

var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);


it('should list ALL data on /mainframe GET', function(done) {
  chai.request(server)
    .get('/mainframe')
    .end(function(err, res){
      res.should.have.status(200);
      res.should.be.html;
      done();
    });
});

it('should list ALL data on /settlementReport GET', function(done) {
  chai.request(server)
    .get('/settlementReport')
    .end(function(err, res){
      res.should.have.status(200);
      res.should.be.html;
      done();
    });
});

it('should post data on /cfm POST', function(done) {
  this.timeout(15000);
  chai.request(server)
    .post('/cfm')
    .set('content-type', 'application/x-www-form-urlencoded')
    .send({'agency_id': '950', 'agency_tracking_id': '898989','app_name':'Single Family Claim Remittance'})
    .end(function(err, res){
     // console.log("Resonse from cfm ",res);
      res.should.have.status(200);
      done();
    });
});

it('should post data on /intr POST', function(done) {
 this.timeout(15000);

  chai.request(server)
    .post('/intr')
    .set('content-type', 'application/x-www-form-urlencoded')
    .send({'agency_id': '1412', 'agency_tracking_id': '898989','app_name':'FHA New Lender Application Fee'})
    .end(function(err, res){
     // console.log("Resonse from intr ",res);
      res.should.have.status(200);
      done();
    });
});

it('should post data on /processPayment POST', function(done) {
 this.timeout(15000);

  chai.request(server)
    .get('/processPayment?userData={"accountHolderFN":"a","accountHolderLN":"b","accountType":"BusinessChecking","routing":"211889898","account":"98098908080","account2":true,"checkNumber":"98098908"&agencyId=1412&agency_tracking_id=9568001&paymentAmount=10000.99&paymentDate=01/10/2005&appName=FHA New Lender Application Fee')
     .auth('mainframe@hpcs.com', 'Password@1234')
      .end(function(err, res){
      done();
    });
});

it('should return message invallid username or password  /settlement POST', function(done) {
  chai.request(server)
    .post('/settlement')
    .set('content-type', 'application/x-www-form-urlencoded')
    .send({'agency_id': '950', 'report_name': 'CollectionsActivityFilev2','date':'10/31/2016','username':'admin@admin.com','password':'Password@1234'})
    .end(function(req, res){
      res.should.have.status(200);
      done();
});
});


