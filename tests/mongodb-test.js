
var fs = require('fs');
var assert = require('assert');
var ca = fs.readFileSync("/paygov/mongodb.pem");
var cert = fs.readFileSync("/paygov/client.pem");
var key = fs.readFileSync("/paygov/mongodb.pem");
var dataSources = require("../server/datasources.local.js");

var dboptions = {
  "username": dataSources.hpcs_db.username,
  "password": dataSources.hpcs_db.password,
  "server": {
    "sslValidate":false,
    "checkServerIdentity":false,
    "sslKey": dataSources.hpcs_db.server.sslKey,
    "sslCert": dataSources.hpcs_db.server.sslKey,
    "sslCA": dataSources.hpcs_db.server.sslCA,
    "sslPass": dataSources.hpcs_db.server.sslPass
  }
};

describe('MongoDB Connectivity Test', function () {

    it('Connect MongoDB', function (done) {
      var mongoose = require('mongoose');
      var hpcs_db = dataSources.hpcs_db;
      var dbURI = 'mongodb://'+dataSources.hpcs_db.host+':'+dataSources.hpcs_db.port+'/'+dataSources.hpcs_db.database+'?ssl=true';
        console.log('Trying to connect MongoDB using url: '+dbURI);
        mongoose.connect(dbURI, dboptions);
        mongoose.connection.on('connected', function () {
          console.log('Successfully connected to MongoDB');
	  done();
        });
        mongoose.connection.on('error',function (err) {
          console.log('Mongoose default connection error: ' + err);
          assert.equal(err, null);
	  done();
        });
    });

});
