var fs = require('fs');
var assert = require('assert');

describe('Encrypt Decrypt Test Suite;', function () {

  before(function(done) {
    done();
  });
  describe('Encrypt and Decrypt TCSSingleService Requests;', function () {

    it('encrypt and Decrypt processACHCancel Request', function (done) {
      var encryptDecrypt = require('./../server/boot/lib/encrypt-util.js');
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHCancel": {
          "orig_paygov_tracking_id": "3FP31S1D",
          "transaction_amount": 6.49,
          "agency_tracking_id": "000022918"
        }
      }
      console.log('Request:');
      console.log(input);
      var encrypt = encryptDecrypt.encrypt(input);
      var decrypt = encryptDecrypt.decrypt(encrypt);
      assert.equal(decrypt, input);
      done();
    });

    it('encrypt and Decrypt processACHDebit Request', function (done) {
      var encryptDecrypt = require('./../server/boot/lib/encrypt-util.js');
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHDebit": {
          "agency_tracking_id": "000000299",
          "account_number": "1234",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name" : "doe",
          "business_name": "AAAE"
        }
      }
      console.log('Request:');
      console.log(input);
      var encrypt = encryptDecrypt.encrypt(input);
      var decrypt = encryptDecrypt.decrypt(encrypt);
      assert.equal(decrypt, input);
      done();
    });

    it('encrypt and Decrypt processACHPrenotification Request', function (done) {
      var encryptDecrypt = require('./../server/boot/lib/encrypt-util.js');
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHPrenotification": {
          "agency_tracking_id": "0000000801",
          "account_number": "5105105105105100",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 0.00,
          "first_name": "John",
          "last_name":"Doe"
        }
      }
      console.log('Request:');
      console.log(input);
      var encrypt = encryptDecrypt.encrypt(input);
      var decrypt = encryptDecrypt.decrypt(encrypt);
      assert.equal(decrypt, input);
      done();
    });
  });

});


