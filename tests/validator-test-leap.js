describe('LEAP Simulator Input Validation Test Suite;', function () {
  var fs = require('fs');

  before(function(done) {
    done();
  });
  describe('LEAP Simulator Input Validation Tests;', function () {

    it('Leap Simulator Tests', function (done) {
      var fs = require('fs');
      var input = JSON.parse(fs.readFileSync('./tests/data/leapSimulator.json'));
      var url = "http://52.222.54.108:80/leapSimulator/resultURL";
      var request = require('request');
      var sslOption = require('./../server/ssl/ssl-cert');
      console.log('Request:');
      console.log(input);
      request({
        url: url,
        method: "POST",
        json: true,
        body: input,
        key: sslOption.key,
        cert: sslOption.cert,
        requestCert:true,
        ca:sslOption.ca,
        rejectUnauthorized:false
      }, function(err, response, body) {
        if(err) { return err; }
        console.log('Response:');
        console.log(body);
        done();
      });
    });
    });
   });
