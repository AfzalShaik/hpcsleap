var fs = require('fs');
var assert = require('assert');
var hpcsIdGenerator = require('./../server/boot/lib/hpcs-id-generator.js');

describe('HPCS Id Generator Test Suite;', function () {

  before(function(done) {
    done();
  });
  describe('TCS Single Service Tests: getAgencyTrackingId', function () {

    it('getAgencyTrackingId from processACHCancel Request', function (done) {

      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHCancel": {
          "orig_paygov_tracking_id": "3FP31S1D",
          "transaction_amount": 6.49,
          "agency_tracking_id": "000022918"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.getAgencyTrackingId(input, 'ACHCancel');
      assert.equal(agencyTrackingId, '000022918');
      done();
    });

    it('getAgencyTrackingId from processACHDebit Request', function (done) {

      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHDebit": {
          "agency_tracking_id": "000000299",
          "account_number": "1234",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name" : "doe",
          "business_name": "AAAE"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.getAgencyTrackingId(input, 'ACHDebit');
      assert.equal(agencyTrackingId, '000000299');
      done();
    });

    it('getAgencyTrackingId from processACHPrenotification Request', function (done) {
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHPrenotification": {
          "agency_tracking_id": "0000000801",
          "account_number": "5105105105105100",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 0.00,
          "first_name": "John",
          "last_name":"Doe"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.getAgencyTrackingId(input, 'ACHPrenotification');
      assert.equal(agencyTrackingId, '0000000801');
      done();
    });

  });

  describe('TCS Single Service Tests: generateHPCSId method', function () {


    it('generateHPCSId from processACHDebit Request', function (done) {
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHDebit": {
          "agency_tracking_id": "000000299",
          "account_number": "1234",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name" : "doe",
          "business_name": "AAAE"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.generateHPCSId(input, 'ACHDebit');
      assert.equal(agencyTrackingId, '000000299');
      done();
    });

    it('generateHPCSId from processACHPrenotification Request', function (done) {
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHPrenotification": {
          "agency_tracking_id": "0000000801",
          "account_number": "5105105105105100",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 0.00,
          "first_name": "John",
          "last_name":"Doe"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.generateHPCSId(input, 'ACHPrenotification');
      assert.equal(agencyTrackingId, '0000000801');
      done();
    });

  });

  describe('TCS Single Service Tests: updateHPCSId method', function () {

    it('updateHPCSId from processACHDebit Request', function (done) {
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHDebit": {
          "agency_tracking_id": "000000299",
          "account_number": "1234",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name" : "doe",
          "business_name": "AAAE"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.updateHPCSId(input, 'ACHDebit');
      assert.equal(agencyTrackingId, '000000299');
      done();
    });

    it('updateHPCSId from processACHPrenotification Request', function (done) {
      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHPrenotification": {
          "agency_tracking_id": "0000000801",
          "account_number": "5105105105105100",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 0.00,
          "first_name": "John",
          "last_name":"Doe"
        }
      }

      var agencyTrackingId = hpcsIdGenerator.updateHPCSId(input, 'ACHPrenotification');
      assert.equal(agencyTrackingId, '0000000801');
      done();
    });

  });
});


