var fs = require('fs');
var assert = require('assert');
var expect = require('chai').expect
var paygovClient = require('./../server/boot/paygov-client.js');
var date = new Date();
describe('Pay Gov Client Test Suite;', function () {

  before(function(done) {
    done();
  });
  describe('Connect to Pay gov Tests;', function () {

    it('callPayGov from ACHDebit Request', function (done) {

      var input =  {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHDebit": {
          "agency_tracking_id": "SFCR00000F299",
          "account_number": "1234",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name" : "doe",
          "business_name": "AAAE"
        }
      };
      console.log('Request:');
      console.log(input);
      paygovClient.callPayGov("TCSSingleService/processACHDebit", input, function(err, response){
        if(err){
          console.log("Error occurred while calling PayGov.");
          assert.equal(err, null);
          done();
        } else {
          console.log(response);
          assert.equal(err, null);
          assert.equal(response.tcs_app_id, 'TCSHUDSFCR');
          assert.equal(response.ACHDebit.transaction_status , 'Failed');
          assert.equal(response.ACHDebit.return_detail, 'The value supplied for the agency_tracking_id is not unique.');
          done();
        }
      })
    });

    it('callPayGov from SingleQuery Request', function (done) {

      var input =  {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "agency_tracking_id": "SFCR00000F299",
        "paygov_tracking_id": "3FP3PGVG"
      };
      console.log('Request:');
      console.log(input);
      paygovClient.callPayGov("TCSSingleQueryService/processTCSSingleQuery", input, function(err, response){
        if(err){
          console.log("Error occurred while calling PayGov.");
          assert.equal(err, null);
          done();
        } else {
          console.log("%j", response);
          assert.equal(err, null);
          assert.equal(response.tcs_app_id, 'TCSHUDSFCR');
          assert.equal(response.tcs_ach_results.ach_cancel_query_response[0].transaction_status, 'Canceled');
          done();
        }
      });
    });


    it('callPayGov from ACHCancel Request', function (done) {

      var input =  {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHCancel": {
          "orig_paygov_tracking_id": "3FP3PGVG",
          "transaction_amount": 6.89,
          "agency_tracking_id": "SFCR00000F299"
        }
      };
      console.log('Request:');
      console.log(input);
      paygovClient.callPayGov("TCSSingleService/processACHCancel", input, function(err, response){
        if(err){
          console.log("Error occurred while calling PayGov.");
          assert.equal(err, null);
          done();
        } else {
          console.log(response);
          assert.equal(err, null);
          assert.equal(response.tcs_app_id, 'TCSHUDSFCR');
          assert.equal(response.ACHCancel.transaction_amount, 6.89);
          done();
        }
      })
    });
  });
});


