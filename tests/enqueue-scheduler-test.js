var seq = 2001;
var appServer = require('../server/server');
var scheduling = require('../server/boot/schedule-services');
var Request = appServer.models.Request;
var assert = require('assert');

describe('Schedule Services Test Suite;', function () {
  this.timeout(30000);
  before(function(done) {
    cleanUpDBRequests(createSampleData);
    setTimeout(function() {
      done();
    }, 5000);
  });

  after(function(done){
    console.log("Cleaning up test data....");
    cleanUpDBRequests(function(){
      console.log("Cleanup successfully");
    });
    setTimeout(function() {
      done();
    }, 3000);
  });

  describe('Enqueue Scheduler Tests;', function () {

    it('Enqueue Scheduler: updateTransactionResponse Test', function (done) {
      var hpcs_id = 'SFC2001';
      var sampleResponse = {
        "agency_id":"950",
        "tcs_app_id":"TCSHUDSFCR",
        "tcs_ach_results":{
          "ach_debit_query_response":[
            {
              "agency_tracking_id":"SFC2001",
              "paygov_tracking_id":"3FP40PD0",
              "transaction_status":"Received",
              "transaction_amount":"590.99",
              "masked_account_number":"************2248",
              "return_code":"1001",
              "return_detail":"Successful submission of ACH Debit",
              "account_holder_name":"John Smith",
              "account_holder_email_address":null,
              "transaction_date":"2016-10-06T09:39:58",
              "account_type":"BusinessChecking",
              "routing_transit_number":"041201936",
              "sec_code":"CCD",
              "return_reason_code":null,
              "deposit_ticket_number":null,
              "debit_voucher_number":null,
              "installment_number":"1",
              "total_installments":"1",
              "payment_frequency":"OneTime",
              "app_batch_id":null
            }
          ]
        }
      };
      scheduling.updateTransactionResponse(
        'SFC2001',
        sampleResponse,
        sampleResponse.tcs_ach_results.ach_debit_query_response[0].transaction_status,
        sampleResponse.tcs_ach_results.ach_debit_query_response[0].return_detail);

      setTimeout(function(){
        Request.find({where: {hpcs_id: hpcs_id}}, function(err, result) {
          console.log(result);
          assert.equal(result[0].status, sampleResponse.tcs_ach_results.ach_debit_query_response[0].transaction_status);
          assert.equal(result[0].return_detail, sampleResponse.tcs_ach_results.ach_debit_query_response[0].return_detail);
          done();
        });
      }, 3000);
    });

    it('Enqueue Scheduler: enqueueInitiatedRequests Test', function (done) {

      try {
        scheduling.enqueueInitiatedRequests();
        setTimeout(function(){
          done();
        }, 10000);

      } catch (err) {
        assert.equal(err, null);
        done();
      }

    });
  });
});


function createSampleData() {
  populateSampleData("SFC" + seq, "TCSHUDSFCR", seq, "Initiated", false, new Date()); //2001
  seq += 1;
  populateSampleData("SFC" + seq, "TCSHUDSFCR", seq, "Initiated", false, new Date()); //2002
  seq += 1;
  populateSampleData("SFC" + seq, "TCSHUDSFCR", seq, "Initiated", false, new Date()); //2003
  seq += 1;
  populateSampleData("SFC" + seq, "TCSHUDSFCR", seq, "Initiated", false, new Date()); //2004
  seq += 1;
  populateSampleData("SFC" + seq, "TCSHUDSFCR", seq, "Initiated", false, new Date()); //2005
  seq += 1;
  populateSampleData("SFC" + seq, "TCSHUDSFCR", seq, "Initiated", false, new Date()); //2006
  seq += 1;
}

function cleanUpDBRequests(cb){
  var hpcs_list = [];
  for(var i = 0 ; i <= 6 ; i++){
    hpcs_list[i] = {
      hpcs_id : "SFC"+ (2001+i)
    }
  }
  Request.deleteAll({or : hpcs_list},function(err, result){
    if(err){
      console.log(err);
    } else {
      console.log(result);
      cb();
    }
  });
}

function populateSampleData(hpcsId, tcs_app_id, trackingId, status, sent, date){
  var sample = {
    "hpcs_id": hpcsId,
    "status": status,
    "operationName": "ACHDebit",
    "agency_tracking_id": trackingId,
    "sent": sent,
    "tcs_app_id": tcs_app_id,
    "paygov_tracking_id": "3FP33O8R",
    "created_date": date,
    "return_detail": "This routing number was invalid.",
    "request" : JSON.stringify(requestData),
    "response" : JSON.stringify(sampleResponse)
  };
  Request.create(sample , function(err, result){
    if(err){
      console.log(err);
    } else {
      console.log('HPCS ID: '+result.hpcs_id + " successfully created.");
    }
  });
}

var sampleResponse = {
  "agency_id": "950",
  "tcs_app_id": "TCSHUDSFCR",
  "tcs_ach_results": {
    "ach_debit_query_response": [ {
      "agency_tracking_id": "SFC2002",
      "paygov_tracking_id": "3FP33O8R",
      "transaction_status": "Received",
      "transaction_amount": "6.89",
      "masked_account_number": "************1234",
      "return_code": "1001",
      "return_detail": "Successful submission of ACH Debit",
      "account_holder_name": "AAAE",
      "account_holder_email_address": null,
      "transaction_date": "2016-05-18T15:17:03",
      "account_type":"BusinessChecking",
      "routing_transit_number": "044000037",
      "sec_code": "CCD",
      "return_reason_code": null,
      "deposit_ticket_number": null,
      "debit_voucher_number": null,
      "installment_number": "0",
      "total_installments": "1",
      "payment_frequency": "OneTime",
      "app_batch_id": null } ]
  }
};

var requestData = {
  "agency_id": "950",
  "tcs_app_id": "TCSHUDSFCR",
  "ACHDebit": {
    "agency_tracking_id": "SFC2002",
    "account_number": "1234",
    "routing_transit_number": "044000037",
    "account_type": "BusinessChecking",
    "transaction_amount": 6.89,
    "first_name": "John",
    "last_name": "doe",
    "business_name": "AAAE"
  }
};
