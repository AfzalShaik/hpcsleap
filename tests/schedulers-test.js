var seq = 1001;
var port = 3030;
var express = require('express');
var app = express();
var appServer = require('../server/server');
var scheduling = require('../server/boot/schedule-services');
var AppConfig = appServer.models.ApplicationConfiguration;
var Request = appServer.models.Request;
var assert = require('assert');
var testData1 = "";
var testData2 = "";

function createSampleData(){
  populateSampleData("SFC"+seq, "TCSHUDSFCR1", seq, "Canceled", false, new Date()); //1001
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR1",seq, "Retired", false, new Date()); //1002
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR1",seq, "Failed", false, new Date()); //1003
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR1",seq, "Canceled", false, new Date()); //1004
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "Settled", false, new Date()); //1005
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "Settled", false, new Date()); //1006
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "Canceled", false, new Date()); //1007
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "TestStatus", false, new Date()); //1008
  seq += 1;
  populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "Settled", false, new Date()); //1009
  seq += 1;
}

describe('Schedule Services Test Suite;', function () {
  this.timeout(30000);

  before(function(done) {
    deleteAppConfigData();
    cleanUpDBRequests(createSampleData);
    populateAppConfigData();
    startSampleServer();
    setTimeout(function() {
      done();
    }, 8000);
  });

  after(function(done){
    cleanUpDBRequests(deleteAppConfigData);
    setTimeout(function() {
      done();
    }, 2000);
  });

  describe('Schedule Services Tests;', function () {

    it('Schedule Services: getTransactionAmount Test', function (done) {
      var achDebit = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR1",
        "ACHDebit": {
          "agency_tracking_id": "044000037",
          "account_number": "1234",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name": "doe",
          "business_name": "AAAE"
        }
      };

      var amt = scheduling.getTransactionAmount(achDebit);
      assert.equal(achDebit.ACHDebit.transaction_amount, amt);

      var achPrenotification = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHPrenotification": {
          "agency_tracking_id": "000000A801",
          "account_number": "5105105105105100",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 4.55,
          "first_name": "John",
          "last_name":"Doe"
        }
      };
      amt = scheduling.getTransactionAmount(achPrenotification);
      assert.equal(achPrenotification.ACHPrenotification.transaction_amount, amt);

      done();
    });

    it('Schedule Services: fetch and update TransactionsByStatus Test', function (done) {
      scheduling.fetchTransactionsByStatus({"status" : "TestStatus"}, function(err, result){
        assert.equal(result[0].status, "TestStatus");
        assert.equal(result[0].hpcs_id, "SFC1008");
        assert.equal(err, null);
        scheduling.updateTransactionStatus("SFC1008", "Settled");
        setTimeout(function(){
          Request.find({where: {hpcs_id: "SFC1008"}}, function(err1, result1) {
            assert(result1[0].status, "Settled");
            done();
          });
        }, 2000);
      });
    });

    it('Schedule Services: updateTransactionSent Test', function (done) {
      scheduling.updateTransactionSent("SFC1008", true);
      setTimeout(function(){
        Request.find({where: {hpcs_id: "SFC1008"}}, function(err1, result1) {
          assert.equal(result1[0].sent, true);
          scheduling.updateTransactionSent("SFC1008", false);
          setTimeout(function(){
            Request.find({where: {hpcs_id: "SFC1008"}}, function(err2, result2) {
              assert.equal(result2[0].sent, false);
              done();
            });
          }, 2000);
        });
      }, 2000);
    });



    it('Schedule Services: fetchResultCfmUrlForTCSAppId Test', function (done) {
      scheduling.fetchResultCfmUrlForTCSAppId("TCSHUDSFCR2", function(err, result){

        assert.equal(err, null);
        assert.equal(result, "http://localhost:"+port+"/callbackUrl2");

        done();
      });


    });

    it('Schedule Services: queryLatestStatus Test', function (done) {
      var requestData =  {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "agency_tracking_id": "000000Z801",
        "paygov_tracking_id": "SFCR000000Z801"
      };
      scheduling.queryLatestStatus(requestData, function(err, response){
        assert.equal(err, null);
        assert.notEqual(response, null);
        done();
      })


    });

    it('Schedule Services: statusScheduler Test', function (done) {
      try {
        scheduling.statusScheduler();
        done();
      } catch(err){
        assert.equal(err, null);
        done();
      }


    });
    
    it('Schedule Services: Pull Settlement Reports Scheduler Test', function (done) {
      try {
        scheduling.pullSettlementReports();
        done();
      } catch(err){
        assert.equal(err, null);
        done();
      }
    });

    it('Schedule Services: Push Settlement Reports To FTP Server Scheduler Test', function (done) {
      try {
        scheduling.pullSettlementFlags();
        done();
      } catch(err){
        assert.equal(err, null);
        done();
      }
    });

    it('Schedule Services: CallbackScheduler Test', function (done) {
      scheduling.callbackScheduler();
      setTimeout(function() {
        Request.find({where: {hpcs_id: 'SFC1001'}}, function(err, result){
	  console.log(testData1);
          if(result){
            assert.equal(result[0].sent, false);
            //assert.equal(testData1, 'response_message=OK');
          }
          else {
            assert.equal(err, null);
            done();
          }
        });

        Request.find({where: {hpcs_id: 'SFC1005'}}, function(err, result){
          if(result){
            assert.equal(result[0].sent, false);
            //assert.equal(testData2, 'response_message=FAILED');
            done();
          }
          else {
            assert.equal(err, null);
            done();
          }
        });

      }, 5000);
    });

    it('Schedule Services: purgeRequests Test', function (done) {
      var d = new Date(); // Today!
      d.setDate(d.getDate() - 200); // 200 days back!
      populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "Settled", false, d ); //1010
      seq += 1;
      setTimeout(function(){
        scheduling.purgeRequests();
        setTimeout(function() {
          Request.find({where: {hpcs_id: 'SFC1010'}}, function (err, result) {
            if (result) {
              assert.equal(result.length, 0);
              done();
            }
            else {
              assert.equal(err, null);
              done();
            }
          });
        }, 1000);

      }, 1000);
    });
 it('Schedule Services: Purge Settlement Reports Test', function (done) {
      try {
        scheduling.purgeSettlementRep();
        done();
      } catch(err){
        assert.equal(err, null);
        done();
      }
            
    });

    it('Schedule Services: fetchTransactionsForStatusScheduler Test', function (done) {
      var today = new Date()
      var priorDate = new Date().setDate(today.getDate() - 44);
      populateSampleData("SFC"+seq, "TCSHUDSFCR2",seq, "Initiated", false, priorDate); //1012
      seq += 1;
      setTimeout(function() {
        scheduling.fetchTransactionsForStatusScheduler(function(err, results){
          if(err){
            assert.equal(err, null);
            done();
          } else {
            for(var i = 0; i < results.length ; i++){
              console.log(results[i].hpcs_id+"<<<<<<<<<<<<>>>>>>>>>"+results[i].created_date);
            }
            done();
          }
        })
      }, 3000);
    });

  });

});

var sampleResponse = {
  "agency_id": "950",
  "tcs_app_id": "TCSHUDCOMMONSERVICES",
  "tcs_ach_results": {
    "ach_debit_query_response": [ {
      "agency_tracking_id": "990000999",
      "paygov_tracking_id": "3FP33O8R",
      "transaction_status": "Received",
      "transaction_amount": "6.89",
      "masked_account_number": "************1234",
      "return_code": "1001",
      "return_detail": "Successful submission of ACH Debit",
      "account_holder_name": "AAAE",
      "account_holder_email_address": null,
      "transaction_date": "2016-05-18T15:17:03",
      "account_type":"BusinessChecking",
      "routing_transit_number": "044000037",
      "sec_code": "CCD",
      "return_reason_code": null,
      "deposit_ticket_number": null,
      "debit_voucher_number": null,
      "installment_number": "0",
      "total_installments": "1",
      "payment_frequency": "OneTime",
      "app_batch_id": null } ]
  }
};

var requestData = {
  "agency_id": "950",
  "tcs_app_id": "TCSHUDSFCR1",
  "ACHDebit": {
    "agency_tracking_id": "044000037",
    "account_number": "1234",
    "routing_transit_number": "044000037",
    "account_type": "BusinessChecking",
    "transaction_amount": 6.89,
    "first_name": "John",
    "last_name": "doe",
    "business_name": "AAAE"
  }
}




function startSampleServer(){

  app.post('/callbackUrl1',function(req,res){
    var rawBody = '';
    req.setEncoding('utf8');

    req.on('data', function(chunk) {
      rawBody += chunk;
    });

    req.on('end', function() {
      console.log(rawBody);
    });
    res.write("response_message=OK");
    testData1 = "response_message=OK";
    res.end();

  });
  app.post('/callbackUrl2',function(req,res){
    var rawBody = '';
    req.setEncoding('utf8');

    req.on('data', function(chunk) {
      rawBody += chunk;
    });

    req.on('end', function() {
      console.log(rawBody);
    });
    testData2 = "response_message=FAILED";
    res.write("response_message=FAILED");
    //res.write("response_message=FAILED - error parsing input XML.");
    res.end();

  });
  try {
    app.listen(port, function () {
      console.log('Example app2 listening on port: '+port);
      //cb();
    });
  } catch(err){
    console.log('Sample Server is already running...');
  }

}

function cleanUpDBRequests(cb){
  var hpcs_list = [];
  for(var i = 0 ; i <= 12 ; i++){
    hpcs_list[i] = {
      hpcs_id : "SFC"+ (1001+i)
    }
  }
  Request.deleteAll({or : hpcs_list},function(err, result){
    if(err){
      console.log(err);
    } else {
      console.log(result);
      cb();
    }
  });
}

function populateSampleData(hpcsId, tcs_app_id, trackingId, status, sent, date){
  var sample = {
    "hpcs_id": hpcsId,
    "status": status,
    "operationName": "ACHDebit",
    "agency_tracking_id": trackingId,
    "sent": sent,
    "tcs_app_id": tcs_app_id,
    "paygov_tracking_id": "3FP33O8R",
    "created_date": date,
    "return_detail": "This routing number was invalid.",
    "request" : JSON.stringify(requestData),
    "response" : JSON.stringify(sampleResponse)
  };
  Request.create(sample , function(err, result){
    if(err){
      console.log(err);
    } else {
      console.log('HPCS ID: '+result.hpcs_id + " successfully created.");
    }
  })
}

function populateAppConfigData(){
  var sample = {
    "tcsAppId": "TCSHUDSFCR1",
    "name": "TCSHUDSFCR1",
    "retpmtCfmURL": "string",
    "resultCfmURLs": "http://localhost:"+port+"/callbackUrl1",
    "settlementURL": "string",
    "id": "1",
    "whitelistedIPs": "string"
  };
  AppConfig.create(sample , function(err, result){
    if(err){
      console.log(err);
    } else {
      console.log('TCS App ID: '+result.tcsAppId + " successfully created.");
    }

    sample = {
      "tcsAppId": "TCSHUDSFCR2",
      "name": "TCSHUDSFCR2",
      "retpmtCfmURL": "string",
      "resultCfmURLs": "http://localhost:"+port+"/callbackUrl2",
      "settlementURL": "string",
      "id": "2",
      "whitelistedIPs": "string"
    };
    AppConfig.create(sample , function(err, result){
      if(err){
        console.log(err);
      } else {
        console.log('TCS App ID: '+result.tcsAppId + " successfully created.");
      }
    })
  });


}

function deleteAppConfigData(){
  AppConfig.deleteAll({ or : [{"tcsAppId": "TCSHUDSFCR1"},{"tcsAppId": "TCSHUDSFCR2"}, {"tcsAppId": "TCSHUDSFCR3"}]  },function(err, result){
    if(err){
      console.log(err)
      throw err;
    } else {
      console.log(result);
    }
  });
}
