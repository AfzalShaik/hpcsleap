var app = require('../server/server');
describe('HPCS Test Suite;', function () {
      var server;
      before(function (done) {
        //server = app.start();
        done();
      });

      after(function () {
        // server.close();
      })

      describe('TCS Single Service Tests;', function () {
        it('TCS Single Service: processACHCancelTest', function (done) {
          var fs = require('fs');
          var loginInput = JSON.parse(fs.readFileSync('./tests/data/applicationAdmin.json'));
          var loginUrl = "https://localhost:2345/api/HpcsUsers/login";
          var input = JSON.parse(fs.readFileSync('./tests/data/processACHCancel.json'));
          var request = require('request');
          var achRequest = require('request');
          var sslOption = require('./../server/ssl/ssl-cert');
          console.log('Request:');
          console.log(input);
          request({
            url: loginUrl,
            method: "POST",
            json: true,
            body: loginInput,
            key: sslOption.key,
            cert: sslOption.cert,
            requestCert: true,
            ca: sslOption.ca,
            rejectUnauthorized: false
          }, function (err, response, body) {
            if (err) {
              return err;
            }
            var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHCancel?access_token=" + body.id;
            console.log('url', url);
            achRequest({
              url: url,
              method: "POST",
              json: true,
              body: input,
              key: sslOption.key,
              cert: sslOption.cert,
              requestCert: true,
              ca: sslOption.ca,
              rejectUnauthorized: false
            }, function (err, response, body) {
              console.log('processACHCancelTest ', body);
              done();
            });
          });
        });

        it('TCS Single Service: processACHDebitTest', function (done) {
          var fs = require('fs');
          var input = JSON.parse(fs.readFileSync('./tests/data/processACHDebit.json'));
          var loginInput = JSON.parse(fs.readFileSync('./tests/data/applicationAdmin.json'));
          var loginUrl = "https://localhost:2345/api/HpcsUsers/login";
          var request = require('request');
          var achDebitRequest = require('request');
          var sslOption = require('./../server/ssl/ssl-cert');
          console.log('Request:');
          console.log(input);
          request({
            url: loginUrl,
            method: "POST",
            json: true,
            body: loginInput,
            key: sslOption.key,
            cert: sslOption.cert,
            requestCert: true,
            ca: sslOption.ca,
            rejectUnauthorized: false
          }, function (err, response, body) {
            if (err) {
              return err;
            }
            var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHDebit?access_token=" + body.id;
            console.log('url', url);
            achDebitRequest({
              url: url,
              method: "POST",
              json: true,
              body: input,
              key: sslOption.key,
              cert: sslOption.cert,
              requestCert: true,
              ca: sslOption.ca,
              rejectUnauthorized: false
            }, function (err, response, body) {
              console.log('Response:');
              console.log(body);
              done();
            });
          });
        });

        it('TCS Single Service: processACHPrenotification', function (done) {
          var fs = require('fs');
          var input = JSON.parse(fs.readFileSync('./tests/data/processACHPrenotification.json'));
          // var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHPrenotification";
          var loginInput = JSON.parse(fs.readFileSync('./tests/data/applicationAdmin.json'));
          var loginUrl = "https://localhost:2345/api/HpcsUsers/login";
          var request = require('request');
          var achDebitRequest = require('request');
          var sslOption = require('./../server/ssl/ssl-cert');
          console.log('Request:');
          console.log(input);
          request({
            url: loginUrl,
            method: "POST",
            json: true,
            body: loginInput,
            key: sslOption.key,
            cert: sslOption.cert,
            requestCert: true,
            ca: sslOption.ca,
            rejectUnauthorized: false
          }, function (err, response, body) {
            if (err) {
              return err;
            }
            var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHPrenotification?access_token=" + body.id;
            console.log('url', url);
            achDebitRequest({
              url: url,
              method: "POST",
              json: true,
              body: input,
              key: sslOption.key,
              cert: sslOption.cert,
              requestCert: true,
              ca: sslOption.ca,
              rejectUnauthorized: false
            }, function (err, response, body) {
              console.log('Response:');
              console.log(body);
              done();
            });
          });
        });

      });

      describe('TCS Single Query Service Tests;', function () {

        it('TCS Single Query Service: processTCSSingleQuery', function (done) {
          var fs = require('fs');
          var input = JSON.parse(fs.readFileSync('./tests/data/processTCSSingleQuery.json'));
          // var url = "https://localhost:2345/api/TCSSingleQueryServiceWrappers/processTCSSingleQuery";
          var loginInput = JSON.parse(fs.readFileSync('./tests/data/applicationAdmin.json'));
          var loginUrl = "https://localhost:2345/api/HpcsUsers/login";
          var request = require('request');
          var achProcessRequest = require('request');
          var sslOption = require('./../server/ssl/ssl-cert');
          console.log('Request:');
          console.log(input);
          request({
            url: loginUrl,
            method: "POST",
            json: true,
            body: loginInput,
            key: sslOption.key,
            cert: sslOption.cert,
            requestCert: true,
            ca: sslOption.ca,
            rejectUnauthorized: false
          }, function (err, response, body) {
            if (err) {
              return err;
            }
            var url = "https://localhost:2345/api/TCSSingleQueryServiceWrappers/processTCSSingleQuery?access_token=" + body.id;
            console.log('url', url);
            achProcessRequest({
              url: url,
              method: "POST",
              json: true,
              body: input,
              key: sslOption.key,
              cert: sslOption.cert,
              requestCert: true,
              ca: sslOption.ca,
              rejectUnauthorized: false
            }, function (err, response, body) {
              console.log('Response:');
              console.log(body);
              done();
            });
          });

        });

      });
    });