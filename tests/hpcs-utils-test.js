var fs = require('fs');
var assert = require('assert');

describe('HPCS Utils Test Suite;', function () {

  before(function (done) {
    done();
  });
    describe('HPCS Utils TCSSingleQueryONAPService Response;', function () {

    it('extractSingleQueryONAP Response - ACHDebit', function (done) {
      var util = require('./../server/boot/lib/hpcs-utils.js');
      var response = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "tcs_ach_results": {
          "ach_debit_query_response": [{
            "agency_tracking_id": "SFCR1JKJLAA99",
            "paygov_tracking_id": "3FP40QBR",
            "transaction_status": "Received",
            "transaction_amount": "6.89",
            "masked_account_number": "************1234",
            "return_code": "1001",
            "return_detail": "Successful submission of ACH Debit",
            "account_holder_name": "AAAE",
            "account_holder_email_address": null,
            "transaction_date": "2016-10-06T11:19:50",
            "account_type": "BusinessChecking",
            "routing_transit_number": "044000037",
            "sec_code": "CCD",
            "return_reason_code": null,
            "deposit_ticket_number": null,
            "debit_voucher_number": null,
            "installment_number": "1",
            "total_installments": "1",
            "payment_frequency": "OneTime",
            "app_batch_id": null
          }]
        }
      }
      console.log('Response:');
      console.log(response);

      var result = util.extractSingleQueryONAPResponse(response);
      var ach_results = response.tcs_ach_results;
      result.hpcs_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
      result.status = ach_results.ach_debit_query_response[0].transaction_status;
      result.return_detail = ach_results.ach_debit_query_response[0].return_detail;
      assert.equal('SFCR1JKJLAA99', result.hpcs_id);
      assert.equal('Received', result.status);
      assert.equal('Successful submission of ACH Debit', result.return_detail);
      done();
    });
  });
  describe('HPCS Utils TCSSingleQueryService Response;', function () {

    it('extractSingleQuery Response - ACHDebit', function (done) {
      var util = require('./../server/boot/lib/hpcs-utils.js');
      var response = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "tcs_ach_results": {
          "ach_debit_query_response": [{
            "agency_tracking_id": "SFCR1JKJLAA99",
            "paygov_tracking_id": "3FP40QBR",
            "transaction_status": "Received",
            "transaction_amount": "6.89",
            "masked_account_number": "************1234",
            "return_code": "1001",
            "return_detail": "Successful submission of ACH Debit",
            "account_holder_name": "AAAE",
            "account_holder_email_address": null,
            "transaction_date": "2016-10-06T11:19:50",
            "account_type": "BusinessChecking",
            "routing_transit_number": "044000037",
            "sec_code": "CCD",
            "return_reason_code": null,
            "deposit_ticket_number": null,
            "debit_voucher_number": null,
            "installment_number": "1",
            "total_installments": "1",
            "payment_frequency": "OneTime",
            "app_batch_id": null
          }]
        }
      }
      console.log('Response:');
      console.log(response);

      var result = util.extractSingleQueryResponse(response);
      var ach_results = response.tcs_ach_results;
      result.hpcs_id = ach_results.ach_debit_query_response[0].agency_tracking_id;
      result.status = ach_results.ach_debit_query_response[0].transaction_status;
      result.return_detail = ach_results.ach_debit_query_response[0].return_detail;
      assert.equal('SFCR1JKJLAA99', result.hpcs_id);
      assert.equal('Received', result.status);
      assert.equal('Successful submission of ACH Debit', result.return_detail);
      done();
    });
  });


  it('extractSingleQuery Response - ACHPrenotification', function (done) {
    var util = require('./../server/boot/lib/hpcs-utils.js');
    var response = {
      "agency_id": "950",
      "tcs_app_id": "TCSHUDSFCR",
      "tcs_ach_results": {
        "ach_prenotification_query_response": [{"agency_tracking_id":"SFCRshil11234567890","paygov_tracking_id":"3FP3PFCK","transaction_status":"Settled","transaction_amount":"0.00","masked_account_number":"************ 890","return_code":"3004","return_detail":"ACH transaction has settled","account_holder_name":"John Doe","account_holder_email_address":null,"transaction_date":"2016-09-09T08:57:44","account_type":"BusinessChecking","effective_date":"2016-09-12","routing_transit_number":"044000037","sec_code":"CCD","return_reason_code":null,"deposit_ticket_number":null,"debit_voucher_number":null,"app_batch_id":null}]
      }

    }
    console.log('Response:');
    console.log(response);

    var result = util.extractSingleQueryResponse(response);
    var ach_results = response.tcs_ach_results;
    result.hpcs_id = ach_results.ach_prenotification_query_response[0].agency_tracking_id;
    result.status = ach_results.ach_prenotification_query_response[0].transaction_status;
    result.return_detail = ach_results.ach_prenotification_query_response[0].return_detail;
    assert.equal('SFCRshil11234567890', result.hpcs_id);
    assert.equal('Settled', result.status);
    assert.equal('ACH transaction has settled', result.return_detail);
    done();
  });


  it('extractSingleQuery Response - ACHCancel', function (done) {
    var util = require('./../server/boot/lib/hpcs-utils.js');
    var response = {
      "agency_id": "950",
      "tcs_app_id": "TCSHUDSFCR",
      "tcs_ach_results": {
        "ach_cancel_query_response": [{
          "agency_tracking_id":"SFCR00000E299","paygov_tracking_id":"3FP3MJPD","transaction_status":"Canceled","transaction_amount":"6.89","masked_account_number":"************1234","return_code":"1003","return_detail":"Successful submission of ACH CancelTransaction","account_holder_name":"AAAE","account_holder_email_address":null,"transaction_date":"2016-08-25T16:24:37","account_type":"BusinessChecking","routing_transit_number":"044000037","sec_code":"CCD","return_reason_code":null,"deposit_ticket_number":null,"debit_voucher_number":null,"app_batch_id":null
        }]
      }
    }

    console.log('Response:');
    console.log(response);

    var result = util.extractSingleQueryResponse(response);
    var ach_results = response.tcs_ach_results;
    result.hpcs_id = ach_results.ach_cancel_query_response[0].agency_tracking_id;
    result.status = ach_results.ach_cancel_query_response[0].transaction_status;
    result.return_detail = ach_results.ach_cancel_query_response[0].return_detail;
    assert.equal('SFCR00000E299', result.hpcs_id);
    assert.equal('Canceled', result.status);
    assert.equal('Successful submission of ACH CancelTransaction', result.return_detail);
    done();
  });

  it('extractSingleQuery Response - No Data Found ', function (done) {
    var util = require('./../server/boot/lib/hpcs-utils.js');
    var response = {
      "agency_id": "950",
      "tcs_app_id": "TCSHUDSFCR",
      "agency_tracking_id":"1DL1804246",
      "paygov_tracking_id":"3FP3T7RJ",
      "return_code":"4061",
      "return_detail":"No Data Found for Search Criteria Entered"
      }
    console.log('Response:');
    console.log(response);

    var result = util.extractSingleQueryResponse(response);
    result.hpcs_id = response.agency_tracking_id;
    result.status = 'Failed';
    result.return_detail = response.return_detail;
    assert.equal('1DL1804246', result.hpcs_id);
    assert.equal('Failed', result.status);
    assert.equal('No Data Found for Search Criteria Entered', result.return_detail);
    done();
  });
});


describe('HPCS Utils extractACHCancelResponse ;', function () {

  it('extractACHCancelResponse  - ACHCancel', function (done) {
    var util = require('./../server/boot/lib/hpcs-utils.js');
    var response = {
     "agency_id": "950",
      "tcs_app_id": "TCSHUDSFCR",
      "ACHCancel":
      { "agency_tracking_id": "SFCR0044560299",
        "transaction_amount": "6.89",
        "masked_account_number": null,
        "return_code": "4022",
        "return_detail": "The selected ACH Debit authorization cannot be cancelled",
        "transaction_status": "Failed",
        "orig_paygov_tracking_id": "3FP3N3V5" } }

    console.log('Response:');
    console.log(response);

    var result = util.extractACHCancelResponse(response);
    result.hpcs_id = response.ACHCancel.agency_tracking_id;
    result.status = response.ACHCancel.transaction_status;
    result.return_detail = response.ACHCancel.return_detail;
    assert.equal('SFCR0044560299', result.hpcs_id);
    assert.equal('Failed', result.status);
    assert.equal('The selected ACH Debit authorization cannot be cancelled', result.return_detail);
    done();
  });
});



