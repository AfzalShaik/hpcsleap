var appServer = require('../server/server');
var AgencyMapper = require('./../server/boot/agency-mapper.js');
var assert = require('assert');
describe('Agency Mapper Test Suite;', function () {

  before(function(done) {
    var AppConfig = appServer.models.ApplicationConfiguration;
    var input1 = {
      "tcsAppId": "TestTcsAppId",
      "name": "TestAppConfig",
      "retpmtCfmURL": "http://www.hpcs.com",
      "resultCfmURLs": "http://www.hpcs.com",
      "settlementURL": "http://www.hpcs.com",
      "whitelistedIPs": [
        "10.1.1.1"
      ],
      "agencyId": "950",
      "applicationCode": "TestAppCode",
      "machineId": "TestMachine",
      "password": "Admin@1234"
    };
    AppConfig.create(input1, function(err, result){
      if(err){
        console.log(err);
      }
      if(result){
        console.log("=========>>>>>>>>>>>>>"+JSON.stringify(result));
        var input2 = {
          "paygovAppName": "Test Single Family Claim Remittance",
          "whitelistIPs": [
            "10.1.70.156",
            "1.1.1.1",
            "96.27.58.209"
          ],
          "callbackAddress": "/callbackAddress1",
          "postbackAddress": "http://96.127.58.209:80/cfmSimulator/Retpmtcfm",
          "tcsAppID": "NewTestTcsAppId"
        };

        var Agency = appServer.models.Agency;

        Agency.create(input2, function(err, result){
          if(result){
            console.log("Sample data is created successfully");
            done();
          }
        });
      }
    })




  });


  after(function(done){
    var AppConfig = appServer.models.ApplicationConfiguration;
    AppConfig.deleteAll({name : 'TestAppConfig'}, function(err, response){
      if(response){
        console.log("Sample data1 is cleanup successfully");
      }
    });
    var Agency = appServer.models.Agency;
    Agency.deleteAll({"tcsAppID": "NewTestTcsAppId"}, function(err, response){
      if(response){
        console.log("Sample data2 is cleanup successfully");
        done();
      }
    });

  });

  describe('findAgencyByAppName Tests;', function () {

    it('findAgencyByAppName Test', function (done) {

      AgencyMapper.findAgencyByAppName('TestAppConfig', function(err, result){
        if(err){
          assert.equal(err, null);
          done();
        } else {
          assert.equal(result.applicationCode, "TestAppCode");
          done();
        }
      })
    });

  });

  describe('findAgencyByTCSAppId Tests', function () {

    it('UpdateStatueForRequest using DBService', function (done) {
      AgencyMapper.findAgencyByTCSAppId("NewTestTcsAppId", function(err, result){
        if(err){
          assert.equal(err, null);
          done();
        } else {
          assert.equal(result.paygovAppName, "Test Single Family Claim Remittance");
          done();
        }
      })

    });

  });

});
