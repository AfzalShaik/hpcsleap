
var app = require('../server/server');

describe('HPCS Test Suite;', function () {
  var server;
  before(function(done) {
    //server = app.start();
    done();
  });

  after(function() {
   // server.close();
  })

  describe('TCS Single Service Tests;', function () {

    it('TCS Single Service: processACHCancelTest', function (done) {
      var fs = require('fs');
      var input = JSON.parse(fs.readFileSync('./tests/data/processACHCancel.json'));
      var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHCancel";
      var request = require('request');
      var sslOption = require('./../server/ssl/ssl-cert');
      console.log('Request:');
      console.log(input);
      request({
        url: url,
        method: "POST",
        json: true,
        body: input,
        key: sslOption.key,
        cert: sslOption.cert,
        requestCert:true,
        ca:sslOption.ca,
        rejectUnauthorized:false
      }, function(err, response, body) {
        if(err) { return err; }
        console.log('Response:',response);
        console.log(body);
        done();
      });
    });

    it('TCS Single Service: processACHDebitTest', function (done) {
      var fs = require('fs');
      var input = JSON.parse(fs.readFileSync('./tests/data/processACHDebit.json'));
      var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHDebit";
      var request = require('request');
      var sslOption = require('./../server/ssl/ssl-cert');
      console.log('Request:');
      console.log(input);
      request({
        url: url,
        method: "POST",
        json: true,
        body: input,
        key: sslOption.key,
        cert: sslOption.cert,
        requestCert:true,
        ca:sslOption.ca,
        rejectUnauthorized:false
      }, function(err, response, body) {
        if(err) { return err; }
        console.log('Response:');
        console.log(body);
        done();
      });
    });

    it('TCS Single Service: processACHPrenotification', function (done) {
      var fs = require('fs');
      var input = JSON.parse(fs.readFileSync('./tests/data/processACHPrenotification.json'));
      var url = "https://localhost:2345/api/TCSSingleServiceWrappers/processACHPrenotification";
      var request = require('request');
      var sslOption = require('./../server/ssl/ssl-cert');
      console.log('Request:');
      console.log(input);
      request({
        url: url,
        method: "POST",
        json: true,
        body: input,
        key: sslOption.key,
        cert: sslOption.cert,
        requestCert:true,
        ca:sslOption.ca,
        rejectUnauthorized:false
      }, function(err, response, body) {
        if(err) { return err; }
        console.log('Response:');
        console.log(body);
        done();
      });
    });

  });

  describe('TCS Single Query Service Tests;', function () {

    it('TCS Single Query Service: processTCSSingleQuery', function (done) {
      var fs = require('fs');
      var input = JSON.parse(fs.readFileSync('./tests/data/processTCSSingleQuery.json'));
      var url = "https://localhost:2345/api/TCSSingleQueryServiceWrappers/processTCSSingleQuery";
      var request = require('request');
      var sslOption = require('./../server/ssl/ssl-cert');
      console.log('Request:');
      console.log(input);
      request({
        url: url,
        method: "POST",
        json: true,
        body: input,
        key: sslOption.key,
        cert: sslOption.cert,
        requestCert:true,
        ca:sslOption.ca,
        rejectUnauthorized:false
      }, function(err, response, body) {
        if(err) { return err; }
        console.log('Response:');
        console.log(body);
        done();
      });
    });

  });

});


