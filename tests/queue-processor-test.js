var assert = require('assert');
var queryProcessor = require('./../server/boot/queue-processor');


describe('Queue Processor Test Suite;', function () {

  describe('pushMessageToQueue Tests', function () {

      it('pushMessageToQueue Test', function (done) {

        var data = {
          service: {
            "agency_id": "950",
            "tcs_app_id": "TCSHUDSFCR",
            "ACHDebit": {
              "agency_tracking_id": "00000AA99",
              "account_number": "1234",
              "routing_transit_number": "044000037",
              "account_type": "BusinessChecking",
              "transaction_amount": 6.89,
              "first_name": "John",
              "last_name" : "doe",
              "business_name": "AAAE"
            }
          },
          request : "TCSSingleService/processACHDebit"
        };
        try {
          queryProcessor.pushMessageToQueue(data);
          done();
        }catch(err){
          if(err){
            assert.equal(err, null);
            done();
          }
        }

      });


    it('callPayGovClient Test', function (done) {

      var msg = {
        service: {
          "agency_id": "950",
          "tcs_app_id": "TCSHUDSFCR",
          "ACHDebit": {
            "agency_tracking_id": "00000AA99",
            "account_number": "1234",
            "routing_transit_number": "044000037",
            "account_type": "BusinessChecking",
            "transaction_amount": 6.89,
            "first_name": "John",
            "last_name" : "doe",
            "business_name": "AAAE"
          }
        },
        request : "TCSSingleService/processACHDebit"
      };
      var data = {
        content : JSON.stringify(msg)
      };
      try {
        queryProcessor.callPayGovClient(data);
        done();
      }catch(err){
        if(err){
          assert.equal(err, null);
          done();
        }
      }

    });

    it('updateStatusToDB Test', function (done) {

      var queryResponse = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDCOMMONSERVICES",
        "tcs_ach_results": {
          "ach_debit_query_response": [ {
            "agency_tracking_id": "990000999",
            "paygov_tracking_id": "3FP33O8R",
            "transaction_status": "Received",
            "transaction_amount": "6.89",
            "masked_account_number": "************1234",
            "return_code": "1001",
            "return_detail": "Successful submission of ACH Debit",
            "account_holder_name": "AAAE",
            "account_holder_email_address": null,
            "transaction_date": "2016-05-18T15:17:03",
            "account_type":"BusinessChecking",
            "routing_transit_number": "044000037",
            "sec_code": "CCD",
            "return_reason_code": null,
            "deposit_ticket_number": null,
            "debit_voucher_number": null,
            "installment_number": "0",
            "total_installments": "1",
            "payment_frequency": "OneTime",
            "app_batch_id": null } ]
        }
      };

      var achdebitResponse = { agency_id: '950',
        tcs_app_id: 'TCSHUDSFCR',
        ACHDebit:
        { agency_tracking_id: '00000AA99',
          transaction_amount: '6.89',
          masked_account_number: '************1234',
          effective_date: '2016-11-08',
          return_code: '1001',
          return_detail: 'Successful submission of ACH Debit',
          transaction_status: 'Received',
          transaction_date: '2016-11-05T04:57:37',
          paygov_tracking_id: '3FP4NR41' } };

      var cancelResponse = { agency_id: '950',
        tcs_app_id: 'TCSHUDSFCR',
        ACHCancel:
        { agency_tracking_id: '00000AA99',
          transaction_amount: '6.89',
          masked_account_number: '************1234',
          effective_date: '2016-11-08',
          return_code: '1003',
          return_detail: 'Successful submission of ACH CancelTransaction',
          transaction_status: 'Canceled',
          transaction_date: '2016-11-05T04:57:37',
          orig_paygov_tracking_id: '3FP4NR41' } };

      try {
        queryProcessor.updateStatusToDB(achdebitResponse, 'ACHDebit');
        queryProcessor.updateStatusToDB(queryResponse, 'TCSSingleQuery');
        queryProcessor.updateStatusToDB(cancelResponse, 'ACHCancel');
        done();
      }catch(err){
        if(err){
          assert.equal(err, null);
          done();
        }
      }

    });

  });


});



