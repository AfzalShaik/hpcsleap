describe('User Creation Demo', function() {

  //before each testcase login
  beforeEach(function() {
   var fs = require('fs');
    var input = JSON.parse(fs.readFileSync('./tests/protractor/DashboardURL/dashboardinput.json'));
    browser.get(input.URL);
    var username =  element(by.id('username'));
    var password =  element(by.id('password'));
    var submit =  element(by.id('Login'));
    var userinput =  input.Username;
    var passinput =  input.Password;
    username.sendKeys(userinput);
    password.sendKeys(passinput);
    submit.click();
browser.driver.sleep(1000);
    
  });

it('User Creation - Invalid First Name',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('1');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('Kumar');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('user2@test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('Testing');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('Oracle!1');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('Test');
    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the First Name ((Alphabets and space only))');
    //browser.pause();
	});
});
it('User Creation - Invalid Last Name',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('Test');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('1');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('user3@test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('Testing');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('Oracle!1');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('Test');
    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the Last Name (Alphabets only)');

	});
});

it('User Creation - Invalid Email',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('Test');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('Kumar');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('user3test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('Testing');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('Oracle!1');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('Test');
    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter a valid Email');

	});
});


it('User Creation - Invalid Password',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('Test');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('Kumar');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('user3@test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('Testing');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('asdfghjk');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('Test');
var status = element(by.id('status'));
status.sendKeys('Active');
    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters');

	});
});

it('User Creation - Application not selected',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('Test');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('Kumar');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('user4@test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('Oracle!1');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('Test');
    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select Application Name');

	});
});
it('User Creation - Role not selected',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('Test');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('Kumar');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('user4@test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('Testing');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('Oracle!1');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('');
    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select atleast one Role');
   
	});
});

it('User Creation - Duplicate User',function(){
   //  browser.sleep(1000);	
	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var createUser = element(by.id('createUser'));
	createUser.click();
    browser.driver.sleep(1000);
	var newname = element(by.model('user.name'));
	newname.sendKeys('Test');
	var userlastname = element(by.model('user.lastName'));
	userlastname.sendKeys('Kumar');
	var useremail = element(by.model('user.email'));
	useremail.sendKeys('test1@test.com');
    var appname = element(by.model('user.applicationName'));
	appname.sendKeys('Testing');
    var userpassword = element(by.model('user.password'));
	userpassword.sendKeys('Oracle!1');
    var userrole = element(by.model('user.role'));
    userrole.sendKeys('Test');
var status = element(by.id('status'));
status.sendKeys('Active');

    element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Email already Exist');
   
	});
});



it('User Edit - Invalid First Name',function(){

	var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
var edituser = element(by.css('[data-target="#userEdit"]'));
edituser.click();
 browser.driver.sleep(1000);
var userfirstname = element(by.id('firstName'));
userfirstname.sendKeys('Kumar1');
var status = element(by.id('status1'));
status.sendKeys('Active');

    element(by.css('input[value="Update"]')).click();	
browser.driver.sleep(1000);

var value = element(by.css('[ng-show="userCreationError"]'));
value.getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the First Name (Alphabets and space only)');

	});


});


it('Edit User  - Invalid Last Name',function(){
   //  browser.sleep(1000);	
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
	var edituser = element(by.css('[data-target="#userEdit"]'));
               edituser.click();
    browser.driver.sleep(1000);
	var userlastname = element(by.id('lastName'));
	userlastname.sendKeys('1');
var status = element(by.id('status1'));
status.sendKeys('Active');
	
    element(by.css('input[value="Update"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the Last Name (Alphabets only)');

	});
});

it('Edit User  - Invalid Email',function(){
   //  browser.sleep(1000);	
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
var edituser = element(by.css('[data-target="#userEdit"]'));
               edituser.click();
    browser.driver.sleep(1000);
	var useremail = element(by.id('email1'));
	useremail.sendKeys('1234');
var status = element(by.id('status1'));
status.sendKeys('Active');

    element(by.css('input[value="Update"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);

   expect(text).toEqual('Please enter a valid Email');
  
	});
});


it('Edit User - Invalid Password',function(){
   //  browser.sleep(1000);	
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var users = element(by.id('users'));
    users.click();
browser.driver.sleep(1000);
var edituser = element(by.css('[data-target="#userEdit"]'));
      edituser.click();
    browser.driver.sleep(1000);
    var userpassword = element(by.model('updateUser.password'));
	userpassword.sendKeys('12345678');
var status = element(by.id('status1'));
status.sendKeys('Active');

      element(by.css('input[value="Update"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="userCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters');

	});
});


it('Role Creation - Invalid Name',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var roles = element(by.id('roles'));
	roles.click();
browser.driver.sleep(1000);
	var createRole = element(by.id('createRole'));
	createRole.click();
	browser.driver.sleep(1000);
	var rolename = element(by.model('role.name'));
	rolename.sendKeys('Tests123');
	var roledesc = element(by.model('role.description'));
	roledesc.sendKeys('Testings');
	var saverole = element(by.id('saveRole'));
	saverole.click();
browser.driver.sleep(1000);
                element(by.css('[ng-show="roleCreationError"]')).getText().then(function(text){
	console.log(text);
               expect(text).toEqual('Please enter the name (Alphabets and Space only)');
   
	});


});

it('Role Creation - Duplicate Role',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var roles = element(by.id('roles'));
	roles.click();
browser.driver.sleep(1000);
	var createRole = element(by.id('createRole'));
	createRole.click();
	browser.driver.sleep(1000);
	var rolename = element(by.model('role.name'));
	rolename.sendKeys('Test');
	var roledesc = element(by.model('role.description'));
	roledesc.sendKeys('Testings');
	var saverole = element(by.id('saveRole'));
	saverole.click();
browser.driver.sleep(1000);
                element(by.css('[ng-show="roleCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
               expect(text).toEqual('Role Already Exist');
   
	});


});
it('Role Edit - Invalid Name',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var roles = element(by.id('roles'));
	roles.click();
browser.driver.sleep(1000);
	var editRole = element(by.css('[data-target="#roleEdit"]'));
	editRole.click();


	browser.driver.sleep(1000);
	var rolename = element(by.model('updateRole.name'));
	rolename.sendKeys('Tests123');
	var roledesc = element(by.model('updateRole.description'));
	roledesc.sendKeys('Testings');
	element(by.css('input[value="Update"]')).click();	
browser.driver.sleep(1000);
                element(by.css('[ng-show="roleCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
               expect(text).toEqual('Please enter the name (Alphabets and space only)');
   
	});

});
it('Role Delete - Assigned to User',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
	var roles = element(by.id('roles'));
	roles.click();
browser.driver.sleep(1000);

	var deleteRole = element(by.css('[data-target="#roleDelete"]'));
	deleteRole.click();

	browser.driver.sleep(1000);
	
                element(by.css('[ng-show="!delRole"]')).getAttribute("textContent").then(function(text){
	console.log(text);
               expect(text).toContain("This Role can't be deleted..Because it is associated with user");

   
	});

});


it(' Application Configuration - Invalid Agency Id',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('a');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.google.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.google.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.google.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('Testing');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);

element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the AgencyId in the Numeric format only, Max length 21 characters');

	});


});

it(' Application Configuration - Invalid TCS App Id',function(){

var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM!');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.google.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.google.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.google.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('Testing');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);

element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the TcsAppId in the AlphaNumeric format');

	});


});

it(' Application Configuration - Invalid Application Name',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps1');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.google.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.google.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.google.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('Testing');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the Application Name in the required format(Alphabets and Space only)');

	});


});


it(' Application Configuration - Invalid RetpmtCfm URL',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.google.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.google.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('Testing');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the retpmtCfmURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)');

	});


});


it(' Application Configuration - Invalid ResultCfm URL',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();

browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.hpcs.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.hpcs.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('Testing');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the resultCfmURLs in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)');

	});


});


it(' Application Configuration - Invalid Settlement URL',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.hpcs.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.hpcs.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www');
var machineid = element(by.id('machineId'));
machineid.sendKeys('Testing');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();
browser.driver.sleep(1000);	
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the SettlementURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)');

	});


});


it(' Application Configuration - Invalid Machine Id',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.hpcs.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.hpcs.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.hpcs.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('a a');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the MachineId (Alphanumerics only)');

	});


});


it(' Application Configuration - Invalid Password',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('TestApps');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.hpcs.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.hpcs.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.hpcs.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('aa');
var password = element(by.id('password'));
password.sendKeys('12345678');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters');
 
	});


});


it(' Application Configuration - Duplicate TCS App Id',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TCSHUDSFCM');
var appname = element(by.id('name'));
appname.sendKeys('MyTest');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.hpcs.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.hpcs.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.hpcs.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('aa');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toContain('The `ApplicationConfiguration` instance is not valid. ');
 
	});


});

it(' Application Configuration - Duplicate Application Name',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[ng-show="superAdminLogin == true"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId'));
agencyId.sendKeys('950');
var tcsappid = element(by.id('tcsAppId'));
tcsappid.sendKeys('TestAppIdss');
var appname = element(by.id('name'));
appname.sendKeys('Appname');
var appcode = element(by.id('appCode'));
appcode.sendKeys('TestAppcode');
var retpmtcfmurl = element(by.id('retpmtCfmURL'));
retpmtcfmurl.sendKeys('http://www.hpcs.com');
var whiteIP = element(by.id('whitelistedIPs'));
whiteIP.sendKeys('');
var resulturl = element(by.id('resultCfmURLs'));
resulturl.sendKeys('http://www.hpcs.com');
var settleurl = element(by.id('settlementURL'));
settleurl.sendKeys('http://www.hpcs.com');
var machineid = element(by.id('machineId'));
machineid.sendKeys('aa');
var password = element(by.id('password'));
password.sendKeys('Oracle!1');
element(by.css('input[value="Save"]')).click();	
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toContain('The `ApplicationConfiguration` instance is not valid. ');
 
	});


});

it(' Edit Application Configuration - Invalid Agency Id',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var agencyId = element(by.id('agencyId1'));
agencyId.sendKeys('a');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);	
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the AgencyId in the Numeric format only, Max length 21 characters');


	});

});

it(' Edit Application Configuration - Invalid TCS App Id',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var tcsappid = element(by.id('tcsAppId1'));
tcsappid.sendKeys('TCSHUDSFCM!');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the TcsAppId in the AlphaNumeric format, Max length 30 characters');

	});


});

it('Edit  Application Configuration - Invalid Application Name',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var appname = element(by.id('name1'));
appname.sendKeys('TestApps1');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
expect(text).toEqual('Please enter the Application Name in the required format(Alphabets and space only, Max length 80 characters)');

	});


});


it('Edit Application Configuration - Invalid RetpmtCfm URL',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var retpmtcfmurl = element(by.id('retpmtCfmURL1'));
retpmtcfmurl.sendKeys('http://www');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
expect(text).toEqual('Please enter the retpmtCfmURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)');

	});


});


it('Edit Application Configuration - Invalid ResultCfm URL',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var resulturl = element(by.id('resultCfmURLs1'));
resulturl.sendKeys('http://www');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
expect(text).toEqual('Please enter the resultCfmURLs in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)');

	});


});


it('Edit Application Configuration - Invalid Settlement URL',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var settleurl = element(by.id('settlementURL1'));
settleurl.sendKeys('http://www');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the SettlementURL in URL format(Ex: http://www.hpcs.com or http://127.0.0.1/)');

	});


});


it('Edit Application Configuration - Invalid Machine Id',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var machineid = element(by.id('machineId1'));
machineid.sendKeys('a a');
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Please enter the MachineId (Alphanumerics only)');

	});


});


it('Edit Application Configuration - Invalid Password',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();
browser.driver.sleep(1000);
var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
element(by.css('[data-target="#appEdit"]')).click();
browser.driver.sleep(1000);
var password = element(by.id('password1'));
password.clear().then(function(){
password.sendKeys('12345678');
})
var appcode = element(by.id('updateAppCode'));
appcode.sendKeys('TestAppcode');
var IPaddr = element(by.id('whitelistedIPs1'));
IPaddr.clear().then(function(){
IPaddr.sendKeys('127.0.0.1');
})

element(by.css('input[value="Update"]')).click();
browser.driver.sleep(1000);
element(by.css('[ng-show="appCreationError"]')).getAttribute("textContent").then(function(text){
	console.log(text);
   expect(text).toEqual('Password must contains one Uppercase letter, one Special character, one Number - Min length 8 characters');
 
	});


});


it('App Delete - Assigned to User',function(){
var adminModule = browser.driver.findElement(by.xpath('.//*[contains(text(),"Admin Module")]'));
    adminModule.click();

browser.driver.sleep(1000);
	var appconfig = element(by.id('appConfig'));
	appconfig.click();
browser.driver.sleep(1000);
	var deleteApp = element(by.css('[data-target="#appDelete"]'));
	deleteApp.click();

	browser.driver.sleep(1000);
	
                element(by.css('[ng-show="!delApp"]')).getAttribute("textContent").then(function(text){
	console.log(text);
               expect(text).toContain("This Application can't be deleted..Because it is associated with user");

   
	});
	//browser.pause();
});

it('Report - Blank Start Date & End Date',function(){
   //  browser.sleep(1000);	
	var reportModule = element(by.css('[href="#/reports"]'));
    reportModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="dateSelectionError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select Start Date');

	});
});

it('Report - Blank Start Date',function(){
   //  browser.sleep(1000);	
	var reportModule = element(by.css('[href="#/reports"]'));
    reportModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('09/20/2016');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="dateSelectionError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select Start Date');

	});
});

it('Report - Blank Start Date',function(){
   //  browser.sleep(1000);	
	var reportModule = element(by.css('[href="#/reports"]'));
    reportModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('09/01/2016');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="dateSelectionError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select End Date');

	});
});

it('Report - Details of selected application',function(){
   //  browser.sleep(1000);	
	var reportModule = element(by.css('[href="#/reports"]'));
    reportModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('09/01/2016');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('09/20/2016');
var appfilter = element(by.id('appFilterOptions'));
appfilter.sendKeys('SingleFamilyPremiumCollectionSubsystemUpfront');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.id('reportsTable_info')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Showing 1 to 1 of 1 entries');

	});
});
it('Report - Blank Start Date & End Date',function(){
   //  browser.sleep(1000);	
	var txnModule = element(by.css('[href="#/transactions"]'));
    txnModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="dateSelectionError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select Start Date');

	});
});

it('Report - Blank Start Date',function(){
   //  browser.sleep(1000);	
	var txnModule = element(by.css('[href="#/transactions"]'));
    txnModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('09/20/2016');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="dateSelectionError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select Start Date');

	});
});

it('Report - Blank Start Date',function(){
   //  browser.sleep(1000);	
	var txnModule = element(by.css('[href="#/transactions"]'));
    txnModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('09/01/2016');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[ng-show="dateSelectionError"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Please Select End Date');

	});
});

it('Report - Details of selected application',function(){
   //  browser.sleep(1000);	
	var txnModule = element(by.css('[href="#/transactions"]'));
    txnModule.click();
browser.driver.sleep(1000);
var startdate = element(by.id('startDatePicker'));
startdate.sendKeys('09/01/2016');
var enddate = element(by.id('endDatePicker'));
enddate.sendKeys('09/20/2016');
var appfilter = element(by.id('appFilterOptions'));
appfilter.sendKeys('SingleFamilyPremiumCollectionSubsystemUpfront');	
browser.driver.sleep(1000);
	
    element(by.css('[ng-click="filterByDate()"]')).click();	
browser.driver.sleep(1000);
    element(by.css('[role="status"]')).getText().then(function(text){
	console.log(text);
   expect(text).toEqual('Showing 1 to 1 of 1 entries');

	});
});

});
