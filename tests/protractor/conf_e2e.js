
// conf.js
exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec_e2e.js'],
//  multiCapabilities: [ {
 //   browserName: 'chrome'
 // }]
restartBrowserBetweenTests: true
}
