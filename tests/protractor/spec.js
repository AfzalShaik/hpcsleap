// spec.js
describe('HPCS Dashboard demo', function() {

  //before each testcase login
  beforeEach(function() {
    browser.get('https://localhost:2345/index.html');
    var login =  element(by.id('login'));
    login.click();
    expect(browser.getCurrentUrl()).toEqual("https://localhost:2345/index.html#/login");
    var username =  element(by.id('username'));
    var password =  element(by.id('password'));
    var submit =  element(by.id('Login'));
    username.sendKeys('admin@admin.com');
    password.sendKeys('Admin@1234');
    submit.click();
  });

 // it('should have a title', function() {
   // expect(browser.getTitle()).toEqual('HPCS DashBoard');
  //});

  it('should show list of users', function() {
      var adminModule = element(by.id('adminModule'));
      adminModule.click();
      var users = element(by.id('users'));
      users.click();

    //var createUser = element(by.id('createUser'));
    //createUser.click();
    //browser.pause();
  });

  it('should show list of roles', function() {
    var adminModule = element(by.id('adminModule'));
    adminModule.click();
    var role = element(by.id('roles'));
    role.click();

    var createRole = element(by.id('createRole'));
    createRole.click();
   // browser.pause();
    var roleName = element(by.id('rolename'));
    //var roleDesc = element(by.id('roledescr'));
   // var saveRole = element(by.id('saveRole'));
    roleName.sendKeys('Test');
    roleDesc.sendKeys('Test Role Created');
    //saveRole.click();
  });
});
