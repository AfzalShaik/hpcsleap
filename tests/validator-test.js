describe('HPCS Input Validation Test Suite;', function () {
  var fs = require('fs');
  var assert = require('assert');
  var validator = require('./../server/boot/lib/validation/validator.js');

  before(function(done) {
    done();
  });
  describe('TCS Single Service Input Validation Tests;', function () {

    it('TCS Single Service: processACHCancel Input Validation - Valid Input', function(done) {

      var input = JSON.parse(fs.readFileSync('./tests/data/processACHCancel.json'));
      var isValidInput = validator.validateInput('TCSSingleService','ACHCancelRequest', input);
      assert.equal(isValidInput, true);
      done();
    });

    it('TCS Single Service: processACHCancel Input Validation - Invalid Input', function(done) {

      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHCancel": {
          "orig_paygov_tracking_id": "3FP31S1D",
          "transaction_amount": "6.49", 
          "agency_tracking_id": "000022918"
        }
      }

      var isValidInput = validator.validateInput('TCSSingleService','ACHCancelRequest', input);
      assert.equal(isValidInput, false);
      done();
    });

    it('TCS Single Service: processACHDebit Input Validation - valid Input', function(done) {

      var input = JSON.parse(fs.readFileSync('./tests/data/processACHDebit.json'));
      var isValidInput = validator.validateInput('TCSSingleService','ACHDebitRequest', input);
      assert.equal(isValidInput, true);
      done();
    });

    it('TCS Single Service: processACHDebit Input Validation - Invalid Input', function(done) {

      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHDebit": {
          "agency_tracking_id": "000000299",
          "account_number": "1234",
          "routing_transit_number": 044000037,
          "account_type": "BusinessChecking",
          "transaction_amount": 6.89,
          "first_name": "John",
          "last_name" : "doe",
          "business_name": "AAAE"
        }
      }

      var isValidInput = validator.validateInput('TCSSingleService','ACHDebitRequest', input);
      assert.equal(isValidInput, false);
      done();
    });

    it('TCS Single Service: processACHPrenotification Input Validation - Valid Input', function(done) {

      var input = JSON.parse(fs.readFileSync('./tests/data/processACHPrenotification.json'));
      var isValidInput = validator.validateInput('TCSSingleService','ACHPrenotificationRequest', input);
      assert.equal(isValidInput, true);
      done();
    });

    it('TCS Single Service: processACHPrenotification Input Validation - Invalid Input', function(done) {

      var input = {
        "agency_id": "950",
        "tcs_app_id": "TCSHUDSFCR",
        "ACHPrenotification": {
          "agency_tracking_id": "0000000801",
          "account_number": "5105105105105100",
          "routing_transit_number": "044000037",
          "account_type": "BusinessChecking",
          "transaction_amount": 0.00,
          "first_name": "John"
        }
      }

      var isValidInput = validator.validateInput('TCSSingleService','ACHPrenotificationRequest', input);
      assert.equal(isValidInput, false);
      done();
    });
  });

 describe('TCS Single Query Service Tests;', function () {

     it('TCS Single Query Service: processSingleQuery Input Validation - valid Input', function(done) {

       var input =  {
         "agency_id": "950",
         "tcs_app_id": "TCSHUDSFCR",
         "agency_tracking_id": "990000999",
         "paygov_tracking_id": "3FP33O8R"
       }

       var isValidInput = validator.validateInput('TCSSingleQueryService','TCSSingleQueryRequest', input);
       assert.equal(isValidInput, true);
       done();
     });


    it('TCS Single Query Service: processSingleQuery Input Validation - Invalid Input', function(done) {

      var input =  {
        "agency_id": "950",
        "agency_tracking_id": "990000999",
        "paygov_tracking_id": "3FP33O8R"
      }

      var isValidInput = validator.validateInput('TCSSingleQueryService','TCSSingleQueryRequest', input);
      assert.equal(isValidInput, false);
      done();
    });

  });
});


