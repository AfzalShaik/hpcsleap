var fs = require('fs');
var assert = require('assert');
var appServer = require('../server/server');
var hpcsId = 'TESTRECORD';
var DBService = require('./../server/boot/database-services.js');
describe('HPCS Id Generator Test Suite;', function () {

  before(function(done){
    var input = {
      "agency_id": "950",
      "tcs_app_id": "TCSHUDSFCR",
      "ACHDebit": {
        "agency_tracking_id": "000000299",
        "account_number": "1234",
        "routing_transit_number": "044000037",
        "account_type": "BusinessChecking",
        "transaction_amount": 6.89,
        "first_name": "John",
        "last_name" : "doe",
        "business_name": "AAAE"
      }
    };
    console.log('Request:');
    console.log(input);
    DBService.persistMessageToDB(hpcsId, 'ACHDebit',input, function(err, output){
      console.log(err);
      console.log(output);
      done();
    });
  });

  after(function(done) {
    // Delete existing record.
    var Request = appServer.models.Request;
    Request.deleteAll({hpcs_id : 'TESTRECORD'}, function(err, response){
      if(err){
        assert.equal(err, null); // to fail the TC
        done();
      } else {
        console.log(response);
        done();
      }
    });
  });
  describe('Persist to DB Tests;', function () {

    it('persistMessage from processACHDebit Request', function (done) {
        var Request = appServer.models.Request;
        Request.find({where: {hpcs_id : 'TESTRECORD'}}, function(err, result){
          if(err){
            console.log(err);
            done();
          } else {
            console.log("FetchRequestFromHPCSId size: " + result.length);
            assert.equal(1, result.length);
            assert.equal('TCSHUDSFCR', result[0].tcs_app_id);
            assert.equal(false, result[0].sent);
            done();
          }
        });
    });
  });

  describe('UpdateStatueForRequest Test;', function () {

    it('UpdateStatueForRequest using DBService', function (done) {

      DBService.updateStatueForRequest(hpcsId, {sent : true});

        var Request = appServer.models.Request;
        Request.find({where: {hpcs_id : 'TESTRECORD'}}, function(err, result) {
          if (err) {
            console.log(err);
            done();
          } else {
            console.log("FetchRequestFromHPCSId size: " + result.length,result[0].tcs_app_id,result[0].sent);
            assert.equal(1, result.length);
            assert.equal('TCSHUDSFCR', result[0].tcs_app_id);
            assert.equal(false, result[0].sent);
            done();
          }
        });
    });

  });

  describe('updateStatueForRequestWithCB Test;', function () {

    it('updateStatueForRequestWithCB using DBService', function (done) {

      DBService.updateStatueForRequestWithCB(hpcsId, {status : 'Failed'}, function(err, result){
        if(err){
          console.log(err);
          assert.equal(err, null);
          done();
        } else {
          console.log(result);
          assert.notEqual(result, null);
          done();
        }
      });
    });
  });

  describe('isUniqueHPCSId Test;', function () {

    it('isUniqueHPCSId using DBService', function (done) {

      DBService.isUniqueHPCSId(hpcsId, function(result){
        console.log(result);
        assert.equal(result, false);
        DBService.isUniqueHPCSId("SomeNonUniqueId", function(result) {
          console.log(result);
          assert.equal(result, true);
          done();
        });
      });
    });
  });

  describe('getPayGovTrackingId Test;', function () {

    it('getPayGovTrackingId using DBService', function (done) {

      DBService.getPayGovTrackingId(hpcsId, function(err, result){
        if(err){
          console.log(err);
          assert.equal(err, null);
          done();
        } else {
          console.log(result);
          assert.notEqual(result, null);
          assert.equal(result.status, 'Failed');
          done();
        }
      });
    });
  });


  describe('FetchRequestFromHPCSId Test;', function () {

    it('FetchRequestFromHPCSId using DBService', function (done) {

      DBService.fetchRequestFromHPCSId(hpcsId, function (err, result) {
        if (err) {
          console.log(err);
          done();
        } else {
          assert.equal('Failed', result.status);
          assert.equal('ACHDebit', result.operationName);
          done();
        }
      });

    });

  });

});


